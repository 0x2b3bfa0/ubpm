<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Importeren van apparaat</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="24"/>
        <source>Device Information</source>
        <translation>Apparaat info</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="53"/>
        <source>Serial</source>
        <translation>Serienummer</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="60"/>
        <source>Producer</source>
        <translation>Fabrikant</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="67"/>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="145"/>
        <source>Import</source>
        <translation>Importeren</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="162"/>
        <source>Write Logfile</source>
        <translation>Logfile schrijven</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="188"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="18"/>
        <source>Loading plugin translation failed!</source>
        <translation>Het laden van de extensievertaling is mislukt!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="155"/>
        <source>Device doesn&apos;t respond, try again?</source>
        <translation>Apparaat antwoordt niet, opnieuw proberen?</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="253"/>
        <source>Could not open logfile %1!

%2</source>
        <translation>Kan logfile %1 niet openen!

%2</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="364"/>
        <location filename="../../DialogImport.cpp" line="403"/>
        <source>Import aborted by user!</source>
        <translation>Import geannuleerd door gebruiker!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="42"/>
        <source>Could not open usb device %1:%2!</source>
        <translation>Kan USB-apparaat %1:%2 niet openen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="260"/>
        <source>Can&apos;t set device into info mode!</source>
        <translation>Apparaat kan niet in de infomodus worden gezet!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="273"/>
        <location filename="../../DialogImport.cpp" line="287"/>
        <location filename="../../DialogImport.cpp" line="312"/>
        <location filename="../../DialogImport.cpp" line="326"/>
        <location filename="../../DialogImport.cpp" line="343"/>
        <location filename="../../DialogImport.cpp" line="377"/>
        <location filename="../../DialogImport.cpp" line="416"/>
        <source>Send %1 command failed!</source>
        <translation>Kommando %1 sturen is niet gelukt!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="299"/>
        <source>Can&apos;t set device into data mode!</source>
        <translation>Kan het apparaat niet in data-modus zetten!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="456"/>
        <source>Import in progress!</source>
        <translation>Importeren wordt uitgevoerd!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="445"/>
        <source>Really abort import?</source>
        <translation>Importeren echt afbreken?</translation>
    </message>
</context>
</TS>
