#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define VID 0x0590
#define PID 0x0090

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>").arg(MODEL)
#define VERSION		QString("1.0.1 [ HIDAPI %1 ]").arg(HID_API_VERSION_STR)
#define PRODUCER	"<a href='https://omronhealthcare.com/blood-pressure'>OMRON Corporation</a>"
#ifdef HEM7131U
	#define MODEL	"HEM-7131U"
	#define ALIAS	"M400 IT, M3 IT"
	#define IMAGE	":/png/hem-7131u.png"
	#define MANUAL	":/pdf/hem-7131u.pdf"
	#define MEMORY	60
#else
	#define MODEL	"HEM-7322U"
	#define ALIAS	"M500 IT, M6 Comfort IT"
	#define IMAGE	":/png/hem-7322u.png"
	#define MANUAL	":/pdf/hem-7322u.pdf"
	#define MEMORY	100
#endif
#define ICON		":/svg/usb.svg"

#define LOGFILE QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/ubpm-import.log"

#include "ui_DialogImport.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QTimer>
#include <QTranslator>

#include "deviceinterface.h"

#include "hidapi.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*);

	bool failed = false;

private:

	QTranslator translatorPlugin;

	quint8 cmd_init[9] = { 0x02, 0x08, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x18 };
	quint8 cmd_data[9] = { 0x02, 0x08, 0x01, 0x00, 0x02, 0xAC, 0x28, 0x00, 0x8F };
	quint8 cmd_done[9] = { 0x02, 0x08, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07 };
	quint8 cmd_fail[9] = { 0x02, 0x08, 0x0F, 0x0F, 0x0F, 0x0F, 0x00, 0x00, 0x08 };

	hid_device *hid;
	quint8 rawdata[64];
	QByteArray payload;
	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	bool abort = false;
	bool finished = true;

	int sendCMD(quint8*);
	int buildCRC(quint8*);
	void decryptPayload();
	void logRawData(bool, int, quint8*);

private slots:

	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void reject();
};

#endif // DLGIMPORT_H
