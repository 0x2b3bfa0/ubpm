<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Geräte Import</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="24"/>
        <source>Device Information</source>
        <translation>Geräte Information</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="53"/>
        <source>Serial</source>
        <translation>Seriennummer</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="60"/>
        <source>Producer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="67"/>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="116"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="133"/>
        <source>Write Logfile</source>
        <translation>Logdatei schreiben</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="159"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="18"/>
        <source>Loading plugin translation failed!</source>
        <translation>Laden der Erweiterung Übersetzung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="164"/>
        <source>Could not open logfile %1!

%2</source>
        <translation>Konnte Logdatei %1 nicht öffnen!

%2</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="189"/>
        <source>Import aborted by user!</source>
        <translation>Import durch Benutzer abgebrochen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="44"/>
        <source>Could not open usb device %1:%2!</source>
        <translation>Konnte USB-Gerät %1:%2 nicht öffnen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="174"/>
        <source>Press START/STOP on device and try again…</source>
        <translation>START/STOP am Gerät drücken und erneut versuchen…</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="231"/>
        <source>Import in progress!</source>
        <translation>Import wird durchgeführt!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="220"/>
        <source>Really abort import?</source>
        <translation>Import wirklich abbrechen?</translation>
    </message>
</context>
</TS>
