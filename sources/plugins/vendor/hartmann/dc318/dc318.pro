TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets serialport
DEFINES		+= DC318
INCLUDEPATH	+= ../../../../
SOURCES		= DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= res/dc318.qrc
TRANSLATIONS	= res/qm/dc318-de_DE.ts
TARGET		= ../../../veroval-dc318

unix:!macx {
	QMAKE_RPATHDIR += $ORIGIN/../../lib
}

system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../gce604/DialogImport.* .))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../gce604/res/qm/gce604-de_DE.qm res/qm/dc318-de_DE.qm))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../gce604/res/qm/gce604-nl.qm res/qm/dc318-nl.qm))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../../../shared/plugin/svg/*.svg res/svg))
QMAKE_CLEAN += $$OUT_PWD/DialogImport.*
QMAKE_CLEAN += $$OUT_PWD/res/qm/*.qm
QMAKE_CLEAN += $$OUT_PWD/res/svg/*.svg
