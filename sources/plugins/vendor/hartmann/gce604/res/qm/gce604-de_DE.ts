<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Geräte Import</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="24"/>
        <source>Device Information</source>
        <translation>Geräte Information</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="63"/>
        <source>Producer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="70"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="119"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="136"/>
        <source>Write Logfile</source>
        <translation>Logdatei schreiben</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="162"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="18"/>
        <source>Loading plugin translation failed!</source>
        <translation>Laden der Erweiterung Übersetzung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="28"/>
        <source>Could not find a serial port!

Is the usb2serial driver installed and the device connected?</source>
        <translation>Konnte keine serielle Schnittstelle finden!

Ist der USB2Seriell-Treiber installiert und das Gerät angeschlossen?</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="71"/>
        <source>Import aborted by user!</source>
        <translation>Import durch Benutzer abgebrochen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="102"/>
        <source>Could not read data!

Is the device powered on?

%1</source>
        <translation>Konnte Daten nicht lesen!

Ist das Gerät eingeschaltet?

%1</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="110"/>
        <source>Could not write data!

%1</source>
        <translation>Konnte Daten nicht schreiben!

%1</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="175"/>
        <source>Could not open logfile %1!

%2</source>
        <translation>Konnte Logdatei %1 nicht öffnen!

%2</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="210"/>
        <source>Could not open serial port &quot;%1&quot;!

%2</source>
        <translation>Konnte serielle Schnittstelle &quot;%1&quot; nicht öffnen!

%2</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="235"/>
        <source>Import in progress!</source>
        <translation>Import wird durchgeführt!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="224"/>
        <source>Really abort import?</source>
        <translation>Import wirklich abbrechen?</translation>
    </message>
</context>
</TS>
