<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Importeren van apparaat</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="24"/>
        <source>Device Information</source>
        <translation>Apparaat info</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="63"/>
        <source>Producer</source>
        <translation>Fabrikant</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="70"/>
        <source>Description</source>
        <translation>Beschrijving</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="119"/>
        <source>Import</source>
        <translation>Importeren</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="136"/>
        <source>Write Logfile</source>
        <translation>Logfile schrijven</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="162"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="18"/>
        <source>Loading plugin translation failed!</source>
        <translation>Het laden van de extensievertaling is mislukt!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="28"/>
        <source>Could not find a serial port!

Is the usb2serial driver installed and the device connected?</source>
        <translation>Kon geen seriële poort vinden!

Is het USB2 seriële stuurprogramma geïnstalleerd en het apparaat aangesloten?</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="71"/>
        <source>Import aborted by user!</source>
        <translation>Import geannuleerd door gebruiker!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="102"/>
        <source>Could not read data!

Is the device powered on?

%1</source>
        <translation>Kan geen data lezen!

Is het apparaat aangeschakeld?

%1</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="110"/>
        <source>Could not write data!

%1</source>
        <translation>Kan geen data wegschrijven!

%1</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="175"/>
        <source>Could not open logfile %1!

%2</source>
        <translation>Kan logfile %1 niet openen!

%2</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="210"/>
        <source>Could not open serial port &quot;%1&quot;!

%2</source>
        <translation>Kan seriele port &quot;%1&quot; niet openen!

%2</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="235"/>
        <source>Import in progress!</source>
        <translation>Importeren wordt uitgevoerd!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="224"/>
        <source>Really abort import?</source>
        <translation>Importeren echt afbreken?</translation>
    </message>
</context>
</TS>
