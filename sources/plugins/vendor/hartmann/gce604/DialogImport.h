#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define VID 0x067B
#define PID 0x2303

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>%2").arg(MODEL, HELPER)
#define VERSION		QString("1.0.0 [ USB2SERIAL ]")
#define PRODUCER	"<a href='https://www.veroval.info/en/products/bloodpressure'>Hartmann</a>"
#ifdef BPM25
	#define MODEL	"BPM25"
	#define ALIAS	"Veroval WR"
	#define IMAGE	":/png/bpm25.png"
	#define MANUAL	":/pdf/bpm25.pdf"
	#define HELPER	", Carsten Presser"
#elif DC318
	#define MODEL	"DC318"
	#define ALIAS	"Veroval Duo Control"
	#define IMAGE	":/png/dc318.png"
	#define MANUAL	":/pdf/dc318.pdf"
	#define HELPER	""
#else
	#define MODEL	"GCE604"
	#define ALIAS	"Veroval UA"
	#define IMAGE	":/png/gce604.png"
	#define MANUAL	":/pdf/gce604.pdf"
	#define HELPER	", Annett Heyder"
#endif
#define ICON		":/svg/usb.svg"

#define LOGFILE QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/ubpm-import.log"

#define TIMEOUT 1000

#include "ui_DialogImport.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QThread>
#include <QTranslator>

#include "deviceinterface.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*);

	bool failed = false;

private:

	QTranslator translatorPlugin;

	QByteArray cmd_init = QByteArray::fromHex("A5F90101FB");
	QByteArray cmd_exit = QByteArray::fromHex("A6F90101FB");;

	QSerialPortInfo spi;
	QSerialPort sp;

	QByteArray payload;
	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	bool abort = false;
	bool finished = true;

	bool sendCMD(QByteArray);
	void decryptPayload();
	void logRawData(bool, int, QByteArray);

private slots:

	void on_comboBox_activated(int);

	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void reject();
};

#endif // DLGIMPORT_H
