TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets serialport
INCLUDEPATH	+= ../../../../
SOURCES		= DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= res/gce604.qrc
TRANSLATIONS	= res/qm/gce604-de_DE.ts res/qm/gce604-nl.ts
TARGET		= ../../../veroval-gce604

unix:!macx {
	QMAKE_RPATHDIR += $ORIGIN/../../lib
}

system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../../../shared/plugin/svg/*.svg res/svg))
QMAKE_CLEAN += $$OUT_PWD/res/svg/*.svg
