TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets serialport
DEFINES		+= BPM25
INCLUDEPATH	+= ../../../../
SOURCES		= DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= res/bpm25.qrc
TRANSLATIONS	= res/qm/bpm25-de_DE.ts
TARGET		= ../../../veroval-bpm25

unix:!macx {
	QMAKE_RPATHDIR += $ORIGIN/../../lib
}

system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../gce604/DialogImport.* .))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../gce604/res/qm/gce604-de_DE.qm res/qm/bpm25-de_DE.qm))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../gce604/res/qm/gce604-nl.qm res/qm/bpm25-nl.qm))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../../../shared/plugin/svg/*.svg res/svg))
QMAKE_CLEAN += $$OUT_PWD/DialogImport.*
QMAKE_CLEAN += $$OUT_PWD/res/qm/*.qm
QMAKE_CLEAN += $$OUT_PWD/res/svg/*.svg
