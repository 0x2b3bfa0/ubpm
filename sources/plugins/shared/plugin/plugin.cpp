#include "plugin.h"

DEVICEINFO DevicePlugin::getDeviceInfo()
{
	QFile png(IMAGE), pdf(MANUAL);

	png.open(QIODevice::ReadOnly);
	pdf.open(QIODevice::ReadOnly);

	QIcon SVG(ICON);
	QByteArray PNG = png.readAll();
	QByteArray PDF = pdf.readAll();

	png.close();
	pdf.close();

	return DEVICEINFO { PRODUCER, MODEL, ALIAS, MAINTAINER, VERSION, SVG, PNG, PDF };
}

bool DevicePlugin::getDeviceData(QWidget *parent, QString language, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2)
{
	DialogImport dlg(parent, language, theme, user1, user2);

	if(!dlg.failed)
	{
		return dlg.exec();
	}

	return QDialog::Rejected;
}
