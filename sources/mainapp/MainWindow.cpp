#include "MainWindow.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	MainWindow win;

	win.show();

	return app.exec();
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
	QApplication::setAttribute(Qt::AA_DisableWindowContextHelpButton);
	QApplication::setStyle("Fusion");

	readSettings();

	setupUi(this);

	actionTranslation->setMenuRole(QAction::NoRole);	// workaround: don't show this entry as "about" in appplication menu for macos (in german it's used as about?)

	QMenu *menuChart = new QMenu(this);
	menuChart->addAction(actionPreviewChart);
	reinterpret_cast<QToolButton*>(mainToolBar->widgetForAction(actionPrintChart))->setMenu(menuChart);
	reinterpret_cast<QToolButton*>(mainToolBar->widgetForAction(actionPrintChart))->setPopupMode(QToolButton::MenuButtonPopup);

	QMenu *menuTable = new QMenu(this);
	menuTable->addAction(actionPreviewTable);
	reinterpret_cast<QToolButton*>(mainToolBar->widgetForAction(actionPrintTable))->setMenu(menuTable);
	reinterpret_cast<QToolButton*>(mainToolBar->widgetForAction(actionPrintTable))->setPopupMode(QToolButton::MenuButtonPopup);

	QMenu *menuStatistic = new QMenu(this);
	menuStatistic->addAction(actionPreviewStatistic);
	reinterpret_cast<QToolButton*>(mainToolBar->widgetForAction(actionPrintStatistic))->setMenu(menuStatistic);
	reinterpret_cast<QToolButton*>(mainToolBar->widgetForAction(actionPrintStatistic))->setPopupMode(QToolButton::MenuButtonPopup);

	QActionGroup *actionGroupUser = new QActionGroup(this);
	actionGroupUser->addAction(actionSwitchUser1);
	actionGroupUser->addAction(actionSwitchUser2);

	actionSwitchUser1->setToolTip(tr("Switch To %1").arg(settings.user[0].name.isEmpty() ? tr("User 1") : settings.user[0].name));
	actionSwitchUser1->setStatusTip(tr("Switch To %1").arg(settings.user[0].name.isEmpty() ? tr("User 1") : settings.user[0].name));
	actionSwitchUser2->setToolTip(tr("Switch To %1").arg(settings.user[1].name.isEmpty() ? tr("User 2") : settings.user[1].name));
	actionSwitchUser2->setStatusTip(tr("Switch To %1").arg(settings.user[1].name.isEmpty() ? tr("User 2") : settings.user[1].name));

	lcd->setSegmentStyle(QLCDNumber::Flat);
	lcd->setToolTip(tr("Records For Selected User"));
	lcd->setStatusTip(tr("Records For Selected User"));
	mainToolBar->insertWidget(actionSwitchUser2, lcd);

	toolbarDateTime->setCalendarPopup(true);
	toolbarDateTime->setWrapping(true);
	toolbarDateTime->setAlignment(Qt::AlignHCenter);
	toolbarDateTime->setDisplayFormat("dddd, dd.MM.yyyy hh:mm");
	toolbarDateTime->setToolTip(tr("Select Date & Time"));
	toolbarDateTime->setStatusTip(tr("Select Date & Time"));
	toolbarDateTime->calendarWidget()->setGridVisible(true);
	toolbarDateTime->calendarWidget()->setVerticalHeaderFormat(QCalendarWidget::ISOWeekNumbers);
	toolbarDateTime->adjustSize();

	toolbarSpacer->setMinimumWidth(5);

	mainToolBar->addWidget(toolbarSpacer);
	mainToolBar->addWidget(toolbarDateTime);

	mainStatusBar->addPermanentWidget(statusbarLabel);

	prepareMainChart(chartMainBP, chartViewBP, axisXBP, axisY1BP, axisY2BP, 60, 180);
	prepareMainChart(chartMainHR, chartViewHR, axisXHR, axisY1HR, axisY2HR, 50, 150);

	prepareAreaChart(chartMainBP, axisXBP, axisY1BP, areaSeriesSYS, lineSeriesLowerSYS, lineSeriesUpperSYS, QColor(syscolors[0]), settings.chart.range[0].sys_min, settings.chart.range[0].sys_max);
	prepareAreaChart(chartMainBP, axisXBP, axisY1BP, areaSeriesDIA, lineSeriesLowerDIA, lineSeriesUpperDIA, QColor(diacolors[0]), settings.chart.range[0].dia_min, settings.chart.range[0].dia_max);
	prepareAreaChart(chartMainHR, axisXHR, axisY1HR, areaSeriesBPM, lineSeriesLowerBPM, lineSeriesUpperBPM, QColor(bpmcolors[0]), settings.chart.range[0].bpm_min, settings.chart.range[0].bpm_max);

	prepareLineChart(chartMainBP, axisXBP, axisY1BP, lineSeriesSYS, QColor(syscolors[3]), tr("Systolic"));
	prepareLineChart(chartMainBP, axisXBP, axisY1BP, lineSeriesDIA, QColor(diacolors[3]), tr("Diastolic"));
	prepareLineChart(chartMainHR, axisXHR, axisY1HR, lineSeriesBPM, QColor(bpmcolors[3]), tr("Heartrate"));

	prepareScatterChart(chartMainBP, axisXBP, axisY1BP, scatterSeriesSYS);
	prepareScatterChart(chartMainBP, axisXBP, axisY1BP, scatterSeriesDIA);
	prepareScatterChart(chartMainHR, axisXHR, axisY1HR, scatterSeriesBPM);
	prepareScatterChart(chartMainHR, axisXHR, axisY1HR, scatterSeriesIHB);
	prepareScatterChart(chartMainHR, axisXHR, axisY1HR, scatterSeriesMOV);

	prepareBarChart(chartView_1, chartSYS1, seriesSYS1, setSYS1Min, setSYS1Max, setSYS1Avg, setSYS1Med, tr("Systolic - Value Range"));
	prepareBarChart(chartView_2, chartDIA1, seriesDIA1, setDIA1Min, setDIA1Max, setDIA1Avg, setDIA1Med, tr("Diastolic - Value Range"));
	prepareBarChart(chartView_3, chartBPM1, seriesBPM1, setBPM1Min, setBPM1Max, setBPM1Avg, setBPM1Med, tr("Heartrate - Value Range"));

	preparePieChart(chartView_4, chartSYS2, seriesSYS2, tr("Systolic - Target Area"));
	preparePieChart(chartView_5, chartDIA2, seriesDIA2, tr("Diastolic - Target Area"));
	preparePieChart(chartView_6, chartBPM2, seriesBPM2, tr("Heartrate - Target Area"));

	connect(toolbarDateTime, &QDateTimeEdit::dateTimeChanged, this, &MainWindow::toolbarDateTimeChanged);
	connect(axisXBP, &QDateTimeAxis::rangeChanged, this, &MainWindow::chartAxisXRangeChanged);
	connect(scatterSeriesSYS, &QScatterSeries::clicked, this, &MainWindow::chartSeriesClicked);
	connect(scatterSeriesDIA, &QScatterSeries::clicked, this, &MainWindow::chartSeriesClicked);
	connect(scatterSeriesBPM, &QScatterSeries::clicked, this, &MainWindow::chartSeriesClicked);
	connect(scatterSeriesIHB, &QScatterSeries::clicked, this, &MainWindow::chartSeriesClicked);
	connect(scatterSeriesMOV, &QScatterSeries::clicked, this, &MainWindow::chartSeriesClicked);
	connect(scatterSeriesSYS, &QScatterSeries::hovered, this, &MainWindow::chartSeriesHovered);
	connect(scatterSeriesDIA, &QScatterSeries::hovered, this, &MainWindow::chartSeriesHovered);
	connect(scatterSeriesBPM, &QScatterSeries::hovered, this, &MainWindow::chartSeriesHovered);
	connect(scatterSeriesIHB, &QScatterSeries::hovered, this, &MainWindow::chartSeriesHovered);
	connect(scatterSeriesMOV, &QScatterSeries::hovered, this, &MainWindow::chartSeriesHovered);
	connect(seriesSYS1, &QBarSeries::clicked, this, &MainWindow::barSeriesClicked);
	connect(seriesDIA1, &QBarSeries::clicked, this, &MainWindow::barSeriesClicked);
	connect(seriesBPM1, &QBarSeries::clicked, this, &MainWindow::barSeriesClicked);
	connect(seriesSYS1, &QBarSeries::hovered, this, &MainWindow::barSeriesHovered);
	connect(seriesDIA1, &QBarSeries::hovered, this, &MainWindow::barSeriesHovered);
	connect(seriesBPM1, &QBarSeries::hovered, this, &MainWindow::barSeriesHovered);
	connect(seriesSYS2, &QPieSeries::clicked, this, &MainWindow::pieSeriesClicked);
	connect(seriesDIA2, &QPieSeries::clicked, this, &MainWindow::pieSeriesClicked);
	connect(seriesBPM2, &QPieSeries::clicked, this, &MainWindow::pieSeriesClicked);
	connect(seriesSYS2, &QPieSeries::hovered, this, &MainWindow::pieSeriesHovered);
	connect(seriesDIA2, &QPieSeries::hovered, this, &MainWindow::pieSeriesHovered);
	connect(seriesBPM2, &QPieSeries::hovered, this, &MainWindow::pieSeriesHovered);
	connect(tableWidget, &QTableWidget::itemChanged, this, &MainWindow::tableItemChanged);
	connect(chartViewBP, &QChartView::customContextMenuRequested, this, &MainWindow::customContextMenuRequestedChart);
	connect(chartViewHR, &QChartView::customContextMenuRequested, this, &MainWindow::customContextMenuRequestedChart);
	connect(tableWidget, &QTableWidget::customContextMenuRequested, this, &MainWindow::customContextMenuRequestedTable);
	connect(chartView_1, &QChartView::customContextMenuRequested, this, &MainWindow::customContextMenuRequestedStats);
	connect(chartView_2, &QChartView::customContextMenuRequested, this, &MainWindow::customContextMenuRequestedStats);
	connect(chartView_3, &QChartView::customContextMenuRequested, this, &MainWindow::customContextMenuRequestedStats);
	connect(chartView_4, &QChartView::customContextMenuRequested, this, &MainWindow::customContextMenuRequestedStats);
	connect(chartView_5, &QChartView::customContextMenuRequested, this, &MainWindow::customContextMenuRequestedStats);
	connect(chartView_6, &QChartView::customContextMenuRequested, this, &MainWindow::customContextMenuRequestedStats);

	tableWidget->setItemDelegateForColumn(0, new tableStyledItemDelegate);
	tableWidget->setItemDelegateForColumn(1, new tableStyledItemDelegate);
	tableWidget->setItemDelegateForColumn(2, new tableStyledItemDelegate);
	tableWidget->setItemDelegateForColumn(3, new tableStyledItemDelegate);
	tableWidget->setItemDelegateForColumn(5, new tableStyledItemDelegate);
	tableWidget->setItemDelegateForColumn(6, new tableStyledItemDelegate);
	tableWidget->setItemDelegateForColumn(7, new tableStyledItemDelegate);
	tableWidget->setItemDelegateForColumn(8, new tableStyledItemDelegate);
	tableWidget->setItemDelegateForColumn(9, new tableStyledItemDelegate);
	tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

	scanPlugins();
	scanLanguages();
	scanStyles();
	scanThemes();

	if(QFile::exists(settings.database.location))
	{
		importFromSQL(settings.database.location, nullptr, nullptr);

		if(database[0].count())
		{
			toolbarDateTime->setDateTime(QDateTime::fromMSecsSinceEpoch(database[0].last().dts));
		}
	}

	restoreGeometry(settings.geometry);

	actionTimeMode->setChecked(settings.mode);

	if(settings.mode)
	{
		widgetSlide->hide();

		buttonGroup2->button(settings.slide)->blockSignals(true);
	}
	else
	{
		widgetRange->hide();

		buttonGroup1->button(settings.range)->blockSignals(true);

		toolbarDateTime->setEnabled(false);
		toolButton_prev->setEnabled(false);
		toolButton_next->setEnabled(false);
	}

	buttonGroup1->button(settings.range)->setChecked(true);
	buttonGroup2->button(settings.slide)->setChecked(true);
	emit buttonGroup1->checkedButton()->toggled(true);
	emit buttonGroup2->checkedButton()->toggled(true);
	buttonGroup1->button(settings.range)->blockSignals(false);
	buttonGroup2->button(settings.slide)->blockSignals(false);

	QTimer::singleShot(1, this, &MainWindow::initAfterShown);
}

void MainWindow::initAfterShown()
{
	lcdDigitWidth = lcd->width();

	updateLCD();

	if(settings.update.autostart)
	{
		new DialogUpdate(this, settings.update.notification);
	}
}

QByteArray MainWindow::encryptString(QString input)
{
	QByteArray ba = input.toUtf8();

	for(int i = 0; i < ba.length(); i++)
	{
		quint8 byte = ba[i];

		ba[i] = (byte << 1) | (byte >> 7);
	}

	return ba;
}

QString MainWindow::decryptString(QByteArray input)
{
	for(int i = 0; i < input.length(); i++)
	{
		quint8 byte = input[i];

		input[i] = (byte >> 1) | (byte << 7);
	}

	return QString(input);
}

void MainWindow::readSettings()
{
	QSettings ini(FILE_SETTINGS, QSettings::IniFormat);

	ini.setIniCodec("UTF-8");

	settings.language = ini.value("Language", "English").toString();
	settings.style = ini.value("Style", "Fusion").toString();
	settings.theme = ini.value("Theme", "System").toString();
	settings.geometry = ini.value("Geometry", QByteArray()).toByteArray();
	settings.mode = ini.value("TimeMode", 1).toInt();
	settings.range = ini.value("RangeIndex", -7).toInt();
	settings.slide = ini.value("SlideIndex", -6).toInt();
	settings.imp = ini.value("Import", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString();
	settings.exp = ini.value("Export", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString();
	settings.qtbase = ini.value("QtBaseWarning", true).toBool();

	ini.beginGroup("Database");
	settings.database.location = ini.value("Location", FILE_DATABASE).toString();
	settings.database.encryption = ini.value("Encryption", false).toBool();
	settings.database.password = decryptString(ini.value("Password", "").toByteArray());
	ini.endGroup();

	ini.beginGroup("User1");
	settings.user[0].gender = ini.value("Gender", "Male").toString();
	settings.user[0].agegroup = ini.value("AgeGroup", 3).toInt();
	settings.user[0].name = ini.value("Name", tr("User 1")).toString();
	settings.user[0].addition = ini.value("Addition", false).toBool();
	settings.user[0].birth = ini.value("Birth", 0).toInt();
	settings.user[0].height = ini.value("Height", 0).toInt();
	settings.user[0].weight = ini.value("Weight", 0).toInt();
	ini.endGroup();

	ini.beginGroup("User2");
	settings.user[1].gender = ini.value("Gender", "Female").toString();
	settings.user[1].agegroup = ini.value("AgeGroup", 3).toInt();
	settings.user[1].name = ini.value("Name", tr("User 2")).toString();
	settings.user[1].addition = ini.value("Addition", false).toBool();
	settings.user[1].birth = ini.value("Birth", 0).toInt();
	settings.user[1].height = ini.value("Height", 0).toInt();
	settings.user[1].weight = ini.value("Weight", 0).toInt();
	ini.endGroup();

	ini.beginGroup("Device");
	settings.device.plugin = ini.value("Plugin", "").toString();
	ini.endGroup();

	ini.beginGroup("Chart");
	settings.chart.dynamic = ini.value("Dynamic", true).toBool();
	settings.chart.colored = ini.value("Colored", true).toBool();
	settings.chart.symbols = ini.value("Symbols", true).toBool();
	settings.chart.lines = ini.value("Lines", true).toBool();
	settings.chart.heartrate = ini.value("Heartrate", true).toBool();
	settings.chart.hrsheet = ini.value("HRSheet", false).toBool();
	settings.chart.range[0].sys_max = ini.value("SYSMax1", SYSMAX).toInt();
	settings.chart.range[0].sys_min = ini.value("SYSMin1", SYSMIN).toInt();
	settings.chart.range[0].dia_max = ini.value("DIAMax1", DIAMAX).toInt();
	settings.chart.range[0].dia_min = ini.value("DIAMin1", DIAMIN).toInt();
	settings.chart.range[0].bpm_max = ini.value("BPMMax1", BPMMAX).toInt();
	settings.chart.range[0].bpm_min = ini.value("BPMMin1", BPMMIN).toInt();
	settings.chart.range[1].sys_max = ini.value("SYSMax2", SYSMAX).toInt();
	settings.chart.range[1].sys_min = ini.value("SYSMin2", SYSMIN).toInt();
	settings.chart.range[1].dia_max = ini.value("DIAMax2", DIAMAX).toInt();
	settings.chart.range[1].dia_min = ini.value("DIAMin2", DIAMIN).toInt();
	settings.chart.range[1].bpm_max = ini.value("BPMMax2", BPMMAX).toInt();
	settings.chart.range[1].bpm_min = ini.value("BPMMin2", BPMMIN).toInt();
	settings.chart.symbolsize = ini.value("SymbolSize", 24).toInt();
	settings.chart.linewidth = ini.value("LineWidth", 3).toInt();
	ini.endGroup();

	ini.beginGroup("Table");
	settings.table[0].warnsys = ini.value("WarnSYS1", WRNSYS).toInt();
	settings.table[0].warndia = ini.value("WarnDIA1", WRNDIA).toInt();
	settings.table[0].warnppr = ini.value("WarnPPR1", WRNPPR).toInt();
	settings.table[0].warnbpm = ini.value("WarnBPM1", WRNBPM).toInt();
	settings.table[1].warnsys = ini.value("WarnSYS2", WRNSYS).toInt();
	settings.table[1].warndia = ini.value("WarnDIA2", WRNDIA).toInt();
	settings.table[1].warnppr = ini.value("WarnPPR2", WRNPPR).toInt();
	settings.table[1].warnbpm = ini.value("WarnBPM2", WRNBPM).toInt();
	ini.endGroup();

	ini.beginGroup("Stats");
	settings.stats.median = ini.value("Median", true).toBool();
	settings.stats.legend = ini.value("Legend", true).toBool();
	ini.endGroup();

	ini.beginGroup("E-Mail");
	settings.email.address = ini.value("Address", "e-m@il.net").toString();
	settings.email.subject = ini.value("Subject", tr("Blood Pressure Report")).toString();
	settings.email.message = ini.value("Message", tr("Dear Dr. House,\n\nplease find attached my blood pressure data for this month.\n\nBest regards,\n$USER\n$CHART$TABLE$STATS")).toString();
	ini.endGroup();

	ini.beginGroup("Update");
	settings.update.autostart = ini.value("Autostart", true).toBool();
	settings.update.notification = ini.value("Notification", true).toBool();
	ini.endGroup();
}

void MainWindow::saveSettings()
{
	QSettings ini(FILE_SETTINGS, QSettings::IniFormat);

	ini.setIniCodec("UTF-8");

	ini.setValue("Language", settings.language);
	ini.setValue("Style", settings.style);
	ini.setValue("Theme", settings.theme);
	ini.setValue("Geometry", settings.geometry);
	ini.setValue("TimeMode", settings.mode);
	ini.setValue("RangeIndex", settings.range);
	ini.setValue("SlideIndex", settings.slide);
	ini.setValue("Import", settings.imp);
	ini.setValue("Export", settings.exp);
	ini.setValue("QtBaseWarning", settings.qtbase);

	ini.beginGroup("Database");
	ini.setValue("Location", settings.database.location);
	ini.setValue("Encryption", settings.database.encryption);
	ini.setValue("Password", encryptString(settings.database.password));
	ini.endGroup();

	ini.beginGroup("User1");
	ini.setValue("Gender", settings.user[0].gender);
	ini.setValue("AgeGroup", settings.user[0].agegroup);
	ini.setValue("Name", settings.user[0].name);
	ini.setValue("Addition", settings.user[0].addition);
	ini.setValue("Birth", settings.user[0].birth);
	ini.setValue("Height", settings.user[0].height);
	ini.setValue("Weight", settings.user[0].weight);
	ini.endGroup();

	ini.beginGroup("User2");
	ini.setValue("Gender", settings.user[1].gender);
	ini.setValue("AgeGroup", settings.user[1].agegroup);
	ini.setValue("Name", settings.user[1].name);
	ini.setValue("Addition", settings.user[1].addition);
	ini.setValue("Birth", settings.user[1].birth);
	ini.setValue("Height", settings.user[1].height);
	ini.setValue("Weight", settings.user[1].weight);
	ini.endGroup();

	ini.beginGroup("Device");
	ini.setValue("Plugin", settings.device.plugin);
	ini.endGroup();

	ini.beginGroup("Chart");
	ini.setValue("Dynamic", settings.chart.dynamic);
	ini.setValue("Colored", settings.chart.colored);
	ini.setValue("Symbols", settings.chart.symbols);
	ini.setValue("Lines", settings.chart.lines);
	ini.setValue("Heartrate", settings.chart.heartrate);
	ini.setValue("HRSheet", settings.chart.hrsheet);
	ini.setValue("SYSMax1", settings.chart.range[0].sys_max);
	ini.setValue("SYSMin1", settings.chart.range[0].sys_min);
	ini.setValue("DIAMax1", settings.chart.range[0].dia_max);
	ini.setValue("DIAMin1", settings.chart.range[0].dia_min);
	ini.setValue("BPMMax1", settings.chart.range[0].bpm_max);
	ini.setValue("BPMMin1", settings.chart.range[0].bpm_min);
	ini.setValue("SYSMax2", settings.chart.range[1].sys_max);
	ini.setValue("SYSMin2", settings.chart.range[1].sys_min);
	ini.setValue("DIAMax2", settings.chart.range[1].dia_max);
	ini.setValue("DIAMin2", settings.chart.range[1].dia_min);
	ini.setValue("BPMMax2", settings.chart.range[1].bpm_max);
	ini.setValue("BPMMin2", settings.chart.range[1].bpm_min);
	ini.setValue("SymbolSize", settings.chart.symbolsize);
	ini.setValue("LineWidth", settings.chart.linewidth);
	ini.endGroup();

	ini.beginGroup("Table");
	ini.setValue("WarnSYS1", settings.table[0].warnsys);
	ini.setValue("WarnDIA1", settings.table[0].warndia);
	ini.setValue("WarnPPR1", settings.table[0].warnppr);
	ini.setValue("WarnBPM1", settings.table[0].warnbpm);
	ini.setValue("WarnSYS2", settings.table[1].warnsys);
	ini.setValue("WarnDIA2", settings.table[1].warndia);
	ini.setValue("WarnPPR2", settings.table[1].warnppr);
	ini.setValue("WarnBPM2", settings.table[1].warnbpm);
	ini.endGroup();

	ini.beginGroup("Stats");
	ini.setValue("Median", settings.stats.median);
	ini.setValue("Legend", settings.stats.legend);
	ini.endGroup();

	ini.beginGroup("E-Mail");
	ini.setValue("Address", settings.email.address);
	ini.setValue("Subject", settings.email.subject);
	ini.setValue("Message", settings.email.message);
	ini.endGroup();

	ini.beginGroup("Update");
	ini.setValue("Autostart", settings.update.autostart);
	ini.setValue("Notification", settings.update.notification);
	ini.endGroup();
}

void MainWindow::updateLCD()
{
	lcd->setDigitCount(QString::number(database[view.user].count()).length());

	lcd->setMinimumWidth(lcdDigitWidth * QString::number(database[view.user].count()).length());

	lcd->display(database[view.user].count());
}

void MainWindow::calcView()
{
	view.init = timespan.toMSecsSinceEpoch();
	view.exit = view.init;
	QVector<int> viewSYS, viewDIA, viewBPM;
	int irregular = 0;
	int movement = 0;

	if(actionTimeMode->isChecked())
	{
		if(toolButton_15m->isChecked())
		{
			view.exit += 900000;		// 15*60 * 1000
		}
		else if(toolButton_30m->isChecked())
		{
			view.exit += 1800000;		// 30*60 * 1000
		}
		else if(toolButton_hourly->isChecked())
		{
			view.exit += 3600000;		// 60*60 * 1000
		}
		else if(toolButton_6h->isChecked())
		{
			view.exit += 21600000;		// 6*60*60 * 1000
		}
		else if(toolButton_12h->isChecked())
		{
			view.exit += 43200000;		// 12*60*60 * 1000
		}
		else if(toolButton_daily->isChecked())
		{
			view.exit += 86400000;		// 24*60*60 * 1000
		}
		else if(toolButton_weekly->isChecked())
		{
			view.exit += 604800000;		// 7*24*60*60 * 1000
		}
		else if(toolButton_monthly->isChecked())
		{
			view.exit += static_cast<qint64>(timespan.date().daysInMonth()) * static_cast<qint64>(24*60*60 * 1000);
		}
		else if(toolButton_quarterly->isChecked())
		{
			view.exit += (timespan.date().daysInMonth() + timespan.date().addMonths(1).daysInMonth() + timespan.date().addMonths(2).daysInMonth()) * static_cast<qint64>(24*60*60 * 1000);
		}
		else if(toolButton_halfyearly->isChecked())
		{
			view.exit += (timespan.date().daysInMonth() + timespan.date().addMonths(1).daysInMonth() + timespan.date().addMonths(2).daysInMonth() + timespan.date().addMonths(3).daysInMonth() + timespan.date().addMonths(4).daysInMonth() + timespan.date().addMonths(5).daysInMonth()) * static_cast<qint64>(24*60*60 * 1000);
		}
		else if(toolButton_yearly->isChecked())
		{
			view.exit += static_cast<qint64>(timespan.date().daysInYear()) * static_cast<qint64>(24*60*60 * 1000);
		}
	}
	else
	{
		if(toolButton_days7->isChecked())
		{
			view.exit += 604800000;		// 7*24*60*60 * 1000
		}
		else if(toolButton_days14->isChecked())
		{
			view.exit += 1209600000;	// 14*24*60*60 * 1000
		}
		else if(toolButton_days21->isChecked())
		{
			view.exit += 1814400000;	// 21*24*60*60 * 1000
		}
		else if(toolButton_days28->isChecked())
		{
			view.exit += 2419200000;	// 28*24*60*60 * 1000
		}
		else if(toolButton_months3->isChecked())
		{
			view.exit += 7776000000;	// 3*30*24*60*60 * 1000
		}
		else if(toolButton_months6->isChecked())
		{
			view.exit += 15552000000;	// 6*30*24*60*60 * 1000
		}
		else if(toolButton_months9->isChecked())
		{
			view.exit += 23328000000;	// 9*30*24*60*60 * 1000
		}
		else if(toolButton_months12->isChecked())
		{
			view.exit += 31104000000;	// 12*30*24*60*60 * 1000
		}
		else if(toolButton_all->isChecked())
		{
			view.exit += timespan.msecsTo(QDateTime::currentDateTime());
		}
	}

	statusbarLabel->setText(QString("   %1 - %2   ").arg(QDateTime::fromMSecsSinceEpoch(view.init).toString("dd.MM.yyyy hh:mm"), QDateTime::fromMSecsSinceEpoch(view.exit).toString("dd.MM.yyyy hh:mm")));

	view.data.clear();

	viewSYS.clear();
	viewDIA.clear();
	viewBPM.clear();

	view.SYSmin = 0;
	view.DIAmin = 0;
	view.BPMmin = 0;
	view.SYSmax = 0;
	view.DIAmax = 0;
	view.BPMmax = 0;
	view.SYSavg = 0;
	view.DIAavg = 0;
	view.BPMavg = 0;
	view.SYSmed = 0;
	view.DIAmed = 0;
	view.BPMmed = 0;

	view.SYSR1 = 0;
	view.SYSR2 = 0;
	view.SYSR3 = 0;
	view.SYSR4 = 0;
	view.SYSR5 = 0;
	view.SYSR6 = 0;
	view.SYSR7 = 0;
	view.DIAR1 = 0;
	view.DIAR2 = 0;
	view.DIAR3 = 0;
	view.DIAR4 = 0;
	view.DIAR5 = 0;
	view.DIAR6 = 0;
	view.DIAR7 = 0;
	view.BPMR1 = 0;
	view.BPMR2 = 0;
	view.BPMR3 = 0;
	view.BPMR4 = 0;
	view.BPMR5 = 0;
	view.BPMR6 = 0;
	view.BPMR7 = 0;

	view.records = 0;

	for(int i = 0; i < database[view.user].count(); i++)
	{
		if((database[view.user].at(i).dts >= view.init && database[view.user].at(i).dts < view.exit) && !database[view.user].at(i).inv)
		{
			view.data.append(database[view.user].at(i));

			viewSYS.append(database[view.user].at(i).sys);
			viewDIA.append(database[view.user].at(i).dia);
			viewBPM.append(database[view.user].at(i).bpm);

			if(database[view.user].at(i).ihb)
			{
				irregular++;
			}

			if(database[view.user].at(i).mov)
			{
				movement++;
			}

			view.records++;
		}
	}

	if(view.records)
	{
		std::sort(viewSYS.begin(), viewSYS.end(), [](int a, int b) { return a < b; });
		std::sort(viewDIA.begin(), viewDIA.end(), [](int a, int b) { return a < b; });
		std::sort(viewBPM.begin(), viewBPM.end(), [](int a, int b) { return a < b; });

		view.SYSmin = viewSYS.first();
		view.SYSmax = viewSYS.last();
		view.DIAmin = viewDIA.first();
		view.DIAmax = viewDIA.last();
		view.BPMmin = viewBPM.first();
		view.BPMmax = viewBPM.last();

		for(int i = 0; i < view.records; i++)
		{
			view.SYSavg += viewSYS.at(i);
			view.DIAavg += viewDIA.at(i);
			view.BPMavg += viewBPM.at(i);

			if(viewSYS.at(i) < SYS_R1)
			{
				view.SYSR1++;
			}
			else if(viewSYS.at(i) < SYS_R2)
			{
				view.SYSR2++;
			}
			else if(viewSYS.at(i) < SYS_R3)
			{
				view.SYSR3++;
			}
			else if(viewSYS.at(i) < SYS_R4)
			{
				view.SYSR4++;
			}
			else if(viewSYS.at(i) < SYS_R5)
			{
				view.SYSR5++;
			}
			else if(viewSYS.at(i) < SYS_R6)
			{
				view.SYSR6++;
			}
			else
			{
				view.SYSR7++;
			}

			if(viewDIA.at(i) < DIA_R1)
			{
				view.DIAR1++;
			}
			else if(viewDIA.at(i) < DIA_R2)
			{
				view.DIAR2++;
			}
			else if(viewDIA.at(i) < DIA_R3)
			{
				view.DIAR3++;
			}
			else if(viewDIA.at(i) < DIA_R4)
			{
				view.DIAR4++;
			}
			else if(viewDIA.at(i) < DIA_R5)
			{
				view.DIAR5++;
			}
			else if(viewDIA.at(i) < DIA_R6)
			{
				view.DIAR6++;
			}
			else
			{
				view.DIAR7++;
			}

			if(viewBPM.at(i) < bpm_target_areas[settings.user[view.user].gender == "Male" ? 0 : 1][BPM_R1][settings.user[view.user].agegroup])
			{
				view.BPMR1++;
			}
			else if(viewBPM.at(i) < bpm_target_areas[settings.user[view.user].gender == "Male" ? 0 : 1][BPM_R2][settings.user[view.user].agegroup])
			{
				view.BPMR2++;
			}
			else if(viewBPM.at(i) < bpm_target_areas[settings.user[view.user].gender == "Male" ? 0 : 1][BPM_R3][settings.user[view.user].agegroup])
			{
				view.BPMR3++;
			}
			else if(viewBPM.at(i) < bpm_target_areas[settings.user[view.user].gender == "Male" ? 0 : 1][BPM_R4][settings.user[view.user].agegroup])
			{
				view.BPMR4++;
			}
			else if(viewBPM.at(i) < bpm_target_areas[settings.user[view.user].gender == "Male" ? 0 : 1][BPM_R5][settings.user[view.user].agegroup])
			{
				view.BPMR5++;
			}
			else if(viewBPM.at(i) < bpm_target_areas[settings.user[view.user].gender == "Male" ? 0 : 1][BPM_R6][settings.user[view.user].agegroup])
			{
				view.BPMR6++;
			}
			else
			{
				view.BPMR7++;
			}
		}

		view.SYSavg /= view.records;
		view.DIAavg /= view.records;
		view.BPMavg /= view.records;

		view.SYSmed = viewSYS.at(view.records / 2);
		view.DIAmed = viewDIA.at(view.records / 2);
		view.BPMmed = viewBPM.at(view.records / 2);

		label_measurements->setText(tr("Measurements : %1  |  Irregular : %2  |  Movement : %3").arg(view.records).arg(irregular).arg(movement));
		label_average->setText(QString(tr("SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6")).arg(view.SYSavg).arg(view.DIAavg).arg(view.BPMavg).arg(view.SYSmed).arg(view.DIAmed).arg(view.BPMmed));
	}
	else
	{
		label_measurements->setText(tr("Measurements : 0  |  Irregular : 0  |  Movement : 0"));
		label_average->setText(QString(tr("SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0")));
	}

	if(tabWidget->currentIndex() == 0)
	{
		showChart();
	}
	else if(tabWidget->currentIndex() == 1)
	{
		showTable();
	}
	else if(tabWidget->currentIndex() == 2)
	{
		showStats();
	}
}

void MainWindow::showChart()
{
	lineSeriesSYS->clear();
	lineSeriesDIA->clear();
	lineSeriesBPM->clear();

	scatterSeriesSYS->clear();
	scatterSeriesDIA->clear();
	scatterSeriesBPM->clear();
	scatterSeriesIHB->clear();
	scatterSeriesMOV->clear();

	for(int i = 0; i < view.data.count(); i++)
	{
		if(view.data.at(i).dts >= view.init && view.data.at(i).dts < view.exit)
		{
			lineSeriesSYS->append(view.data.at(i).dts, view.data.at(i).sys);
			lineSeriesDIA->append(view.data.at(i).dts, view.data.at(i).dia);
			lineSeriesBPM->append(view.data.at(i).dts, view.data.at(i).bpm);

			if(settings.chart.symbols)
			{
				scatterSeriesSYS->append(view.data.at(i).dts, view.data.at(i).sys);
				scatterSeriesDIA->append(view.data.at(i).dts, view.data.at(i).dia);
				scatterSeriesBPM->append(view.data.at(i).dts, view.data.at(i).bpm);

				if(view.data.at(i).ihb)
				{
					scatterSeriesIHB->append(view.data.at(i).dts, view.data.at(i).bpm);
				}

				if(view.data.at(i).mov)
				{
					scatterSeriesMOV->append(view.data.at(i).dts, view.data.at(i).bpm);
				}
			}

			lineSeriesSYS->setPen(QPen(QColor(syscolors[3]), settings.chart.linewidth, settings.chart.lines ? Qt::SolidLine : Qt::NoPen, Qt::RoundCap, Qt::RoundJoin));
			lineSeriesDIA->setPen(QPen(QColor(diacolors[3]), settings.chart.linewidth, settings.chart.lines ? Qt::SolidLine : Qt::NoPen, Qt::RoundCap, Qt::RoundJoin));
			lineSeriesBPM->setPen(QPen(QColor(bpmcolors[3]), settings.chart.linewidth, settings.chart.lines ? Qt::SolidLine : Qt::NoPen, Qt::RoundCap, Qt::RoundJoin));
		}
	}

	if(settings.chart.colored)
	{
		lineSeriesUpperSYS->replace(0, axisXBP->min().toMSecsSinceEpoch(), lineSeriesUpperSYS->at(0).y());
		lineSeriesUpperSYS->replace(1, axisXBP->max().toMSecsSinceEpoch(), lineSeriesUpperSYS->at(1).y());
		lineSeriesLowerSYS->replace(0, axisXBP->min().toMSecsSinceEpoch(), lineSeriesLowerSYS->at(0).y());
		lineSeriesLowerSYS->replace(1, axisXBP->max().toMSecsSinceEpoch(), lineSeriesLowerSYS->at(1).y());

		lineSeriesUpperDIA->replace(0, axisXBP->min().toMSecsSinceEpoch(), lineSeriesUpperDIA->at(0).y());
		lineSeriesUpperDIA->replace(1, axisXBP->max().toMSecsSinceEpoch(), lineSeriesUpperDIA->at(1).y());
		lineSeriesLowerDIA->replace(0, axisXBP->min().toMSecsSinceEpoch(), lineSeriesLowerDIA->at(0).y());
		lineSeriesLowerDIA->replace(1, axisXBP->max().toMSecsSinceEpoch(), lineSeriesLowerDIA->at(1).y());

		lineSeriesUpperBPM->replace(0, axisXHR->min().toMSecsSinceEpoch(), lineSeriesUpperBPM->at(0).y());
		lineSeriesUpperBPM->replace(1, axisXHR->max().toMSecsSinceEpoch(), lineSeriesUpperBPM->at(1).y());
		lineSeriesLowerBPM->replace(0, axisXHR->min().toMSecsSinceEpoch(), lineSeriesLowerBPM->at(0).y());
		lineSeriesLowerBPM->replace(1, axisXHR->max().toMSecsSinceEpoch(), lineSeriesLowerBPM->at(1).y());

		areaSeriesSYS->show();
		areaSeriesDIA->show();
		areaSeriesBPM->show();

		chartMainBP->legend()->markers(areaSeriesSYS).at(0)->setVisible(false);
		chartMainBP->legend()->markers(areaSeriesDIA).at(0)->setVisible(false);
		chartMainHR->legend()->markers(areaSeriesBPM).at(0)->setVisible(false);
	}
	else
	{
		areaSeriesSYS->hide();
		areaSeriesDIA->hide();
		areaSeriesBPM->hide();
	}

	if(settings.chart.dynamic)
	{
		int maxBP = view.SYSmax + (10 - (view.SYSmax % 10));
		int minBP = view.DIAmin - (view.DIAmin % 10);
		int maxHR = view.BPMmax + (10 - (view.BPMmax % 10));
		int minHR = view.BPMmin - (view.BPMmin % 10);

		if(maxBP - view.SYSmax < 5)
		{
			maxBP += 10;
		}

		if(view.DIAmin - minBP < 5)
		{
			minBP -= 10;
		}

		if(minBP > 60 || view.DIAmin == 0)
		{
			minBP = 60;
		}
		else if(minBP < 0)
		{
			minBP = 0;
		}

		if(maxHR - view.BPMmax < 5)
		{
			maxHR += 10;
		}

		if(view.BPMmin - minBP < 5)
		{
			minHR -= 10;
		}

		if(minHR > 50 || view.BPMmin == 0)
		{
			minHR = 50;
		}
		else if(minHR < 0)
		{
			minHR = 0;
		}

		axisY1BP->setTickCount(((maxBP - minBP) / 10) + 1);
		axisY2BP->setTickCount(((maxBP - minBP) / 10) + 1);
		axisY1HR->setTickCount(((maxHR - minHR) / 10) + 1);
		axisY2HR->setTickCount(((maxHR - minHR) / 10) + 1);

		axisY1BP->setRange(minBP, maxBP);
		axisY2BP->setRange(minBP, maxBP);
		axisY1HR->setRange(minHR, maxHR);
		axisY2HR->setRange(minHR, maxHR);
	}
	else
	{
		axisY1BP->setTickCount(((180 - 60) / 10) + 1);
		axisY2BP->setTickCount(((180 - 60) / 10) + 1);
		axisY1HR->setTickCount(((150 - 50) / 10) + 1);
		axisY2HR->setTickCount(((150 - 50) / 10) + 1);

		axisY1BP->setRange(60, 180);
		axisY2BP->setRange(60, 180);
		axisY1HR->setRange(50, 150);
		axisY2HR->setRange(50, 150);
	}

	settings.chart.heartrate ? chartViewHR->show() : chartViewHR->hide();
}

void MainWindow::showTable()
{
	QSignalBlocker blocker(tableWidget);

	tableWidget->clearContents();
	tableWidget->setRowCount(0);

	for(int i = 0; i < database[view.user].count(); i++)
	{
		if(database[view.user].at(i).dts >= view.init && database[view.user].at(i).dts < view.exit)
		{
			int row = tableWidget->rowCount();

			QTableWidgetItem *twiDate = new QTableWidgetItem();
			QTableWidgetItem *twiTime = new QTableWidgetItem();
			QTableWidgetItem *twiSys = new QTableWidgetItem();
			QTableWidgetItem *twiDia = new QTableWidgetItem();
			QTableWidgetItem *twiPpr = new QTableWidgetItem();
			QTableWidgetItem *twiBpm = new QTableWidgetItem();
			QTableWidgetItem *twiIhb = new QTableWidgetItem();
			QTableWidgetItem *twiMov = new QTableWidgetItem();
			QTableWidgetItem *twiInv = new QTableWidgetItem();
			QTableWidgetItem *twiMsg = new QTableWidgetItem();

			twiDate->setData(Qt::DisplayRole, QDateTime::fromMSecsSinceEpoch(database[view.user].at(i).dts).date());
			twiTime->setData(Qt::DisplayRole, QDateTime::fromMSecsSinceEpoch(database[view.user].at(i).dts).time());
			twiSys->setData(Qt::DisplayRole, database[view.user].at(i).sys);
			twiDia->setData(Qt::DisplayRole, database[view.user].at(i).dia);
			twiPpr->setData(Qt::DisplayRole, database[view.user].at(i).sys - database[view.user].at(i).dia);
			twiBpm->setData(Qt::DisplayRole, database[view.user].at(i).bpm);
			twiIhb->setData(Qt::DisplayRole, database[view.user].at(i).ihb ? 1 : 0);
			twiMov->setData(Qt::DisplayRole, database[view.user].at(i).mov ? 1 : 0);
			twiInv->setData(Qt::DisplayRole, database[view.user].at(i).inv ? 1 : 0);
			twiMsg->setData(Qt::DisplayRole, database[view.user].at(i).msg);

			twiDate->setFlags(twiDate->flags() & (~Qt::ItemIsEditable));
			twiTime->setFlags(twiTime->flags() & (~Qt::ItemIsEditable));
			twiPpr->setFlags(twiPpr->flags() & (~Qt::ItemIsEditable));

			twiDate->setTextAlignment(Qt::AlignCenter);
			twiTime->setTextAlignment(Qt::AlignCenter);
			twiSys->setTextAlignment(Qt::AlignCenter);
			twiDia->setTextAlignment(Qt::AlignCenter);
			twiPpr->setTextAlignment(Qt::AlignCenter);
			twiBpm->setTextAlignment(Qt::AlignCenter);
			twiIhb->setTextAlignment(Qt::AlignCenter);
			twiMov->setTextAlignment(Qt::AlignCenter);
			twiInv->setTextAlignment(Qt::AlignCenter);
			twiMsg->setTextAlignment(Qt::AlignCenter);

			if(database[view.user].at(i).sys >= settings.table[view.user].warnsys) twiSys->setForeground(Qt::red);
			if(database[view.user].at(i).dia >= settings.table[view.user].warndia) twiDia->setForeground(Qt::red);
			if(database[view.user].at(i).sys - database[view.user].at(i).dia > settings.table[view.user].warnppr) twiPpr->setForeground(Qt::red);
			if(database[view.user].at(i).bpm >= settings.table[view.user].warnbpm) twiBpm->setForeground(Qt::red);
			if(database[view.user].at(i).ihb) twiIhb->setForeground(Qt::red);
			if(database[view.user].at(i).mov) twiMov->setForeground(Qt::red);

			tableWidget->insertRow(row);

			tableWidget->setItem(row, 0, twiDate);
			tableWidget->setItem(row, 1, twiTime);
			tableWidget->setItem(row, 2, twiSys);
			tableWidget->setItem(row, 3, twiDia);
			tableWidget->setItem(row, 4, twiPpr);
			tableWidget->setItem(row, 5, twiBpm);
			tableWidget->setItem(row, 6, twiIhb);
			tableWidget->setItem(row, 7, twiMov);
			tableWidget->setItem(row, 8, twiInv);
			tableWidget->setItem(row, 9, twiMsg);
		}
	}
}

void MainWindow::showStats()
{
	drawBarChart(chartSYS1, seriesSYS1, setSYS1Min, setSYS1Max, setSYS1Avg, setSYS1Med, view.records ? view.SYSmin : 0, view.records ? view.SYSmax : 0, view.SYSavg, view.SYSmed);
	drawBarChart(chartDIA1, seriesDIA1, setDIA1Min, setDIA1Max, setDIA1Avg, setDIA1Med, view.records ? view.DIAmin : 0, view.records ? view.DIAmax : 0, view.DIAavg, view.DIAmed);
	drawBarChart(chartBPM1, seriesBPM1, setBPM1Min, setBPM1Max, setBPM1Avg, setBPM1Med, view.records ? view.BPMmin : 0, view.records ? view.BPMmax : 0, view.BPMavg, view.BPMmed);

	drawPieChart(chartSYS2, seriesSYS2, view.SYSR1, view.SYSR2, view.SYSR3, view.SYSR4, view.SYSR5, view.SYSR6, view.SYSR7);
	drawPieChart(chartDIA2, seriesDIA2, view.DIAR1, view.DIAR2, view.DIAR3, view.DIAR4, view.DIAR5, view.DIAR6, view.DIAR7);
	drawPieChart(chartBPM2, seriesBPM2, view.BPMR1, view.BPMR2, view.BPMR3, view.BPMR4, view.BPMR5, view.BPMR6, view.BPMR7);
}

void MainWindow::prepareMainChart(QChart *chart, QChartView *view, QDateTimeAxis *x, QValueAxis *y1, QValueAxis *y2, int min, int max)
{
	y1->setRange(min, max);
	y1->setTickCount(((max - min) / 10) + 1);
	y1->setMinorTickCount(1);
	y1->setLabelFormat("%d");

	y2->setRange(min, max);
	y2->setTickCount(((max - min) / 10) + 1);
	y2->setMinorTickCount(1);
	y2->setLabelFormat("%d");

	chart->addAxis(x, Qt::AlignBottom);
	chart->addAxis(y1, Qt::AlignLeft);
	chart->addAxis(y2, Qt::AlignRight);
	chart->legend()->setMarkerShape(QLegend::MarkerShapeFromSeries);
	chart->setBackgroundRoundness(0);
	chart->setMargins(QMargins(0, 0, 0, 0));
	chart->layout()->setContentsMargins(0, 0, 0, 0);

	view->setRenderHint(QPainter::Antialiasing);
	view->setChart(chart);
}

void MainWindow::prepareAreaChart(QChart *chart, QDateTimeAxis *x, QValueAxis *y, QAreaSeries *area, QLineSeries *lower, QLineSeries *upper, QColor color, int min, int max)
{
	lower->append(0, min);
	lower->append(1, min);
	upper->append(0, max);
	upper->append(1, max);

	color.setAlpha(32);

	area->setPen(QPen(Qt::lightGray, 1));
	area->setColor(color);

	chart->addSeries(area);
	chart->legend()->markers(area).at(0)->setVisible(false);

	area->attachAxis(x);
	area->attachAxis(y);
}

void MainWindow::prepareLineChart(QChart *chart, QDateTimeAxis *x, QValueAxis *y, QLineSeries *series, QColor color, QString title)
{
	series->setPen(QPen(color, settings.chart.linewidth, settings.chart.lines ? Qt::SolidLine : Qt::NoPen, Qt::RoundCap, Qt::RoundJoin));
	series->setName(title);

	chart->addSeries(series);

	series->attachAxis(x);
	series->attachAxis(y);
}

void MainWindow::prepareScatterChart(QChart *chart, QDateTimeAxis *x, QValueAxis *y, QScatterSeries *series)
{
	QBrush brush;

	if(series == scatterSeriesSYS) brush = QBrush(QIcon(":/svg/systolic.svg").pixmap(settings.chart.symbolsize, settings.chart.symbolsize).toImage());
	if(series == scatterSeriesDIA) brush = QBrush(QIcon(":/svg/diastolic.svg").pixmap(settings.chart.symbolsize, settings.chart.symbolsize).toImage());
	if(series == scatterSeriesBPM) brush = QBrush(QIcon(":/svg/heart-normal.svg").pixmap(settings.chart.symbolsize, settings.chart.symbolsize).toImage());
	if(series == scatterSeriesIHB) brush = QBrush(QIcon(":/svg/heart-irregular.svg").pixmap(settings.chart.symbolsize, settings.chart.symbolsize).toImage());
	if(series == scatterSeriesMOV) brush = QBrush(QIcon(":/svg/heart-movement-overlay.svg").pixmap(settings.chart.symbolsize, settings.chart.symbolsize).toImage());

	series->setBrush(brush);
	series->setMarkerSize(settings.chart.symbolsize);
	series->setPen(QColor(Qt::transparent));

	chart->addSeries(series);
	chart->legend()->markers(series).at(0)->setVisible(false);

	series->attachAxis(x);
	series->attachAxis(y);
}

void MainWindow::prepareBarChart(QChartView *view, QChart *chart, QBarSeries *series, QBarSet *minSet, QBarSet *maxSet, QBarSet *avgSet, QBarSet *medSet, QString title)
{
	chart->setBackgroundRoundness(0);
	chart->setMargins(QMargins(0, 10, 0, 0));
	chart->layout()->setContentsMargins(1, 1, 1, 1);
	chart->setTitle(QString("<center><b><u>%1</u></b><br></center>").arg(title));
	chart->legend()->setAlignment(Qt::AlignBottom);

	minSet->setPen(QPen(Qt::black, 1));
	maxSet->setPen(QPen(Qt::black, 1));
	avgSet->setPen(QPen(Qt::black, 1));
	medSet->setPen(QPen(Qt::black, 1));

	if(chart == chartSYS1)
	{
		minSet->setColor(syscolors[0]);
		maxSet->setColor(syscolors[6]);
		avgSet->setColor(syscolors[3]);
		medSet->setColor(syscolors[3]);
	}
	else if(chart == chartDIA1)
	{
		minSet->setColor(diacolors[0]);
		maxSet->setColor(diacolors[6]);
		avgSet->setColor(diacolors[3]);
		medSet->setColor(diacolors[3]);
	}
	else if(chart == chartBPM1)
	{
		minSet->setColor(bpmcolors[0]);
		maxSet->setColor(bpmcolors[6]);
		avgSet->setColor(bpmcolors[3]);
		medSet->setColor(bpmcolors[3]);
	}

	minSet->append(200);
	maxSet->append(200);
	avgSet->append(200);
	medSet->append(200);

	series->append(minSet);
	series->append(maxSet);
	series->append(avgSet);
	series->append(medSet);

	series->setBarWidth(0.8);
	series->setLabelsVisible(true);

	chart->addSeries(series);

	view->setRenderHint(QPainter::Antialiasing);
	view->setChart(chart);
}

void MainWindow::preparePieChart(QChartView *view, QChart *chart, QPieSeries *series, QString title)
{
	chart->setBackgroundRoundness(0);
	chart->setMargins(QMargins(0, 10, 0, 0));
	chart->layout()->setContentsMargins(1, 1, 1, 1);

	chart->setTitle(QString("<center><b><u>%1</u></b><br></center>").arg(title));

	series->setPieSize(0.65);
	series->setHoleSize(0.2);

	chart->addSeries(series);

	chart->legend()->setMarkerShape(QLegend::MarkerShapeCircle);
	chart->legend()->setAlignment(Qt::AlignBottom);

	view->setRenderHint(QPainter::Antialiasing);
	view->setChart(chart);
}

void MainWindow::modifyChartRanges()
{
	lineSeriesLowerSYS->replace(0, 0, settings.chart.range[view.user].sys_min);
	lineSeriesLowerSYS->replace(1, 1, settings.chart.range[view.user].sys_min);
	lineSeriesUpperSYS->replace(0, 0, settings.chart.range[view.user].sys_max);
	lineSeriesUpperSYS->replace(1, 1, settings.chart.range[view.user].sys_max);

	lineSeriesLowerDIA->replace(0, 0, settings.chart.range[view.user].dia_min);
	lineSeriesLowerDIA->replace(1, 1, settings.chart.range[view.user].dia_min);
	lineSeriesUpperDIA->replace(0, 0, settings.chart.range[view.user].dia_max);
	lineSeriesUpperDIA->replace(1, 1, settings.chart.range[view.user].dia_max);

	lineSeriesLowerBPM->replace(0, 0, settings.chart.range[view.user].bpm_min);
	lineSeriesLowerBPM->replace(1, 1, settings.chart.range[view.user].bpm_min);
	lineSeriesUpperBPM->replace(0, 0, settings.chart.range[view.user].bpm_max);
	lineSeriesUpperBPM->replace(1, 1, settings.chart.range[view.user].bpm_max);
}

void MainWindow::drawBarChart(QChart *chart, QBarSeries *series, QBarSet *minSet, QBarSet *maxSet, QBarSet *avgSet, QBarSet *medSet, int min, int max, int avg, int med)
{
	chart->removeSeries(series);

	minSet->replace(0, min);
	maxSet->replace(0, max);
	avgSet->replace(0, avg);
	medSet->replace(0, med);

	series->append(settings.stats.median ? medSet : avgSet);
	series->take(settings.stats.median ? avgSet : medSet);

	max ? chart->legend()->show() : chart->legend()->hide();

	chart->addSeries(series);
}

void MainWindow::drawPieChart(QChart *chart, QPieSeries *series, int r1, int r2, int r3, int r4, int r5, int r6, int r7)
{
	int color = 0;

	chart->removeSeries(series);

	series->clear();

	if(r1) series->append("1", r1)->setPen(QPen(Qt::black, 1));
	if(r2) series->append("2", r2)->setPen(QPen(Qt::black, 1));
	if(r3) series->append("3", r3)->setPen(QPen(Qt::black, 1));
	if(r4) series->append("4", r4)->setPen(QPen(Qt::black, 1));
	if(r5) series->append("5", r5)->setPen(QPen(Qt::black, 1));
	if(r6) series->append("6", r6)->setPen(QPen(Qt::black, 1));
	if(r7) series->append("7", r7)->setPen(QPen(Qt::black, 1));

	chart->addSeries(series);

	series->setLabelsVisible(true);

	foreach(QPieSlice *slice, series->slices())
	{
		if(slice->label() == "1")      slice->setProperty("TargetArea", chart == chartBPM2 ? tr("Athlete") : tr("To Low"));
		else if(slice->label() == "2") slice->setProperty("TargetArea", chart == chartBPM2 ? tr("Excellent") : tr("Optimal"));
		else if(slice->label() == "3") slice->setProperty("TargetArea", chart == chartBPM2 ? tr("Great") : tr("Normal"));
		else if(slice->label() == "4") slice->setProperty("TargetArea", chart == chartBPM2 ? tr("Good") : tr("High Normal"));
		else if(slice->label() == "5") slice->setProperty("TargetArea", chart == chartBPM2 ? tr("Average") : tr("Hyper 1"));
		else if(slice->label() == "6") slice->setProperty("TargetArea", chart == chartBPM2 ? tr("Below Average") : tr("Hyper 2"));
		else if(slice->label() == "7") slice->setProperty("TargetArea", chart == chartBPM2 ? tr("Poor") : tr("Hyper 3"));

		chart->legend()->markers().at(color)->setLabel(settings.stats.legend ? QString("%1=%2%").arg(slice->value()).arg(slice->percentage() * 100, 0, 'f', 0) : slice->property("TargetArea").toString());
		slice->setLabel(settings.stats.legend ? slice->property("TargetArea").toString() : QString("%1=%2%").arg(slice->value()).arg(slice->percentage() * 100, 0, 'f', 0));

		if(chart == chartSYS2)
		{
			slice->setColor(syscolors[color]);
		}
		else if(chart == chartDIA2)
		{
			slice->setColor(diacolors[color]);
		}
		else if(chart == chartBPM2)
		{
			slice->setColor(bpmcolors[color]);
		}

		connect(chart->legend()->markers().at(color), &QLegendMarker::clicked , this, &MainWindow::pieLegendClicked);
		connect(chart->legend()->markers().at(color), &QLegendMarker::hovered , this, &MainWindow::pieLegendHovered);

		color++;
	}
}

bool MainWindow::recordAdd(bool user, HEALTHDATA data)
{
	for(int i = 0; i < database[user].count(); i++)
	{
		if(database[user].at(i).dts == data.dts)
		{
			return false;
		}
	}

	database[user].append(data);

	validateDB();
	updateLCD();
	calcView();

	return true;
}

bool MainWindow::recordMod(bool user, HEALTHDATA data)
{
	for(int i = 0; i < database[user].count(); i++)
	{
		if(database[user].at(i).dts == data.dts)
		{
			database[user].replace(i, data);

			calcView();

			return true;
		}
	}

	return false;
}

bool MainWindow::recordDel(bool user, qint64 dts)
{
	for(int i = 0; i < database[user].count(); i++)
	{
		if(database[user].at(i).dts == dts)
		{
			database[user].remove(i);

			updateLCD();

			QTimer::singleShot(1, this, &MainWindow::calcView);	// fixme: direct call crash, timer not?

			return true;
		}
	}

	return false;
}

bool MainWindow::recordHide(bool user, qint64 dts, bool state)
{
	for(int i = 0; i < database[user].count(); i++)
	{
		if(database[user].at(i).dts == dts)
		{
			database[user][i].inv = state;

			QTimer::singleShot(1, this, &MainWindow::calcView);	// fixme: direct call crash, timer not?

			return true;
		}
	}

	return false;
}

void MainWindow::switchUser(bool user)
{
	view.user = user;

	if(database[view.user].count())
	{
		if(!actionTimeMode->isChecked())
		{
			actionTimeMode->trigger();
		}

		toolbarDateTime->setDateTime(QDateTime::fromMSecsSinceEpoch(database[view.user].last().dts));
		toolButton_daily->setChecked(true);
	}

	updateLCD();

	modifyChartRanges();

	calcView();
}

void MainWindow::scanPlugins()
{
	QDir dir(DIR_PLUGINS);
	QStringList files = dir.entryList(QStringList()<<"*.dll"<<"*.dylib"<<"*.so", QDir::Files);

	foreach(QString file, files)
	{
		QPluginLoader *pluginLoader = new QPluginLoader(dir.absoluteFilePath(file));
		QObject *devicePlugin = pluginLoader->instance();

		if(devicePlugin)
		{
			DeviceInterface *deviceInterface = qobject_cast<DeviceInterface*>(devicePlugin);

			if(deviceInterface)
			{
				plugins.append(pluginLoader);

				if(settings.device.plugin == file)
				{
					plugin = plugins.count() - 1;

					actionImportDevice->setEnabled(true);
				}
			}

			pluginLoader->unload();
		}
		else
		{
			QMessageBox::critical(nullptr, APPNAME, tr("Scanning import plugin \"%1\" failed!\n\n%2").arg(file, pluginLoader->errorString()));
		}
	}
}

void MainWindow::scanLanguages()
{
	QDir files(DIR_LANGUAGES);
	QStringList languages = files.entryList(QStringList("ubpm_*.qm"), QDir::Files);
	QAction *action = new QAction("English", this);
	QActionGroup *actionGroup = new QActionGroup(this);

	action->setCheckable(true);
	action->setStatusTip(tr("Switch Language to %1").arg(action->text()));

	actionGroup->addAction(action);

	menuLanguage->addAction(action);
	menuLanguage->addSeparator();

	action->setChecked(true);

	if(translatorApplication.load(":/qm/plurals_en.qm"))
	{
		qApp->installTranslator(&translatorApplication);
	}

	foreach(QString language, languages)
	{
		QLocale locale(language.split('.').at(0).mid(5));
		QString text(locale.nativeLanguageName().front().toUpper() + locale.nativeLanguageName().mid(1));
		action = new QAction(text, this);

		action->setCheckable(true);
		action->setData(language.split('.').at(0).mid(5, 2));
		action->setStatusTip(tr("Switch Language to %1").arg(text));

		actionGroup->addAction(action);

		menuLanguage->addAction(action);

		if(settings.language == action->text())
		{
			action->setChecked(true);

			languageChanged(action);
		}
	}

	connect(actionGroup, &QActionGroup::triggered, this, &MainWindow::languageChanged);
}

void MainWindow::scanThemes()
{
	QDir files(DIR_THEMES);
	QStringList themes = files.entryList(QStringList("*.qss"), QDir::Files);
	QAction *action = new QAction("System", this);
	QActionGroup *actionGroup = new QActionGroup(this);

	action->setCheckable(true);
	action->setStatusTip(tr("Switch Theme to %1").arg(action->text()));

	actionGroup->addAction(action);

	menuTheme->addAction(action);
	menuTheme->addSeparator();

	action->setChecked(true);

	foreach(QString theme, themes)
	{
		action = new QAction(theme.split('.').at(0), this);

		action->setCheckable(true);
		action->setData(theme);
		action->setStatusTip(tr("Switch Theme to %1").arg(action->text()));

		actionGroup->addAction(action);

		menuTheme->addAction(action);

		if(settings.theme == action->text())
		{
			action->setChecked(true);

			themeChanged(action);
		}
	}

	connect(actionGroup, &QActionGroup::triggered, this, &MainWindow::themeChanged);
}

void MainWindow::scanStyles()
{
	QStringList styles = QStyleFactory::keys();
	QAction *action = new QAction("Fusion", this);
	QActionGroup *actionGroup = new QActionGroup(this);

	action->setCheckable(true);
	action->setData(action->text());
	action->setStatusTip(tr("Switch Style to %1").arg(action->text()));

	actionGroup->addAction(action);

	menuStyle->addAction(action);
	menuStyle->addSeparator();

	action->setChecked(true);

	styles.sort();

	foreach(QString style, styles)
	{
		if(style == "Fusion") continue;

		QString label(style);
		action = new QAction(label.replace("macintosh", "Macintosh").replace("windowsvista", "Windows Vista"), this);

		action->setCheckable(true);
		action->setData(style);
		action->setStatusTip(tr("Switch Style to %1").arg(action->text()));

		actionGroup->addAction(action);

		menuStyle->addAction(action);

		if(settings.style == action->text())
		{
			action->setChecked(true);

			styleChanged(action);
		}
	}

	connect(actionGroup, &QActionGroup::triggered, this, &MainWindow::styleChanged);
}

void MainWindow::hideAxisLabels(QChartView *view)
{
	QList <QGraphicsItem*> itemlist;
	QGraphicsItemGroup *itemgroup;
	QList <QGraphicsItem*> childs;
	QGraphicsTextItem *item;

	QGuiApplication::processEvents();

	itemlist = view->items();

	for(int i = 0; i < itemlist.count(); i++)
	{
		if((itemgroup = qgraphicsitem_cast<QGraphicsItemGroup*>(itemlist.at(i))))
		{
			childs = itemgroup->childItems();

			if(childs.count())
			{
				if((item = qgraphicsitem_cast<QGraphicsTextItem*>(childs.at(0))))
				{
					if(QString(item->metaObject()->className()).contains("ValueAxisLabel"))
					{
						childs.first()->setOpacity(0);
					}
					else if(QString(item->metaObject()->className()).contains("DateTimeAxisLabel"))
					{
						for(int c = 0; c < childs.count(); c++)
						{
							childs.at(c)->setOpacity(1);
						}

						childs.first()->setOpacity(0);
						childs.last()->setOpacity(0);
					}
				}
			}
		}
	}
}

void MainWindow::chartAxisXRangeChanged()
{
	hideAxisLabels(chartViewBP);
	hideAxisLabels(chartViewHR);
}

void MainWindow::chartSeriesClicked(QPointF point)
{
	QMenu *menu = new QMenu(this);

	menu->addAction(QIcon(":/svg/delete.svg"), tr("Delete record"));
	menu->addAction(QIcon(":/svg/invisible.svg"), tr("Hide record"));
	menu->addSeparator();
	menu->addAction(QIcon(":/svg/edit.svg"), tr("Edit record"));

	QAction *action = menu->exec(chartViewBP->cursor().pos());

	if(action)
	{
		QList <QPointF> sys = scatterSeriesSYS->points();

		int index = 0;

		foreach(QPointF p, sys)
		{
			if(p.x() == point.x())
			{
				if(action->text() == tr("Delete record"))
				{
					if(QMessageBox::question(this, APPNAME, tr("Really delete selected record?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
					{
						recordDel(view.user, QDateTime::fromMSecsSinceEpoch(sys.at(index).x()).toMSecsSinceEpoch());
					}
				}
				else if(action->text() == tr("Hide record"))
				{
					recordHide(view.user, QDateTime::fromMSecsSinceEpoch(sys.at(index).x()).toMSecsSinceEpoch(), true);
				}
				else if(action->text() == tr("Edit record"))
				{
					for(int i = 0; i < database[view.user].count(); i++)
					{
						if(database[view.user].at(i).dts == sys.at(index).x())
						{
							DialogRecord(this, view.user, settings, database[view.user].at(i)).exec();

							break;
						}
					}
				}

				break;
			}

			index++;
		}
	}
}

void MainWindow::chartSeriesHovered(QPointF point, bool state)
{
	QChart *chart;

	if(sender() == scatterSeriesSYS || sender() == scatterSeriesDIA)
	{
		chart = chartMainBP;
	}
	else
	{
		chart = chartMainHR;
	}

	if(state)
	{
		QList <QPointF> sys = scatterSeriesSYS->points();

		foreach(QPointF p, sys)
		{
			if(p.x() == point.x())
			{
				for(int i = 0; i < view.data.count(); i++)
				{
					if(view.data.at(i).dts == p.x())
					{
						chart->setToolTip(QString(
"<style>\
	table,th,td { border-collapse: collapse; border: 1px solid black; white-space: nowrap; }\
</style>\
<table>\
  <tr>\
	<th colspan='4'><br>&nbsp;%1&nbsp;<br></td>\
  </tr>\
  <tr>\
	<th><img src=':/svg/systolic.svg'/></td>\
	<th><img src=':/svg/diastolic.svg'/></td>\
	<th><img src=':/svg/pressure.svg'/></td>\
	<th><img src=':/svg/heart-%11.svg'/></td>\
  </tr>\
  <tr>\
	<td align='center' bgcolor='%7'>%2</td>\
	<td align='center' bgcolor='%8'>%3</td>\
	<td align='center' bgcolor='%9'>%4</td>\
	<td align='center' bgcolor='%10'>%5</td>\
  </tr>\
  <tr>\
	<td align='center' colspan='4'>&nbsp;%6&nbsp;</td>\
  </tr>\
</table>").arg(QDateTime::fromMSecsSinceEpoch(view.data.at(i).dts).toString("dddd, dd.MM.yyyy - hh:mm:ss")).arg(view.data.at(i).sys).arg(view.data.at(i).dia).arg(view.data.at(i).sys - view.data.at(i).dia).arg(view.data.at(i).bpm).arg(view.data.at(i).msg).arg(view.data.at(i).sys >= settings.table[view.user].warnsys ? "#FF0000" : "#00FF00", view.data.at(i).dia >= settings.table[view.user].warndia ? "#FF0000" : "#00FF00", view.data.at(i).sys - view.data.at(i).dia > settings.table[view.user].warnppr ? "#FF0000" : "#00FF00", view.data.at(i).bpm >= settings.table[view.user].warnbpm ? "#FF0000" : "#00FF00", (view.data.at(i).ihb && view.data.at(i).mov ? "irregular-movement" : view.data.at(i).ihb ? "irregular" : view.data.at(i).mov ? "movement" : "normal")));

						break;
					}
				}

				break;
			}
		}
	}
	else
	{
		chart->setToolTip(nullptr);
	}
}

void MainWindow::barSeriesClicked(int /*index*/, QBarSet *barset)
{
	if(barset == setSYS1Avg || barset == setDIA1Avg || barset == setBPM1Avg)
	{
		settings.stats.median = true;
	}
	else if(barset == setSYS1Med || barset == setDIA1Med || barset == setBPM1Med)
	{
		settings.stats.median = false;
	}

	drawBarChart(chartSYS1, seriesSYS1, setSYS1Min, setSYS1Max, setSYS1Avg, setSYS1Med, view.records ? view.SYSmin : 0, view.records ? view.SYSmax : 0, view.SYSavg, view.SYSmed);
	drawBarChart(chartDIA1, seriesDIA1, setDIA1Min, setDIA1Max, setDIA1Avg, setDIA1Med, view.records ? view.DIAmin : 0, view.records ? view.DIAmax : 0, view.DIAavg, view.DIAmed);
	drawBarChart(chartBPM1, seriesBPM1, setBPM1Min, setBPM1Max, setBPM1Avg, setBPM1Med, view.records ? view.BPMmin : 0, view.records ? view.BPMmax : 0, view.BPMavg, view.BPMmed);
}

void MainWindow::barSeriesHovered(bool status, int /*index*/, QBarSet *barset)
{
	if(status && (barset->label() == tr("Average") || barset->label() == tr("Median")))
	{
		dynamic_cast<QBarSeries*>(barset->parent())->chart()->setToolTip(tr("Click to swap Average and Median"));
	}
	else
	{
		dynamic_cast<QBarSeries*>(barset->parent())->chart()->setToolTip(nullptr);
	}
}

void MainWindow::pieSeriesClicked(QPieSlice *slice)
{
	bool exploded = slice->isExploded();

	foreach(QPieSlice *current, slice->series()->slices())
	{
		current->setExplodeDistanceFactor(0);
		current->setExploded(false);
		current->setLabel(settings.stats.legend ? current->property("TargetArea").toString() : QString("%1=%2%").arg(current->value()).arg(current->percentage() * 100, 0, 'f', 0));
	}

	if(!exploded)
	{
		slice->setExplodeDistanceFactor(0.1);
		slice->setExploded(true);
		slice->setLabel(settings.stats.legend ? QString("%1=%2%").arg(slice->value()).arg(slice->percentage() * 100, 0, 'f', 0) : slice->property("TargetArea").toString());
	}
}

void MainWindow::pieSeriesHovered(QPieSlice *slice, bool state)
{
	if(state)
	{
		foreach(QPieSlice *current, slice->series()->slices())
		{
			if(current == slice)
			{
				slice->series()->chart()->setToolTip(settings.stats.legend ? QString("%1=%2%").arg(slice->value()).arg(slice->percentage() * 100, 0, 'f', 0) : slice->property("TargetArea").toString());

				break;
			}
		}
	}
	else
	{
		slice->series()->chart()->setToolTip(nullptr);
	}
}

void MainWindow::pieLegendClicked()
{
	int index = 0;
	QString label;

	settings.stats.legend ^= 1;

	foreach(QPieSlice *slice, seriesSYS2->slices())
	{
		label = slice->label();

		slice->setLabel(chartSYS2->legend()->markers().at(index)->label());
		chartSYS2->legend()->markers().at(index)->setLabel(label);

		index++;
	}

	index = 0;

	foreach(QPieSlice *slice, seriesDIA2->slices())
	{
		label = slice->label();

		slice->setLabel(chartDIA2->legend()->markers().at(index)->label());
		chartDIA2->legend()->markers().at(index)->setLabel(label);

		index++;
	}

	index = 0;

	foreach(QPieSlice *slice, seriesBPM2->slices())
	{
		label = slice->label();

		slice->setLabel(chartBPM2->legend()->markers().at(index)->label());
		chartBPM2->legend()->markers().at(index)->setLabel(label);

		index++;
	}
}

void MainWindow::pieLegendHovered(bool status)
{
	QChart *chart = qobject_cast <QPieLegendMarker*>(sender())->series()->chart();

	if(status)
	{
		chart->setToolTip(tr("Click to swap Legend and Label"));
	}
	else
	{
		chart->setToolTip(nullptr);
	}
}

int MainWindow::calculatePrintTableRowHeight(QTextDocument *document)
{
	int rowheight;

	document->setHtml("\
<style>\
	table { border-collapse: collapse; }\
	th,td { border: 1px solid black; padding: 2px; }\
</style>\
<table>\
	<tr>\
		<th></th>\
	</tr>\
</table>");

	rowheight = document->size().height();

	document->setHtml("\
<style>\
	table { border-collapse: collapse; }\
	th,td { border: 1px solid black; padding: 2px; }\
</style>\
<table>\
	<tr>\
		<th></th>\
	</tr>\
	<tr>\
		<td></td>\
	</tr>\
</table>");

	rowheight = document->size().height() - rowheight;

	document->clear();

	return rowheight;
}

void MainWindow::printPreview(int index)
{
	if(view.records)
	{
		printPreviewDialog = new QPrintPreviewDialog(this, Qt::Window | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);

		printPreviewDialog->printer()->setPageSize(QPageSize(QPageSize::A4));
		printPreviewDialog->printer()->setPageOrientation(QPageLayout::Landscape);
		printPreviewDialog->printer()->setColorMode(QPrinter::Color);
		printPreviewDialog->printer()->setOutputFormat(QPrinter::PdfFormat);

		if(index == 0)
		{
			connect(printPreviewDialog, &QPrintPreviewDialog::paintRequested, this, [this]{ MainWindow::printChart(printPreviewDialog->printer()); });
		}
		else if(index == 1)
		{
			connect(printPreviewDialog, &QPrintPreviewDialog::paintRequested, this, [this]{ MainWindow::printTable(printPreviewDialog->printer()); });
		}
		else
		{
			connect(printPreviewDialog, &QPrintPreviewDialog::paintRequested, this, [this]{ MainWindow::printStats(printPreviewDialog->printer()); });
		}

		printPreviewDialog->findChildren<QToolBar*>().at(0)->actions().at(8)->setChecked(true);	// why setPageOrientation doesn't set this?
		printPreviewDialog->exec();

		disconnect(printPreviewDialog);
	}
	else
	{
		QMessageBox::warning(this, APPNAME, tr("No records to preview for selected time range!"));
	}
}

void MainWindow::printRecords(int index)
{
	if(view.records)
	{
		printDialog = new QPrintDialog(this);

		printDialog->printer()->setPageSize(QPageSize(QPageSize::A4));
		printDialog->printer()->setPageOrientation(QPageLayout::Landscape);
		printDialog->printer()->setColorMode(QPrinter::Color);

		if(printDialog->exec() == QDialog::Accepted)
		{
			if(index == 0)
			{
				printChart(printDialog->printer());
			}
			else if(index == 1)
			{
				printTable(printDialog->printer());
			}
			else
			{
				printStats(printDialog->printer());
			}
		}
	}
	else
	{
		QMessageBox::warning(this, APPNAME, tr("No records to print for selected time range!"));
	}
}

void MainWindow::printChart(QPrinter *printer)
{
	QRect page = printer->pageLayout().paintRectPixels(printer->resolution());
	QPainter painter(printer);
	QFont font = painter.font();
	QSize size;
	int w = page.width();
	int h = page.height();
	int h1 = h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN;
	int h2 = h1 / 2;
	int tab = tabWidget->currentIndex();

	tabWidget->setCurrentIndex(0);

	painter.setViewport(0, 0, w, h);
	painter.setWindow(0, 0, w, h);

	painter.setPen(QPen(Qt::black, 1));

	painter.setBrush(QColor(224, 224, 224));
	painter.drawRoundedRect(0, 0, w, h * PRINT_HEADER, PRINT_RADIUS, PRINT_RADIUS);
	painter.drawRoundedRect(0, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER, PRINT_RADIUS, PRINT_RADIUS);

	painter.drawImage(PRINT_MARGIN, 0, QIcon(QString(":/svg/user%1.svg").arg(view.user + 1)).pixmap(h*PRINT_HEADER, h*PRINT_HEADER).toImage());
	painter.drawImage(w - h*PRINT_HEADER - PRINT_MARGIN, 0, QIcon(QString(":/svg/user%1.svg").arg(view.user + 1)).pixmap(h*PRINT_HEADER, h*PRINT_HEADER).toImage());

	font.setPointSize(5);
	painter.setFont(font);
	painter.drawText(QRect(50, 0, w/8, h * PRINT_HEADER), Qt::AlignHCenter | Qt::AlignVCenter, tr("Created with UBPM for\nWindows / Linux / MacOS"));
	painter.drawText(QRect(w - w/8 - 50, 0, w/8, h * PRINT_HEADER), Qt::AlignHCenter | Qt::AlignVCenter, tr("Free and OpenSource\nhttps://codeberg.org/lazyt/ubpm"));

	font.setBold(true);
	font.setPointSize(15);
	painter.setFont(font);
	painter.drawText(QRect(0, 0, w, h * PRINT_HEADER), Qt::AlignCenter, settings.user[view.user].addition ? tr("%1 (Age: %2, Height: %3, Weight: %4)").arg(settings.user[view.user].name.isEmpty() ? tr("User %1").arg(1 + view.user) : settings.user[view.user].name).arg(QDate::currentDate().year() - settings.user[view.user].birth).arg(settings.user[view.user].height).arg(settings.user[view.user].weight) : settings.user[view.user].name.isEmpty() ? tr("User %1").arg(1 + view.user) : settings.user[view.user].name);

	font.setBold(false);
	font.setPointSize(10);
	painter.setFont(font);
	painter.drawText(QRect(PRINT_MARGIN, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER), Qt::AlignLeft | Qt::AlignVCenter, label_measurements->text());
	painter.drawText(QRect(0, h - h*PRINT_FOOTER, w - PRINT_MARGIN, h * PRINT_FOOTER), Qt::AlignRight | Qt::AlignVCenter, label_average->text());

	if(printer->pageLayout().orientation() == QPageLayout::Landscape)
	{
		painter.drawText(QRect(0, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER), Qt::AlignCenter, label_timespan->text());
	}

	size = chartViewBP->size();

	chartViewBP->resize(w, (settings.chart.heartrate && !settings.chart.hrsheet)? h2 : h1);
	chartViewBP->render(&painter, QRect(0, h * PRINT_HEADER, w, h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN));
	chartViewBP->resize(size);

	if(settings.chart.heartrate && !settings.chart.hrsheet)
	{
		chartViewHR->resize(w, h2);
		chartViewHR->render(&painter, QRect(0, h * PRINT_HEADER + h2, w, h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN));
		chartViewHR->resize(size);
	}

	painter.setBrush(QBrush());
	painter.drawRoundedRect(0, h*PRINT_HEADER + PRINT_MARGIN, w, h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN, PRINT_RADIUS, PRINT_RADIUS);

	if(settings.chart.heartrate && settings.chart.hrsheet)
	{
		printer->newPage();

		painter.setPen(QPen(Qt::black, 1));

		painter.setBrush(QColor(224, 224, 224));
		painter.drawRoundedRect(0, 0, w, h * PRINT_HEADER, PRINT_RADIUS, PRINT_RADIUS);
		painter.drawRoundedRect(0, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER, PRINT_RADIUS, PRINT_RADIUS);

		painter.drawImage(PRINT_MARGIN, 0, QIcon(QString(":/svg/user%1.svg").arg(view.user + 1)).pixmap(h*PRINT_HEADER, h*PRINT_HEADER).toImage());
		painter.drawImage(w - h*PRINT_HEADER - PRINT_MARGIN, 0, QIcon(QString(":/svg/user%1.svg").arg(view.user + 1)).pixmap(h*PRINT_HEADER, h*PRINT_HEADER).toImage());

		font.setPointSize(5);
		painter.setFont(font);
		painter.drawText(QRect(50, 0, w/8, h * PRINT_HEADER), Qt::AlignHCenter | Qt::AlignVCenter, tr("Created with UBPM for\nWindows / Linux / MacOS"));
		painter.drawText(QRect(w - w/8 - 50, 0, w/8, h * PRINT_HEADER), Qt::AlignHCenter | Qt::AlignVCenter, tr("Free and OpenSource\nhttps://codeberg.org/lazyt/ubpm"));

		font.setBold(true);
		font.setPointSize(15);
		painter.setFont(font);
		painter.drawText(QRect(0, 0, w, h * PRINT_HEADER), Qt::AlignCenter, settings.user[view.user].addition ? tr("%1 (Age: %2, Height: %3, Weight: %4)").arg(settings.user[view.user].name.isEmpty() ? tr("User %1").arg(1 + view.user) : settings.user[view.user].name).arg(QDate::currentDate().year() - settings.user[view.user].birth).arg(settings.user[view.user].height).arg(settings.user[view.user].weight) : settings.user[view.user].name.isEmpty() ? tr("User %1").arg(1 + view.user) : settings.user[view.user].name);

		font.setBold(false);
		font.setPointSize(10);
		painter.setFont(font);
		painter.drawText(QRect(PRINT_MARGIN, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER), Qt::AlignLeft | Qt::AlignVCenter, label_measurements->text());
		painter.drawText(QRect(0, h - h*PRINT_FOOTER, w - PRINT_MARGIN, h * PRINT_FOOTER), Qt::AlignRight | Qt::AlignVCenter, label_average->text());

		if(printer->pageLayout().orientation() == QPageLayout::Landscape)
		{
			painter.drawText(QRect(0, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER), Qt::AlignCenter, label_timespan->text());
		}

		size = chartViewHR->size();

		chartViewHR->resize(w, h1);
		chartViewHR->render(&painter, QRect(0, h * PRINT_HEADER, w, h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN));
		chartViewHR->resize(size);

		painter.setBrush(QBrush());
		painter.drawRoundedRect(0, h*PRINT_HEADER + PRINT_MARGIN, w, h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN, PRINT_RADIUS, PRINT_RADIUS);
	}

	tabWidget->setCurrentIndex(tab);
}

void MainWindow::printTable(QPrinter *printer)
{
	QRect page = printer->pageLayout().paintRectPixels(printer->resolution());
	QPainter painter(printer);
	QFont font = painter.font();
	QTextDocument *document = new QTextDocument(this);
	QTextCursor cursor(document);
	QString html;
	int w = page.width();
	int h = page.height();
	int rowheight = calculatePrintTableRowHeight(document);
	int topmargin = h*PRINT_HEADER + PRINT_MARGIN;
	bool fill;
	int tab = tabWidget->currentIndex();

	tabWidget->setCurrentIndex(1);

	document->setTextWidth(w);

	painter.setViewport(0, 0, w, h);
	painter.setWindow(0, 0, w, h);

	int record = 0;
	int count = view.records;

	while(count > 0)
	{
		document->clear();
		html.clear();
		int row = 1;

		painter.setPen(QPen(Qt::black, 1));

		painter.setBrush(QColor(224, 224, 224));
		painter.drawRoundedRect(0, 0, w, h * PRINT_HEADER, PRINT_RADIUS, PRINT_RADIUS);
		painter.drawRoundedRect(0, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER, PRINT_RADIUS, PRINT_RADIUS);

		painter.drawImage(PRINT_MARGIN, 0, QIcon(QString(":/svg/user%1.svg").arg(view.user + 1)).pixmap(h*PRINT_HEADER, h*PRINT_HEADER).toImage());
		painter.drawImage(w - h*PRINT_HEADER - PRINT_MARGIN, 0, QIcon(QString(":/svg/user%1.svg").arg(view.user + 1)).pixmap(h*PRINT_HEADER, h*PRINT_HEADER).toImage());

		font.setPointSize(5);
		painter.setFont(font);
		painter.drawText(QRect(50, 0, w/8, h * PRINT_HEADER), Qt::AlignHCenter | Qt::AlignVCenter, tr("Created with UBPM for\nWindows / Linux / MacOS"));
		painter.drawText(QRect(w - w/8 - 50, 0, w/8, h * PRINT_HEADER), Qt::AlignHCenter | Qt::AlignVCenter, tr("Free and OpenSource\nhttps://codeberg.org/lazyt/ubpm"));

		font.setBold(true);
		font.setPointSize(15);
		painter.setFont(font);
		painter.drawText(QRect(0, 0, w, h * PRINT_HEADER), Qt::AlignCenter, settings.user[view.user].addition ? tr("%1 (Age: %2, Height: %3, Weight: %4)").arg(settings.user[view.user].name.isEmpty() ? tr("User %1").arg(1 + view.user) : settings.user[view.user].name).arg(QDate::currentDate().year() - settings.user[view.user].birth).arg(settings.user[view.user].height).arg(settings.user[view.user].weight) : settings.user[view.user].name.isEmpty() ? tr("User %1").arg(1 + view.user) : settings.user[view.user].name);

		font.setBold(false);
		font.setPointSize(10);
		painter.setFont(font);
		painter.drawText(QRect(PRINT_MARGIN, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER), Qt::AlignLeft | Qt::AlignVCenter, label_measurements->text());
		painter.drawText(QRect(0, h - h*PRINT_FOOTER, w - PRINT_MARGIN, h * PRINT_FOOTER), Qt::AlignRight | Qt::AlignVCenter, label_average->text());

		if(printer->pageLayout().orientation() == QPageLayout::Landscape)
		{
			painter.drawText(QRect(0, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER), Qt::AlignCenter, label_timespan->text());
		}

		painter.setBrush(QBrush());
		painter.drawRoundedRect(0, h*PRINT_HEADER + PRINT_MARGIN, w, h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN, PRINT_RADIUS, PRINT_RADIUS);

		html.append(QString("\
<style>\
	table { width: 100%; border-collapse: collapse; margin-top: %1px }\
	tr.header { font-weight: bold; color: white; background-color: grey; }\
	tr.even { background-color: #e0e0e0; }\
	th, td { text-align: center; border: 1px solid black; padding: 2px; }\
	td.warn { color: red; font-weight: bold; }\
</style>\
<table>\
	<tr class=header>\
		<th width=20%>%2</th>\
		<th width=10%>%3</th>\
		<th width=5%>%4</th>\
		<th width=5%>%5</th>\
		<th width=5%>%6</th>\
		<th width=5%>%7</th>\
		<th width=5%>%8</th>\
		<th width=50%>%9</th>\
	</tr>").arg(topmargin).arg(tr("DATE"), tr("TIME"), tr("SYS"), tr("DIA"), tr("PPR"), tr("BPM"), tr("IHB"), tr("COMMENT")));

		while(topmargin + (row + 1)*rowheight < h - h*PRINT_FOOTER - PRINT_MARGIN)
		{
			fill = count > 0;

			QString date = fill ? QDateTime::fromMSecsSinceEpoch(view.data.at(record).dts).toString(printer->pageLayout().orientation() == QPageLayout::Landscape ? "dddd, dd.MM.yyyy" : "ddd, dd.MM.yyyy") : "";
			QString time = fill ? QDateTime::fromMSecsSinceEpoch(view.data.at(record).dts).toString("hh:mm:ss") : "";
			QString sys = fill ? QString("%1").arg(view.data.at(record).sys) : "";
			QString dia = fill ? QString("%1").arg(view.data.at(record).dia) : "";
			QString ppr = fill ? QString("%1").arg(view.data.at(record).sys - view.data.at(record).dia) : "";
			QString bpm = fill ? QString("%1").arg(view.data.at(record).bpm) : "";
			QString ihb = fill ? QString("%1").arg(view.data.at(record).ihb) : "";
			QString msg = fill ? QString("%1").arg(view.data.at(record).msg) : "";
			QString warnSys = fill ? QString("%1").arg(view.data.at(record).sys >= settings.table[view.user].warnsys ? "warn" : "") : "";
			QString warnDia = fill ? QString("%1").arg(view.data.at(record).dia >= settings.table[view.user].warndia ? "warn" : "") : "";
			QString warnPpr = fill ? QString("%1").arg(view.data.at(record).sys - view.data.at(record).dia > settings.table[view.user].warnppr ? "warn" : "") : "";
			QString warnBpm = fill ? QString("%1").arg(view.data.at(record).bpm >= settings.table[view.user].warnbpm ? "warn" : "") : "";
			QString warnIhb = fill ? QString("%1").arg(view.data.at(record).ihb ? "warn" : "") : "";

			html.append(QString("\
<tr class=%1>\
	<td>%2</td>\
	<td>%3</td>\
	<td class=%10>%4</td>\
	<td class=%11>%5</td>\
	<td class=%12>%6</td>\
	<td class=%13>%7</td>\
	<td class=%14>%8</td>\
	<td>%9</td>\
</tr>").arg((row & 1) ? "" : "even").arg(date, time, sys, dia, ppr, bpm, ihb, msg, warnSys, warnDia, warnPpr, warnBpm, warnIhb));

			record++;
			row++;
			count--;
		}

		html.append("</table>");

		cursor.insertHtml(html);

		document->drawContents(&painter);

		if(count > 0)
		{
			printer->newPage();
		}
	}

	tabWidget->setCurrentIndex(tab);
}

void MainWindow::printStats(QPrinter *printer)
{
	QRect page = printer->pageLayout().paintRectPixels(printer->resolution());
	QPainter painter(printer);
	QFont font = painter.font();
	QSize size;
	int w = page.width();
	int h = page.height();
	int tab = tabWidget->currentIndex();

	tabWidget->setCurrentIndex(2);

	painter.setViewport(0, 0, w, h);
	painter.setWindow(0, 0, w, h);

	painter.setPen(QPen(Qt::black, 1));

	painter.setBrush(QColor(224, 224, 224));
	painter.drawRoundedRect(0, 0, w, h * PRINT_HEADER, PRINT_RADIUS, PRINT_RADIUS);
	painter.drawRoundedRect(0, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER, PRINT_RADIUS, PRINT_RADIUS);

	painter.drawImage(PRINT_MARGIN, 0, QIcon(QString(":/svg/user%1.svg").arg(view.user + 1)).pixmap(h*PRINT_HEADER, h*PRINT_HEADER).toImage());
	painter.drawImage(w - h*PRINT_HEADER - PRINT_MARGIN, 0, QIcon(QString(":/svg/user%1.svg").arg(view.user + 1)).pixmap(h*PRINT_HEADER, h*PRINT_HEADER).toImage());

	font.setPointSize(5);
	painter.setFont(font);
	painter.drawText(QRect(50, 0, w/8, h * PRINT_HEADER), Qt::AlignHCenter | Qt::AlignVCenter, tr("Created with UBPM for\nWindows / Linux / MacOS"));
	painter.drawText(QRect(w - w/8 - 50, 0, w/8, h * PRINT_HEADER), Qt::AlignHCenter | Qt::AlignVCenter, tr("Free and OpenSource\nhttps://codeberg.org/lazyt/ubpm"));

	font.setBold(true);
	font.setPointSize(15);
	painter.setFont(font);
	painter.drawText(QRect(0, 0, w, h * PRINT_HEADER), Qt::AlignCenter, settings.user[view.user].addition ? tr("%1 (Age: %2, Height: %3, Weight: %4)").arg(settings.user[view.user].name.isEmpty() ? tr("User %1").arg(1 + view.user) : settings.user[view.user].name).arg(QDate::currentDate().year() - settings.user[view.user].birth).arg(settings.user[view.user].height).arg(settings.user[view.user].weight) : settings.user[view.user].name.isEmpty() ? tr("User %1").arg(1 + view.user) : settings.user[view.user].name);

	font.setBold(false);
	font.setPointSize(10);
	painter.setFont(font);
	painter.drawText(QRect(PRINT_MARGIN, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER), Qt::AlignLeft | Qt::AlignVCenter, label_measurements->text());
	painter.drawText(QRect(0, h - h*PRINT_FOOTER, w - PRINT_MARGIN, h * PRINT_FOOTER), Qt::AlignRight | Qt::AlignVCenter, label_average->text());

	if(printer->pageLayout().orientation() == QPageLayout::Landscape)
	{
		painter.drawText(QRect(0, h - h*PRINT_FOOTER, w, h * PRINT_FOOTER), Qt::AlignCenter, label_timespan->text());
	}

	size = chartView_1->size();
	chartView_1->resize(w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN) / 2);
	chartView_1->render(&painter, QRect(0, h*PRINT_HEADER + PRINT_MARGIN, w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2));
	chartView_1->resize(size);

	size = chartView_2->size();
	chartView_2->resize(w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN) / 2);
	chartView_2->render(&painter, QRect(w/3, h*PRINT_HEADER + PRINT_MARGIN, w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2));
	chartView_2->resize(size);

	size = chartView_3->size();
	chartView_3->resize(w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN) / 2);
	chartView_3->render(&painter, QRect(w/3 * 2 + 1, h*PRINT_HEADER + PRINT_MARGIN, w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2));
	chartView_3->resize(size);

	size = chartView_4->size();
	chartView_4->resize(w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN) / 2);
	chartView_4->render(&painter, QRect(0, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2 + h*PRINT_HEADER + PRINT_MARGIN, w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2));
	chartView_4->resize(size);

	size = chartView_5->size();
	chartView_5->resize(w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN) / 2);
	chartView_5->render(&painter, QRect(w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2 + h*PRINT_HEADER + PRINT_MARGIN, w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2));
	chartView_5->resize(size);

	size = chartView_6->size();
	chartView_6->resize(w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN) / 2);
	chartView_6->render(&painter, QRect(w/3 * 2 + 1, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2 + h*PRINT_HEADER + PRINT_MARGIN, w/3, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2));
	chartView_6->resize(size);

	painter.setBrush(QBrush());
	painter.drawRoundedRect(0, h*PRINT_HEADER + PRINT_MARGIN, w, h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN, PRINT_RADIUS, PRINT_RADIUS);

	painter.setPen(QPen(Qt::black, 1, Qt::DashLine));

	painter.drawLine(w/3, h*PRINT_HEADER + PRINT_MARGIN, w/3, h - h*PRINT_FOOTER - PRINT_MARGIN);
	painter.drawLine(w/3 * 2 + 1, h*PRINT_HEADER + PRINT_MARGIN, w/3 * 2 + 1, h - h*PRINT_FOOTER - PRINT_MARGIN);
	painter.drawLine(0, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2 + h*PRINT_HEADER + PRINT_MARGIN, w, (h - h*PRINT_HEADER - h*PRINT_FOOTER - 2*PRINT_MARGIN)/2 + h*PRINT_HEADER + PRINT_MARGIN);

	tabWidget->setCurrentIndex(tab);
}


QString MainWindow::calcBase64FromFile(QString filename)
{
	QFile file(filename);
	QByteArray content;
	QString base64;

	if(!file.open(QIODevice::ReadOnly))
	{
		QMessageBox::warning(this, APPNAME, tr("Could not create e-mail because generating base64 for attachment \"%1\" failed!\n\n%2").arg(filename, file.errorString()));
	}
	else
	{
		content = file.readAll();

		file.remove();

		base64 = content.toBase64();
	}

	return base64;
}

int MainWindow::validateDB()
{
	int duplicate = 0;

	if(database[0].count())
	{
		std::sort(database[0].begin(), database[0].end(), [](const HEALTHDATA &a, const HEALTHDATA &b) { return a.dts < b.dts; });

		for(int i = 0; i < database[0].count() - 1; i++)
		{
			if(database[0].at(i).dts == database[0].at(i + 1).dts)
			{
				duplicate++;

				database[0].remove(i-- + 1);
			}
			else if(database[0].at(i).dts == database[0].at(i + 1).dts - QDateTime::fromMSecsSinceEpoch(database[0].at(i + 1).dts).time().second()*1000)	// prevent double entries for existing records from old plugins without seconds
			{
				database[0][i].dts = database[0][i +1].dts;
				database[0].remove(i-- + 1);
			}
		}
	}

	if(database[1].count())
	{
		std::sort(database[1].begin(), database[1].end(), [](const HEALTHDATA &a, const HEALTHDATA &b) { return a.dts < b.dts; });

		for(int i = 0; i < database[1].count() - 1; i++)
		{
			if(database[1].at(i).dts == database[1].at(i + 1).dts)
			{
				duplicate++;

				database[1].remove(i-- + 1);
			}
			else if(database[1].at(i).dts == database[1].at(i + 1).dts - QDateTime::fromMSecsSinceEpoch(database[1].at(i + 1).dts).time().second()*1000)	// prevent double entries for existing records from old plugins without seconds
			{
				database[1][i].dts = database[1][i +1].dts;
				database[1].remove(i-- + 1);
			}
		}
	}

	return duplicate;
}

void MainWindow::importFromFile()
{
	QString filename(QFileDialog::getOpenFileName(this, tr("Import from CSV/XML/JSON/SQL"), settings.imp, tr("CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)")));

	if(!filename.isEmpty())
	{
		QFile file(filename);

		settings.imp = QFileInfo(filename).path();

		if(file.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			int invalid = 0;
			int duplicate = 0;
			int user1 = 0;
			int user2 = 0;

			if(filename.endsWith(".csv", Qt::CaseInsensitive))
			{
				invalid = importFromCSV(&file, &user1, &user2);
			}
			else if(filename.endsWith(".xml", Qt::CaseInsensitive))
			{
				invalid = importFromXML(&file, &user1, &user2);
			}
			else if(filename.endsWith(".json", Qt::CaseInsensitive))
			{
				invalid = importFromJSON(&file, &user1, &user2);
			}
			else if(filename.endsWith(".sql", Qt::CaseInsensitive))
			{
				invalid = importFromSQL(filename, &user1, &user2);
			}

			file.close();

			duplicate = validateDB();

			updateLCD();

			QString msg(tr("Successfully imported %n record(s) from %1.\n\n     User 1 : %2\n     User 2 : %3", "", user1 + user2).arg(filename.split('.').last().toUpper()).arg(user1).arg(user2));

			if(invalid)
			{
				msg.append("\n\n" + tr("Skipped %n invalid record(s)!", "", invalid));
			}

			if(duplicate)
			{
				msg.append("\n\n" + tr("Skipped %n duplicate record(s)!", "", duplicate));
			}

			if(database[0].count())
			{
				switchUser(0);
			}
			else if(database[1].count())
			{
				switchUser(1);
			}

			QMessageBox::information(this, APPNAME, msg);
		}
		else
		{
			QMessageBox::critical(this, APPNAME, tr("Could not open \"%1\"!\n\nReason: %2").arg(filename, file.errorString()));
		}
	}
}

int MainWindow::importFromCSV(QFile *file, int *user1, int *user2)
{
	int lines = 0;
	int invalid = 0;

	while(!file->atEnd())
	{
		QString line = file->readLine().simplified();

		if(!line.isEmpty())
		{
			QStringList values = line.split(';');

			HEALTHDATA record;
			QDateTime dt;
			QString msg;
			int user, sys, dia, bpm;

			if(values.count() == 10)		// UBPM format
			{
				user = values.at(0).toInt();
				dt = QDateTime::fromString(values.at(1) + values.at(2), "dd.MM.yyyyhh:mm:ss");
				sys = values.at(3).toInt();
				dia = values.at(4).toInt();
				bpm = values.at(5).toInt();
				int ihb = values.at(6).toInt();
				int mov = values.at(7).toInt();
				int inv = values.at(8).toInt();
				msg = values.at(9).toUtf8();

				if((user > 0 && user < 3) && dt.isValid() && (sys > 0 && sys < 256) && (dia > 0 && dia < 256) && (bpm > 0 && bpm < 256) && (ihb >= 0 && ihb <= 1) && (mov >= 0 && mov <= 1) && (inv >= 0 && inv <= 1))
				{
					record.dts = dt.toMSecsSinceEpoch();
					record.sys = sys;
					record.dia = dia;
					record.bpm = bpm;
					record.ihb = ihb;
					record.mov = mov;
					record.inv = inv;
					record.msg = msg;

					database[user - 1].append(record);

					user - 1 ? *user2 += 1 : *user1 += 1;
				}
				else if(lines > 0)
				{
					invalid++;
				}
			}
			if(values.count() == 9)			// UBPM format < 1.0.4
			{
				user = values.at(0).toInt();
				dt = QDateTime::fromString(values.at(1) + values.at(2), "dd.MM.yyyyhh:mm:ss");
				sys = values.at(3).toInt();
				dia = values.at(4).toInt();
				bpm = values.at(5).toInt();
				int ihb = values.at(6).toInt();
				int inv = values.at(7).toInt();
				msg = values.at(8).toUtf8();

				if((user > 0 && user < 3) && dt.isValid() && (sys > 0 && sys < 256) && (dia > 0 && dia < 256) && (bpm > 0 && bpm < 256) && (ihb >= 0 && ihb <= 1) && (inv >= 0 && inv <= 1))
				{
					record.dts = dt.toMSecsSinceEpoch();
					record.sys = sys;
					record.dia = dia;
					record.bpm = bpm;
					record.ihb = ihb;
					record.mov = 0;
					record.inv = inv;
					record.msg = msg;

					database[user - 1].append(record);

					user - 1 ? *user2 += 1 : *user1 += 1;
				}
				else if(lines > 0)
				{
					invalid++;
				}
			}
			else if(values.count() == 7)	// OBPM format
			{
				user = values.at(0).toInt();
				dt = QDateTime::fromString(values.at(1) + values.at(2), "dd.MM.yyhh:mm").addYears(100);
				sys = values.at(3).toInt();
				dia = values.at(4).toInt();
				bpm = values.at(5).toInt();
				msg = values.at(6).toUtf8();

				if(dt.date().year() > QDate::currentDate().year())
				{
					dt = dt.addYears(-100);
				}

				if((user > 0 && user < 3) && dt.isValid() && (sys > 0 && sys < 256) && (dia > 0 && dia < 256) && (bpm > 0 && bpm < 256))
				{
					record.dts = dt.toMSecsSinceEpoch();
					record.sys = sys;
					record.dia = dia;
					record.bpm = bpm;
					record.ihb = 0;
					record.mov = 0;
					record.inv = 0;
					record.msg = msg;

					database[user - 1].append(record);

					user - 1 ? *user2 += 1 : *user1 += 1;
				}
				else if(lines > 0)
				{
					invalid++;
				}
			}
			else
			{
				invalid++;
			}
		}

		lines++;
	}

	return invalid;
}

int MainWindow::importFromXML(QFile *file, int *user1, int *user2)
{
	QXmlStreamReader xml(file);
	int user = 0;
	int invalid = 0;

	while(!xml.atEnd())
	{
		if(xml.readNextStartElement())
		{
			if(xml.name() == "U1")
			{
				user = 1;
			}
			else if(xml.name() == "U2")
			{
				user = 2;
			}
			else if(xml.name() == "record")
			{
				int sys = -1, dia = -1, bpm = -1, ihb = -1, mov = -1, inv = -1;
				QString date, time, msg;
				QDateTime dt;
				HEALTHDATA record;

				while(xml.readNextStartElement())
				{
					if(xml.name() == "date")
					{
						date = xml.readElementText();
					}
					else if(xml.name() == "time")
					{
						time = xml.readElementText();
					}
					else if(xml.name() == "sys")
					{
						sys = xml.readElementText().toInt();
					}
					else if(xml.name() == "dia")
					{
						dia = xml.readElementText().toInt();
					}
					else if(xml.name() == "bpm")
					{
						bpm = xml.readElementText().toInt();
					}
					else if(xml.name() == "ihb")
					{
						ihb = xml.readElementText().toInt();
					}
					else if(xml.name() == "mov")
					{
						mov = xml.readElementText().toInt();
					}
					else if(xml.name() == "inv")
					{
						inv = xml.readElementText().toInt();
					}
					else if(xml.name() == "msg")
					{
						msg = xml.readElementText();
					}
				}

				dt = QDateTime::fromString(date + time, "dd.MM.yyyyhh:mm:ss");

				if((user > 0 && user < 3) && dt.isValid() && (sys > 0 && sys < 256) && (dia > 0 && dia < 256) && (bpm > 0 && bpm < 256) && (ihb >= 0 && ihb <= 1) && (mov >= 0 && mov <= 1) && (inv >= 0 && inv <= 1))
				{
					record.dts = dt.toMSecsSinceEpoch();
					record.sys = sys;
					record.dia = dia;
					record.bpm = bpm;
					record.ihb = ihb;
					record.mov = mov;
					record.inv = inv;
					record.msg = msg;

					database[user - 1].append(record);

					user - 1 ? *user2 += 1 : *user1 += 1;
				}
				else
				{
					invalid++;
				}
			}
		}
	}

	return invalid;
}

int MainWindow::importFromJSON(QFile *file, int *user1, int *user2)
{
	QJsonObject obj = QJsonDocument::fromJson(file->readAll()).object();
	QJsonArray u1 = obj.value("UBPM").toObject().value("U1").toArray();
	QJsonArray u2 = obj.value("UBPM").toObject().value("U2").toArray();

	int sys, dia, bpm, ihb, mov, inv;
	QString msg;
	QDateTime dt;
	HEALTHDATA record;
	int invalid = 0;

	for(int i = 0; i < u1.count(); i++)
	{
		dt = QDateTime::fromString(u1.at(i).toObject().value("date").toString() + u1.at(i).toObject().value("time").toString(), "dd.MM.yyyyhh:mm:ss");
		sys = u1.at(i).toObject().value("sys").toInt();
		dia = u1.at(i).toObject().value("dia").toInt();
		bpm = u1.at(i).toObject().value("bpm").toInt();
		ihb = u1.at(i).toObject().value("ihb").toInt();
		mov = u1.at(i).toObject().value("mov").toInt();
		inv = u1.at(i).toObject().value("inv").toInt();
		msg = u1.at(i).toObject().value("msg").toString();

		if(dt.isValid() && (sys > 0 && sys < 256) && (dia > 0 && dia < 256) && (bpm > 0 && bpm < 256) && (ihb >= 0 && ihb <= 1) && (mov >= 0 && mov <= 1) && (inv >= 0 && inv <= 1))
		{
			record.dts = dt.toMSecsSinceEpoch();
			record.sys = sys;
			record.dia = dia;
			record.bpm = bpm;
			record.ihb = ihb;
			record.mov = mov;
			record.inv = inv;
			record.msg = msg;

			database[0].append(record);

			*user1 += 1;
		}
		else
		{
			invalid++;
		}
	}

	for(int i = 0; i < u2.count(); i++)
	{
		dt = QDateTime::fromString(u2.at(i).toObject().value("date").toString() + u2.at(i).toObject().value("time").toString(), "dd.MM.yyyyhh:mm:ss");
		sys = u2.at(i).toObject().value("sys").toInt();
		dia = u2.at(i).toObject().value("dia").toInt();
		bpm = u2.at(i).toObject().value("bpm").toInt();
		ihb = u2.at(i).toObject().value("ihb").toInt();
		mov = u2.at(i).toObject().value("mov").toInt();
		inv = u2.at(i).toObject().value("inv").toInt();
		msg = u2.at(i).toObject().value("msg").toString();

		if(dt.isValid() && (sys > 0 && sys < 256) && (dia > 0 && dia < 256) && (bpm > 0 && bpm < 256) && (ihb >= 0 && ihb <= 1) && (mov >= 0 && mov <= 1) && (inv >= 0 && inv <= 1))
		{
			record.dts = dt.toMSecsSinceEpoch();
			record.sys = sys;
			record.dia = dia;
			record.bpm = bpm;
			record.ihb = ihb;
			record.mov = mov;
			record.inv = inv;
			record.msg = msg;

			database[1].append(record);

			*user2 += 1;
		}
		else
		{
			invalid++;
		}
	}

	return invalid;
}

int MainWindow::importFromSQL(QString filename, int *user1, int *user2)
{
	int invalid = 0;

	if(!sqldb.isValid())
	{
		sqldb = QSqlDatabase::addDatabase(QSqlDatabase::isDriverAvailable("QSQLCIPHER") ? "QSQLCIPHER" : "QSQLITE", "UBPM");
	}

	sqldb.setDatabaseName(filename);

	if(sqldb.open())
	{
		QSqlQuery query(sqldb);
		int sys, dia, bpm, ihb, mov, inv;
		QString msg;
		QDateTime dt;
		HEALTHDATA record;

		if(settings.database.encryption)
		{
			query.exec(QString("PRAGMA key = '%1'").arg(settings.database.password));
		}

		if(query.exec("SELECT * FROM U1"))
		{
			sqlcipher = true;

			while(query.next())
			{
				dt = QDateTime::fromString(query.value("date").toString() + query.value("time").toString(), "dd.MM.yyyyhh:mm:ss");
				sys = query.value("sys").toInt();
				dia = query.value("dia").toInt();
				bpm = query.value("bpm").toInt();
				ihb = query.value("ihb").toInt();
				mov = query.value("mov").toInt();
				inv = query.value("inv").toInt();
				msg = query.value("msg").toString();

				if((dt.isValid()) && (sys > 0 && sys < 256) && (dia > 0 && dia < 256) && (bpm > 0 && bpm < 256) && (ihb >= 0 && ihb <= 1) && (mov >= 0 && mov <= 1) && (inv >= 0 && inv <= 1))
				{
					record.dts = dt.toMSecsSinceEpoch();
					record.sys = sys;
					record.dia = dia;
					record.bpm = bpm;
					record.ihb = ihb;
					record.mov = mov;
					record.inv = inv;
					record.msg = msg;

					database[0].append(record);

					if(user1) *user1 += 1;
				}
				else
				{
					invalid++;
				}
			}
		}
		else
		{
			if(query.lastError().databaseText() == "file is not a database")
			{
				sqlcipher = false;

				QMessageBox::warning(isHidden() ? nullptr : this, APPNAME, tr("Doesn't look like a UBPM database!\n\nMaybe wrong encryption settings/password?"));
			}
		}

		if(query.exec("SELECT * FROM U2"))
		{
			while(query.next())
			{
				dt = QDateTime::fromString(query.value("date").toString() + query.value("time").toString(), "dd.MM.yyyyhh:mm:ss");
				sys = query.value("sys").toInt();
				dia = query.value("dia").toInt();
				bpm = query.value("bpm").toInt();
				ihb = query.value("ihb").toInt();
				mov = query.value("mov").toInt();
				inv = query.value("inv").toInt();
				msg = query.value("msg").toString();

				if((dt.isValid()) && (sys > 0 && sys < 256) && (dia > 0 && dia < 256) && (bpm > 0 && bpm < 256) && (ihb >= 0 && ihb <= 1) && (mov >= 0 && mov <= 1) && (inv >= 0 && inv <= 1))
				{
					record.dts = dt.toMSecsSinceEpoch();
					record.sys = sys;
					record.dia = dia;
					record.bpm = bpm;
					record.ihb = ihb;
					record.mov = mov;
					record.inv = inv;
					record.msg = msg;

					database[1].append(record);

					if(user2) *user2 += 1;
				}
				else
				{
					invalid++;
				}
			}
		}

		sqldb.close();
	}

	return invalid;
}

void MainWindow::exportToFile(QString filetype)
{
	if(database[0].count() || database[1].count())
	{
		QString filename = QFileDialog::getSaveFileName(this, tr("Export to %1").arg(filetype), settings.exp + QString("/ubpm-%1.%2").arg(QDateTime::currentDateTime().date().toString("yyyy-MM-dd"), filetype.toLower()), tr("%1 File (*.%2)").arg(filetype, filetype.toLower()));

		if(!filename.isEmpty())
		{
			QFile file(filename);

			settings.exp = QFileInfo(filename).path();

			if(file.open(QIODevice::WriteOnly | QIODevice::Text))
			{
				if(filetype == "CSV")
				{
					exportToCSV(&file);
				}
				else if(filetype == "XML")
				{
					exportToXML(&file);
				}
				else if(filetype == "JSON")
				{
					exportToJSON(&file);
				}
				else if(filetype == "SQL")
				{
					exportToSQL(filename);
				}

				file.close();
			}
			else
			{
				QMessageBox::critical(this, APPNAME, tr("Could not create \"%1\"!\n\nReason: %2").arg(filename, file.errorString()));
			}
		}
	}
	else
	{
		QMessageBox::warning(this, APPNAME, tr("The database is empty, no records to export!"));
	}
}

void MainWindow::exportToCSV(QFile *file)
{
	file->write("USER;DATE;TIME;SYS;DIA;BPM;IHB;MOV;INV;MSG\n");

	for(int i = 0; i < database[0].count(); i++)
	{
		file->write(QString("1;%1;%2;%3;%4;%5;%6;%7;%8;%9\n").arg(QDateTime::fromMSecsSinceEpoch(database[0].at(i).dts).toString("dd.MM.yyyy"), QDateTime::fromMSecsSinceEpoch(database[0].at(i).dts).toString("hh:mm:ss")).arg(database[0].at(i).sys, 3, 10, QChar('0')).arg(database[0].at(i).dia, 3, 10, QChar('0')).arg(database[0].at(i).bpm, 3, 10, QChar('0')).arg(database[0].at(i).ihb).arg(database[0].at(i).mov).arg(database[0].at(i).inv).arg(database[0].at(i).msg).toUtf8());
	}

	for(int i = 0; i < database[1].count(); i++)
	{
		file->write(QString("2;%1;%2;%3;%4;%5;%6;%7;%8;%9\n").arg(QDateTime::fromMSecsSinceEpoch(database[1].at(i).dts).toString("dd.MM.yyyy"), QDateTime::fromMSecsSinceEpoch(database[1].at(i).dts).toString("hh:mm:ss")).arg(database[1].at(i).sys, 3, 10, QChar('0')).arg(database[1].at(i).dia, 3, 10, QChar('0')).arg(database[1].at(i).bpm, 3, 10, QChar('0')).arg(database[1].at(i).ihb).arg(database[1].at(i).mov).arg(database[1].at(i).inv).arg(database[1].at(i).msg).toUtf8());
	}
}

void MainWindow::exportToXML(QFile *file)
{
	file->write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<UBPM>\n\t<U1>\n");

	for(int i = 0; i < database[0].count(); i++)
	{
		file->write(QString("\t\t<record>\n\t\t\t<date>%1</date>\n\t\t\t<time>%2</time>\n\t\t\t<sys>%3</sys>\n\t\t\t<dia>%4</dia>\n\t\t\t<bpm>%5</bpm>\n\t\t\t<ihb>%6</ihb>\n\t\t\t<mov>%7</mov>\n\t\t\t<inv>%8</inv>\n\t\t\t<msg>%9</msg>\n\t\t</record>\n").arg(QDateTime::fromMSecsSinceEpoch(database[0].at(i).dts).toString("dd.MM.yyyy"), QDateTime::fromMSecsSinceEpoch(database[0].at(i).dts).toString("hh:mm:ss")).arg(database[0].at(i).sys).arg(database[0].at(i).dia).arg(database[0].at(i).bpm).arg(database[0].at(i).ihb).arg(database[0].at(i).mov).arg(database[0].at(i).inv).arg(database[0].at(i).msg).toUtf8());
	}

	file->write("\t</U1>\n\t<U2>\n");

	for(int i = 0; i < database[1].count(); i++)
	{
		file->write(QString("\t\t<record>\n\t\t\t<date>%1</date>\n\t\t\t<time>%2</time>\n\t\t\t<sys>%3</sys>\n\t\t\t<dia>%4</dia>\n\t\t\t<bpm>%5</bpm>\n\t\t\t<ihb>%6</ihb>\n\t\t\t<mov>%7</mov>\n\t\t\t<inv>%8</inv>\n\t\t\t<msg>%9</msg>\n\t\t</record>\n").arg(QDateTime::fromMSecsSinceEpoch(database[1].at(i).dts).toString("dd.MM.yyyy"), QDateTime::fromMSecsSinceEpoch(database[1].at(i).dts).toString("hh:mm:ss")).arg(database[1].at(i).sys).arg(database[1].at(i).dia).arg(database[1].at(i).bpm).arg(database[1].at(i).ihb).arg(database[1].at(i).mov).arg(database[1].at(i).inv).arg(database[1].at(i).msg).toUtf8());
	}

	file->write("\t</U2>\n</UBPM>\n");
}

void MainWindow::exportToJSON(QFile *file)
{
	file->write("{\n\t\"UBPM\": {\n\t\t\"U1\": [\n");

	for(int i = 0; i < database[0].count(); i++)
	{
		if(i > 0)
		{
			file->write(",\n");
		}

		file->write(QString("\t\t\t{\n\t\t\t\t\"date\": \"%1\",\n\t\t\t\t\"time\": \"%2\",\n\t\t\t\t\"sys\": %3,\n\t\t\t\t\"dia\": %4,\n\t\t\t\t\"bpm\": %5,\n\t\t\t\t\"ihb\": %6,\n\t\t\t\t\"mov\": %7,\n\t\t\t\t\"inv\": %8,\n\t\t\t\t\"msg\": \"%9\"\n\t\t\t}").arg(QDateTime::fromMSecsSinceEpoch(database[0].at(i).dts).toString("dd.MM.yyyy"), QDateTime::fromMSecsSinceEpoch(database[0].at(i).dts).toString("hh:mm:ss")).arg(database[0].at(i).sys).arg(database[0].at(i).dia).arg(database[0].at(i).bpm).arg(database[0].at(i).ihb).arg(database[0].at(i).mov).arg(database[0].at(i).inv).arg(database[0].at(i).msg).toUtf8());
	}

	file->write("\n\t\t],\n\t\t\"U2\": [\n");

	for(int i = 0; i < database[1].count(); i++)
	{
		if(i > 0)
		{
			file->write(",\n");
		}

		file->write(QString("\t\t\t{\n\t\t\t\t\"date\": \"%1\",\n\t\t\t\t\"time\": \"%2\",\n\t\t\t\t\"sys\": %3,\n\t\t\t\t\"dia\": %4,\n\t\t\t\t\"bpm\": %5,\n\t\t\t\t\"ihb\": %6,\n\t\t\t\t\"mov\": %7,\n\t\t\t\t\"inv\": %8,\n\t\t\t\t\"msg\": \"%9\"\n\t\t\t}").arg(QDateTime::fromMSecsSinceEpoch(database[1].at(i).dts).toString("dd.MM.yyyy"), QDateTime::fromMSecsSinceEpoch(database[1].at(i).dts).toString("hh:mm:ss")).arg(database[1].at(i).sys).arg(database[1].at(i).dia).arg(database[1].at(i).bpm).arg(database[1].at(i).ihb).arg(database[1].at(i).mov).arg(database[1].at(i).inv).arg(database[1].at(i).msg).toUtf8());
	}

	file->write("\n\t\t]\n\t}\n}\n");
}

void MainWindow::exportToSQL(QString filename)
{
	if(!sqldb.isValid())
	{
		sqldb = QSqlDatabase::addDatabase(QSqlDatabase::isDriverAvailable("QSQLCIPHER") ? "QSQLCIPHER" : "QSQLITE", "UBPM");
	}

	sqldb.setDatabaseName(filename);

	if(sqldb.open())
	{
		QSqlQuery query(sqldb);

		if(settings.database.encryption)
		{
			query.exec(QString("PRAGMA key = '%1'").arg(settings.database.password));
		}

		sqldb.transaction();

		if(sqldb.tables().count())
		{
			query.exec("DROP TABLE 'U1'");
			query.exec("DROP TABLE 'U2'");
		}

		query.exec("CREATE TABLE 'U1' ('date' TEXT, 'time' TEXT, 'sys' INTEGER, 'dia' INTEGER, 'bpm' INTEGER, 'ihb' INTEGER, 'mov' INTEGER, 'inv' INTEGER, 'msg' TEXT)");
		query.exec("CREATE TABLE 'U2' ('date' TEXT, 'time' TEXT, 'sys' INTEGER, 'dia' INTEGER, 'bpm' INTEGER, 'ihb' INTEGER, 'mov' INTEGER, 'inv' INTEGER, 'msg' TEXT)");

		for(int i = 0; i < database[0].count(); i++)
		{
			query.exec(QString("INSERT INTO 'U1' VALUES ('%1', '%2', %3, %4, %5, %6, %7, %8, '%9')").arg(QDateTime::fromMSecsSinceEpoch(database[0].at(i).dts).toString("dd.MM.yyyy"), QDateTime::fromMSecsSinceEpoch(database[0].at(i).dts).toString("hh:mm:ss")).arg(database[0].at(i).sys).arg(database[0].at(i).dia).arg(database[0].at(i).bpm).arg(database[0].at(i).ihb).arg(database[0].at(i).mov).arg(database[0].at(i).inv).arg(database[0].at(i).msg));
		}

		for(int i = 0; i < database[1].count(); i++)
		{
			query.exec(QString("INSERT INTO 'U2' VALUES ('%1', '%2', %3, %4, %5, %6, %7, %8, '%9')").arg(QDateTime::fromMSecsSinceEpoch(database[1].at(i).dts).toString("dd.MM.yyyy"), QDateTime::fromMSecsSinceEpoch(database[1].at(i).dts).toString("hh:mm:ss")).arg(database[1].at(i).sys).arg(database[1].at(i).dia).arg(database[1].at(i).bpm).arg(database[1].at(i).ihb).arg(database[1].at(i).mov).arg(database[1].at(i).inv).arg(database[1].at(i).msg));
		}

		sqldb.commit();

		sqldb.close();
	}
}

void MainWindow::changeTimeAxis(QString format, int ticks, int time, int span)
{
	QSignalBlocker blocker(toolbarDateTime);

	toolbarDateTime->setDateTime(timespan);

	axisXBP->setFormat(format);
	axisXBP->setTickCount(ticks + 2);

	axisXHR->setFormat(format);
	axisXHR->setTickCount(ticks + 2);

	if(!span)
	{
		if(database[view.user].count())
		{
			qint64 delta = (database[view.user].last().dts - database[view.user].first().dts) / 15000;

			axisXBP->setRange(timespan.addSecs(-delta), QDateTime::fromMSecsSinceEpoch(database[view.user].last().dts).addSecs(delta));
			axisXHR->setRange(timespan.addSecs(-delta), QDateTime::fromMSecsSinceEpoch(database[view.user].last().dts).addSecs(delta));
		}
		else
		{
			axisXBP->setRange(timespan, QDateTime::currentDateTime());
			axisXHR->setRange(timespan, QDateTime::currentDateTime());
		}
	}
	else if(span == 7*24*60 || span >= 90*24*60)
	{
		axisXBP->setRange(timespan.addSecs(-time/2 * 60), timespan.addSecs((time/2 + span)*60));
		axisXHR->setRange(timespan.addSecs(-time/2 * 60), timespan.addSecs((time/2 + span)*60));
	}
	else
	{
		axisXBP->setRange(timespan.addSecs(-time*60), timespan.addSecs((time + span)*60));
		axisXHR->setRange(timespan.addSecs(-time*60), timespan.addSecs((time + span)*60));
	}

	if(actionTimeMode->isChecked())
	{
		if(span < 12*60)
		{
			label_timespan->setText(timespan.toString("dddd, dd. MMMM yyyy  |  %1 - %2").arg(timespan.toString("hh:mm"), timespan.addSecs(span * 60).toString("hh:mm")));
		}
		else if(span == 12*60)
		{
			label_timespan->setText(QString("%1  |  %2").arg(timespan.toString("dddd, dd. MMMM yyyy"), timespan.time().hour() < 12 ? tr("Morning") : tr("Afternoon")));
		}
		else if(span == 24*60)
		{
			label_timespan->setText(timespan.toString("dddd, dd. MMMM yyyy"));
		}
		else if(span == 7*24*60)
		{
			label_timespan->setText(QString("%1 - %2  |  %3 %4").arg(timespan.toString("dd. MMMM yyyy"), timespan.addDays(6).toString("dd. MMMM yyyy"), tr("Week")).arg(timespan.date().weekNumber()));
		}
		else if(span < 90*24*60)
		{
			label_timespan->setText(timespan.toString("MMMM yyyy"));
		}
		else if(span == 90*24*60)
		{
			label_timespan->setText(QString("%1. %2 %3").arg((timespan.date().month() + 2) / 3).arg(tr("Quarter")).arg(timespan.date().year()));
		}
		else if(span == 180*24*60)
		{
			label_timespan->setText(QString("%1. %2 %3").arg((timespan.date().month() + 5) / 6).arg(tr("Half Year")).arg(timespan.date().year()));
		}
		else
		{
			label_timespan->setText(QString("%1 %2").arg(tr("Year"), timespan.toString("yyyy")));
		}
	}
	else
	{
		if(span == 7*24*60)
		{
			label_timespan->setText(QString("%1 - %2").arg(timespan.toString("dd. MMMM yyyy"), timespan.addDays(6).toString("dd. MMMM yyyy")));
		}
		else if(span == 14*24*60)
		{
			label_timespan->setText(QString("%1 - %2").arg(timespan.toString("dd. MMMM yyyy"), timespan.addDays(13).toString("dd. MMMM yyyy")));
		}
		else if(span == 21*24*60)
		{
			label_timespan->setText(QString("%1 - %2").arg(timespan.toString("dd. MMMM yyyy"), timespan.addDays(20).toString("dd. MMMM yyyy")));
		}
		else if(span == 28*24*60)
		{
			label_timespan->setText(QString("%1 - %2").arg(timespan.toString("dd. MMMM yyyy"), timespan.addDays(27).toString("dd. MMMM yyyy")));
		}
		else if(span == 90*24*60)
		{
			label_timespan->setText(QString("%1 - %2").arg(timespan.toString("dd. MMMM yyyy"), timespan.addDays(89).toString("dd. MMMM yyyy")));
		}
		else if(span == 180*24*60)
		{
			label_timespan->setText(QString("%1 - %2").arg(timespan.toString("dd. MMMM yyyy"), timespan.addDays(179).toString("dd. MMMM yyyy")));
		}
		else if(span == 270*24*60)
		{
			label_timespan->setText(QString("%1 - %2").arg(timespan.toString("dd. MMMM yyyy"), timespan.addDays(269).toString("dd. MMMM yyyy")));
		}
		else if(span == 365*24*60)
		{
			label_timespan->setText(QString("%1 - %2").arg(timespan.toString("dd. MMMM yyyy"), timespan.addDays(364).toString("dd. MMMM yyyy")));
		}
		else
		{
			label_timespan->setText(QString("%1 - %2").arg(timespan.toString("dd. MMMM yyyy"), database[view.user].count() ? QDateTime::fromMSecsSinceEpoch(database[view.user].last().dts).toString("dd. MMMM yyyy") : QDateTime::currentDateTime().toString("dd. MMMM yyyy")));
		}
	}

	calcView();
}

void MainWindow::clearUser(int user)
{
	if(QMessageBox::question(this, APPNAME, tr("Really delete all records for user %1?").arg(user), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		QFile file(settings.database.location);

		database[user - 1].clear();

		if(actionSwitchUser1->isEnabled())
		{
			updateLCD();

			calcView();
		}

		file.copy(file.fileName() + ".bak");

		QMessageBox::information(this, APPNAME, tr("All records for user %1 deleted and existing database saved to \"ubpm.sql.bak\".").arg(user));
	}
}

void MainWindow::on_actionPreviewChart_triggered()
{
	printPreview(0);
}

void MainWindow::on_actionPreviewTable_triggered()
{
	printPreview(1);
}

void MainWindow::on_actionPreviewStatistic_triggered()
{
	printPreview(2);
}

void MainWindow::on_actionPrintChart_triggered()
{
	printRecords(0);
}

void MainWindow::on_actionPrintTable_triggered()
{
	printRecords(1);
}

void MainWindow::on_actionPrintStatistic_triggered()
{
	printRecords(2);
}

void MainWindow::on_actionImportDevice_triggered()
{
	QVector <HEALTHDATA> u1, u2;
	DeviceInterface *deviceInterface = qobject_cast<DeviceInterface*>(plugins.at(plugin)->instance());
	DEVICEINFO deviceInfo = deviceInterface->getDeviceInfo();

	if(deviceInterface->getDeviceData(this, translatorApplication.language(), theme, &u1, &u2))
	{
		int duplicate = 0;

		database[0].append(u1);
		database[1].append(u2);

		duplicate = validateDB();

		updateLCD();

		QString msg(tr("Successfully imported %n record(s) from %1.\n\n     User 1 : %2\n     User 2 : %3", "", u1.count() + u2.count()).arg(deviceInfo.model).arg(u1.count()).arg(u2.count()));

		if(duplicate)
		{
			msg.append("\n\n" + tr("Skipped %n duplicate record(s)!", "", duplicate));
		}

		if(database[0].count())
		{
			switchUser(0);
		}
		else if(database[1].count())
		{
			switchUser(1);
		}

		QMessageBox::information(this, APPNAME, msg);
	}
}

void MainWindow::on_actionImportFile_triggered()
{
	importFromFile();
}

void MainWindow::on_actionImportInput_triggered()
{
	HEALTHDATA record = {};

	DialogRecord(this, view.user, settings, record).exec();
}

void MainWindow::on_actionExportCSV_triggered()
{
	exportToFile("CSV");
}

void MainWindow::on_actionExportXML_triggered()
{
	exportToFile("XML");
}

void MainWindow::on_actionExportJSON_triggered()
{
	exportToFile("JSON");
}

void MainWindow::on_actionExportSQL_triggered()
{
	exportToFile("SQL");
}

void MainWindow::on_actionClearAll_triggered()
{
	if(QMessageBox::question(this, APPNAME, tr("Really delete all records?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		database[0].clear();
		database[1].clear();

		updateLCD();

		calcView();

		QFile::moveToTrash(settings.database.location);

		QMessageBox::information(this, APPNAME, tr("All records deleted and existing database \"ubpm.sql\" moved to trash."));
	}
}

void MainWindow::on_actionClearUser1_triggered()
{
	clearUser(1);
}

void MainWindow::on_actionClearUser2_triggered()
{
	clearUser(2);
}

void MainWindow::on_actionAnalysis_triggered()
{
	DialogAnalysis(this, view.user + 1, &settings, &database[0], &database[1]).exec();
}

void MainWindow::on_actionMail_triggered()
{
	QString eml = QString("To: %1\nSubject: %2\nMIME-Version: 1.0\nX-Unsent: 1\nContent-type: multipart/mixed; boundary=\"#####*#####\"\n\n--#####*#####\nContent-Type: text/html; charset=UTF-8\n\n%3").arg(settings.email.address, settings.email.subject, QString(settings.email.message).replace("\n", "<br>").replace("$USER", settings.user[view.user].name));
	QPrinter *printer = new QPrinter();
	QFile file(FILE_EMAIL);
	QString attachment;

	printer->setPageSize(QPageSize(QPageSize::A4));
	printer->setPageOrientation(QPageLayout::Landscape);
	printer->setColorMode(QPrinter::Color);
	printer->setOutputFormat(QPrinter::PdfFormat);

	printer->setOutputFileName(FILE_CHART);
	printChart(printer);

	printer->setOutputFileName(FILE_TABLE);
	printTable(printer);

	printer->setOutputFileName(FILE_STATS);
	printStats(printer);

	if(eml.contains("$CHART"))
	{
		eml.replace("$CHART", "");
		eml.append(QString("\n\n--#####*#####\nContent-Type: application/pdf; name=%1.pdf\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment\n\n").arg(tr("Chart")));

		attachment = calcBase64FromFile(FILE_CHART);

		if(attachment.isEmpty())
		{
			return;
		}
		else
		{
			eml.append(attachment);
		}
	}

	if(eml.contains("$TABLE"))
	{
		eml.replace("$TABLE", "");
		eml.append(QString("\n\n--#####*#####\nContent-Type: application/pdf; name=%1.pdf\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment\n\n").arg(tr("Table")));

		attachment = calcBase64FromFile(FILE_TABLE);

		if(attachment.isEmpty())
		{
			return;
		}
		else
		{
			eml.append(attachment);
		}
	}

	if(eml.contains("$STATS"))
	{
		eml.replace("$STATS", "");
		eml.append(QString("\n\n--#####*#####\nContent-Type: application/pdf; name=%1.pdf\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment\n\n").arg(tr("Statistic")));

		attachment = calcBase64FromFile(FILE_STATS);

		if(attachment.isEmpty())
		{
			return;
		}
		else
		{
			eml.append(attachment);
		}
	}

	eml.append("\n\n--#####*#####--\n");

	if(!file.open(QIODevice::WriteOnly))
	{
		QMessageBox::warning(this, APPNAME, tr("Could not open e-mail \"%1\"!\n\n%2").arg(file.fileName(), file.errorString()));

		return;
	}

	file.write(eml.toUtf8());
	file.close();

	if(!QDesktopServices::openUrl(QUrl::fromLocalFile(FILE_EMAIL)))
	{
		QMessageBox::warning(this, APPNAME, tr("Could not start e-mail client!"));
	}
}

void MainWindow::on_actionSettings_triggered()
{
	int symbolsize = settings.chart.symbolsize;

	if(DialogSettings(this, &settings, plugins, &plugin).exec() == QDialog::Accepted)
	{
		actionImportDevice->setEnabled(!settings.device.plugin.isEmpty());

		actionSwitchUser1->setToolTip(tr("Switch To %1").arg(settings.user[0].name.isEmpty() ? tr("User 1") : settings.user[0].name));
		actionSwitchUser1->setStatusTip(tr("Switch To %1").arg(settings.user[0].name.isEmpty() ? tr("User 1") : settings.user[0].name));
		actionSwitchUser2->setToolTip(tr("Switch To %1").arg(settings.user[1].name.isEmpty() ? tr("User 2") : settings.user[1].name));
		actionSwitchUser2->setStatusTip(tr("Switch To %1").arg(settings.user[1].name.isEmpty() ? tr("User 2") : settings.user[1].name));

		saveSettings();

		if(symbolsize != settings.chart.symbolsize)
		{
			chartMainBP->removeSeries(scatterSeriesSYS);
			chartMainBP->removeSeries(scatterSeriesDIA);
			chartMainHR->removeSeries(scatterSeriesBPM);
			chartMainHR->removeSeries(scatterSeriesIHB);
			chartMainHR->removeSeries(scatterSeriesMOV);

			prepareScatterChart(chartMainBP, axisXBP, axisY1BP, scatterSeriesSYS);
			prepareScatterChart(chartMainBP, axisXBP, axisY1BP, scatterSeriesDIA);
			prepareScatterChart(chartMainHR, axisXHR, axisY1HR, scatterSeriesBPM);
			prepareScatterChart(chartMainHR, axisXHR, axisY1HR, scatterSeriesIHB);
			prepareScatterChart(chartMainHR, axisXHR, axisY1HR, scatterSeriesMOV);
		}

		modifyChartRanges();

		calcView();
	}
}

void MainWindow::on_actionSwitchUser1_triggered(bool checked)
{
	if(checked)
	{
		switchUser(0);
	}
}

void MainWindow::on_actionSwitchUser2_triggered(bool checked)
{
	if(checked)
	{
		switchUser(1);
	}
}

void MainWindow::on_actionTimeMode_triggered(bool checked)
{
	toolbarDateTime->setEnabled(checked);
	toolButton_prev->setEnabled(checked);
	toolButton_next->setEnabled(checked);

	if(checked)
	{
		widgetSlide->setVisible(false);
		widgetRange->setVisible(true);

		emit buttonGroup1->checkedButton()->toggled(true);
	}
	else
	{
		widgetRange->setVisible(false);
		widgetSlide->setVisible(true);

		emit buttonGroup2->checkedButton()->toggled(true);
	}
}

void MainWindow::on_actionQuit_triggered()
{
	close();
}

void MainWindow::on_actionAbout_triggered()
{
	QMessageBox::about(this, APPNAME, QString("<b>%1 %2 [ %3 ]</b><p>Qt %4<p>%5<hr>%6<p><table style='margin-left: 15'><tr><td><b>NL</b></td><td>André Simons</td></tr><tr><td><b>NO</b></td><td>Allan Nordhøy</td></tr><tr><td><b>RU</b></td><td>Val Orekhov</td></tr></table><hr><small style='color:red'>%7</small><hr>&copy; 2020-2021 <a href='mailto:lazyt@mailbox.org?subject=UBPM'>Thomas Löwe</a> - <a href='https://codeberg.org/lazyt/ubpm'>Git Repository</a>").arg(tr("Version"), APPVERS, APPDATE, QT_VERSION_STR, QSysInfo::prettyProductName(), tr("Thanks to all translators:"), tr("This program may be installed and used free of charge for non-commercial use on as many computers as you like without limitations. A liability for any damages resulting from the use is excluded. Use at your own risk.")));
}

void MainWindow::on_actionGuide_triggered()
{
	DialogHelp(this, "00").exec();
}

void MainWindow::on_actionTranslation_triggered()
{
	QDesktopServices::openUrl(QUrl("https://hosted.weblate.org/engage/ubpm"));
}

void MainWindow::on_actionBugreport_triggered()
{
	QDesktopServices::openUrl(QUrl("https://codeberg.org/lazyt/ubpm/issues"));
}

void MainWindow::on_actionDonateAmazon_triggered()
{
	QMessageBox::information(this, APPNAME, tr("Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify \"UBPM\" as message.\n\nThank you very much!"));

	QDesktopServices::openUrl(QUrl("https://amazon.com"));
}

void MainWindow::on_actionDonateLiberapay_triggered()
{
	QDesktopServices::openUrl(QUrl("https://liberapay.com/LazyT/donate"));
}

void MainWindow::on_actionDonatePaypal_triggered()
{
	QDesktopServices::openUrl(QUrl("https://paypal.me/LazyT"));
}

void MainWindow::on_actionDonateSEPA_triggered()
{
	QMessageBox::information(this, APPNAME, tr("Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.\n\nThank you very much!"));

	QDesktopServices::openUrl(QUrl("mailto:lazyt@mailbox.org?subject=UBPM SEPA&body="));
}

void MainWindow::on_actionUpdate_triggered()
{
	new DialogUpdate(this, true);
}

void MainWindow::languageChanged(QAction *action)
{
	settings.language = action->text();

	qApp->removeTranslator(&translatorApplication);
	qApp->removeTranslator(&translatorQtBase);

	if(action->data().isValid())
	{
		if(translatorApplication.load(QString("%1/ubpm_%2.qm").arg(DIR_LANGUAGES, action->data().toString())))
		{
			qApp->installTranslator(&translatorApplication);
		}
		else
		{
			QMessageBox::warning(isHidden() ? nullptr : this, APPNAME, tr("Loading application translation failed!"));
		}

		if(translatorQtBase.load(QString("%1/qtbase_%2.qm").arg(DIR_LANGUAGES, action->data().toString())))
		{
			qApp->installTranslator(&translatorQtBase);
		}
		else if(settings.qtbase)
		{
			if(QMessageBox::warning(isHidden() ? nullptr : this, APPNAME, tr("Loading Qt base translation for \"%1\" failed!\n\nInternal base translations (like \"Yes/No\") are not available.\n\nDon't show this warning again?").arg(action->data().toString().toUpper()), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
			{
				settings.qtbase = false;
			}
		}
	}
	else
	{
		if(translatorApplication.load(":/qm/plurals_en.qm"))
		{
			qApp->installTranslator(&translatorApplication);
		}
	}

	lcd->setToolTip(tr("Records For Selected User"));
	lcd->setStatusTip(tr("Records For Selected User"));

	toolbarDateTime->setToolTip(tr("Select Date & Time"));
	toolbarDateTime->setStatusTip(tr("Select Date & Time"));

	foreach(QAction *current, menuLanguage->actions())
	{
		if(!current->text().isEmpty())
		{
			current->setStatusTip(tr("Switch Language to %1").arg(current->text()));
		}
	}

	foreach(QAction *current, menuTheme->actions())
	{
		if(!current->text().isEmpty())
		{
			current->setStatusTip(tr("Switch Theme to %1").arg(current->text()));
		}
	}

	foreach(QAction *current, menuStyle->actions())
	{
		if(!current->text().isEmpty())
		{
			current->setStatusTip(tr("Switch Style to %1").arg(current->text()));
		}
	}

	lineSeriesSYS->setName(tr("Systolic"));
	lineSeriesDIA->setName(tr("Diastolic"));
	lineSeriesBPM->setName(tr("Heartrate"));

	chartSYS1->setTitle(QString("<center><b><u>%1</u></b><br></center>").arg(tr("Systolic - Value Range")));
	chartDIA1->setTitle(QString("<center><b><u>%1</u></b><br></center>").arg(tr("Diastolic - Value Range")));
	chartBPM1->setTitle(QString("<center><b><u>%1</u></b><br></center>").arg(tr("Heartrate - Value Range")));
	chartSYS2->setTitle(QString("<center><b><u>%1</u></b><br></center>").arg(tr("Systolic - Target Area")));
	chartDIA2->setTitle(QString("<center><b><u>%1</u></b><br></center>").arg(tr("Diastolic - Target Area")));
	chartBPM2->setTitle(QString("<center><b><u>%1</u></b><br></center>").arg(tr("Heartrate - Target Area")));

	setSYS1Min->setLabel(tr("Minimum"));
	setSYS1Max->setLabel(tr("Maximum"));
	setSYS1Avg->setLabel(tr("Average"));
	setSYS1Med->setLabel(tr("Median"));
	setDIA1Min->setLabel(tr("Minimum"));
	setDIA1Max->setLabel(tr("Maximum"));
	setDIA1Avg->setLabel(tr("Average"));
	setDIA1Med->setLabel(tr("Median"));
	setBPM1Min->setLabel(tr("Minimum"));
	setBPM1Max->setLabel(tr("Maximum"));
	setBPM1Avg->setLabel(tr("Average"));
	setBPM1Med->setLabel(tr("Median"));

	settings.email.subject = tr(settings.email.subject.toUtf8());
	settings.email.message = tr(settings.email.message.toUtf8());

	retranslateUi(this);

	if(actionTimeMode->isChecked())
	{
		emit buttonGroup1->checkedButton()->toggled(true);
	}

	actionSwitchUser1->setToolTip(tr("Switch To %1").arg(settings.user[0].name.isEmpty() ? tr("User 1") : settings.user[0].name));
	actionSwitchUser1->setStatusTip(tr("Switch To %1").arg(settings.user[0].name.isEmpty() ? tr("User 1") : settings.user[0].name));
	actionSwitchUser2->setToolTip(tr("Switch To %1").arg(settings.user[1].name.isEmpty() ? tr("User 2") : settings.user[1].name));
	actionSwitchUser2->setStatusTip(tr("Switch To %1").arg(settings.user[1].name.isEmpty() ? tr("User 2") : settings.user[1].name));

	calcView();
}

void MainWindow::styleChanged(QAction *action)
{
	settings.style = action->text();

	if(action->data().isValid())
	{
		QApplication::setStyle(action->data().toString());
	}
}

void MainWindow::themeChanged(QAction *action)
{
	settings.theme = action->text();

	if(action->data().isValid())
	{
		QFile file(DIR_THEMES + "/" + action->data().toString());

		if(file.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			theme = file.readAll();

			file.close();

			setStyleSheet(theme);
		}
		else
		{
			QMessageBox::warning(isHidden() ? nullptr : this, APPNAME, tr("Could not open theme \"%1\" file!\n\nReason: %2").arg(file.fileName(), file.errorString()));
		}
	}
	else
	{
		setStyleSheet("");
	}
}

void MainWindow::on_toolButton_prev_clicked()
{
	QAbstractButton *range = buttonGroup1->checkedButton();

	if(range == toolButton_15m)
	{
		timespan = timespan.addSecs(-15*60);

		changeTimeAxis("hh:mm", 16, 1, 15);
	}
	else if(range == toolButton_30m)
	{
		timespan = timespan.addSecs(-30*60);

		changeTimeAxis("hh:mm", 16, 2, 30);
	}
	else if(range == toolButton_hourly)
	{
		timespan = timespan.addSecs(-60*60);

		changeTimeAxis("hh:mm", 13, 5, 60);
	}
	else if(range == toolButton_6h)
	{
		timespan = timespan.addSecs(-6*60*60);

		changeTimeAxis("hh:mm", 25, 15, 6*60);
	}
	else if(range == toolButton_12h)
	{
		timespan = timespan.addSecs(-12*60*60);

		changeTimeAxis("hh:mm", 25, 30, 12*60);
	}
	else if(range == toolButton_daily)
	{
		timespan = timespan.addDays(-1);

		changeTimeAxis("hh:mm", 25, 60, 24*60);
	}
	else if(range == toolButton_weekly)
	{
		timespan = timespan.addDays(-7);

		changeTimeAxis("dddd", 7, 24*60, 7*24*60);
	}
	else if(range == toolButton_monthly)
	{
		timespan = timespan.addMonths(-1);

		changeTimeAxis("d", timespan.date().daysInMonth(), 24*60, timespan.date().daysInMonth()*24*60);
	}
	else if(range == toolButton_quarterly)
	{
		timespan = timespan.addMonths(-3);

		changeTimeAxis("MMMM", 3, 30*24*60, 90*24*60);
	}
	else if(range == toolButton_halfyearly)
	{
		timespan = timespan.addMonths(-6);

		changeTimeAxis("MMMM", 6, 30*24*60, 180*24*60);
	}
	else if(range == toolButton_yearly)
	{
		timespan = timespan.addYears(-1);

		changeTimeAxis("MMMM", 12, 30*24*60, 365*24*60);
	}
}

void MainWindow::on_toolButton_next_clicked()
{
	QAbstractButton *range = buttonGroup1->checkedButton();

	if(range == toolButton_15m)
	{
		timespan = timespan.addSecs(15*60);

		changeTimeAxis("hh:mm", 16, 1, 15);
	}
	else if(range == toolButton_30m)
	{
		timespan = timespan.addSecs(30*60);

		changeTimeAxis("hh:mm", 16, 2, 30);
	}
	else if(range == toolButton_hourly)
	{
		timespan = timespan.addSecs(60*60);

		changeTimeAxis("hh:mm", 13, 5, 60);
	}
	else if(range == toolButton_6h)
	{
		timespan = timespan.addSecs(6*60*60);

		changeTimeAxis("hh:mm", 25, 15, 6*60);
	}
	else if(range == toolButton_12h)
	{
		timespan = timespan.addSecs(12*60*60);

		changeTimeAxis("hh:mm", 25, 30, 12*60);
	}
	else if(range == toolButton_daily)
	{
		timespan = timespan.addDays(1);

		changeTimeAxis("hh:mm", 25, 60, 24*60);
	}
	else if(range == toolButton_weekly)
	{
		timespan = timespan.addDays(7);

		changeTimeAxis("dddd", 7, 24*60, 7*24*60);
	}
	else if(range == toolButton_monthly)
	{
		timespan = timespan.addMonths(1);

		changeTimeAxis("d", timespan.date().daysInMonth(), 24*60, timespan.date().daysInMonth()*24*60);
	}
	else if(range == toolButton_quarterly)
	{
		timespan = timespan.addMonths(3);

		changeTimeAxis("MMMM", 3, 30*24*60, 90*24*60);
	}
	else if(range == toolButton_halfyearly)
	{
		timespan = timespan.addMonths(6);

		changeTimeAxis("MMMM", 6, 30*24*60, 180*24*60);
	}
	else if(range == toolButton_yearly)
	{
		timespan = timespan.addYears(1);

		changeTimeAxis("MMMM", 12, 30*24*60, 365*24*60);
	}
}

void MainWindow::on_toolButton_15m_toggled(bool checked)
{
	if(checked)
	{
		changeTimeAxis("hh:mm", 16, 1, 15);
	}
}

void MainWindow::on_toolButton_30m_toggled(bool checked)
{
	if(checked)
	{
		changeTimeAxis("hh:mm", 16, 2, 30);
	}
}

void MainWindow::on_toolButton_hourly_toggled(bool checked)
{
	if(checked)
	{
		timespan.setTime(QTime(timespan.time().hour(), 0, 0, 0));

		changeTimeAxis("hh:mm", 13, 5, 60);
	}
}

void MainWindow::on_toolButton_6h_toggled(bool checked)
{
	if(checked)
	{
		int hour = timespan.time().hour();

		if(hour < 6)
		{
			hour = 0;
		}
		else if(hour < 12)
		{
			hour = 6;
		}
		else if(hour < 18)
		{
			hour = 12;
		}
		else
		{
			hour = 18;
		}

		timespan.setTime(QTime(hour, 0, 0, 0));

		changeTimeAxis("hh:mm", 25, 15, 6*60);
	}
}

void MainWindow::on_toolButton_12h_toggled(bool checked)
{
	if(checked)
	{
		int hour = timespan.time().hour();

		if(hour < 12)
		{
			hour = 0;
		}
		else
		{
			hour = 12;
		}

		timespan.setTime(QTime(hour, 0, 0, 0));

		changeTimeAxis("hh:mm", 25, 30, 12*60);
	}
}

void MainWindow::on_toolButton_daily_toggled(bool checked)
{
	if(checked)
	{
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("hh:mm", 25, 60, 24*60);
	}
}

void MainWindow::on_toolButton_weekly_toggled(bool checked)
{
	if(checked)
	{
		timespan = timespan.addDays(-(timespan.date().dayOfWeek() - 1));
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("dddd", 7, 24*60, 7*24*60);
	}
}

void MainWindow::on_toolButton_monthly_toggled(bool checked)
{
	if(checked)
	{
		timespan.setDate(QDate(timespan.date().year(), timespan.date().month(), 1));
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("d", timespan.date().daysInMonth(), 24*60, timespan.date().daysInMonth()*24*60);
	}
}

void MainWindow::on_toolButton_halfyearly_toggled(bool checked)
{
	if(checked)
	{
		int month = timespan.date().month();

		if(month < 7)
		{
			month = 1;
		}
		else
		{
			month = 7;
		}

		timespan.setDate(QDate(timespan.date().year(), month, 1));
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("MMMM", 6, 30*24*60, 180*24*60);
	}
}

void MainWindow::on_toolButton_quarterly_toggled(bool checked)
{
	if(checked)
	{
		int month = timespan.date().month();

		if(month < 4)
		{
			month = 1;
		}
		else if(month < 7)
		{
			month = 4;
		}
		else if(month < 10)
		{
			month = 7;
		}
		else
		{
			month = 10;
		}

		timespan.setDate(QDate(timespan.date().year(), month, 1));
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("MMMM", 3, 30*24*60, 90*24*60);
	}
}

void MainWindow::on_toolButton_yearly_toggled(bool checked)
{
	if(checked)
	{
		timespan.setDate(QDate(timespan.date().year(), 1, 1));
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("MMMM", 12, 30*24*60, 365*24*60);
	}
}

void MainWindow::on_toolButton_days7_toggled(bool checked)
{
	if(checked)
	{
		timespan = QDateTime::currentDateTime().addDays(-7);
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("dddd", 7, 24*60, 7*24*60);
	}
}

void MainWindow::on_toolButton_days14_toggled(bool checked)
{
	if(checked)
	{
		timespan = QDateTime::currentDateTime().addDays(-14);
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("dddd", 14, 24*60, 14*24*60);
	}
}

void MainWindow::on_toolButton_days21_toggled(bool checked)
{
	if(checked)
	{
		timespan = QDateTime::currentDateTime().addDays(-21);
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("ddd", 21, 24*60, 21*24*60);
	}
}

void MainWindow::on_toolButton_days28_toggled(bool checked)
{
	if(checked)
	{
		timespan = QDateTime::currentDateTime().addDays(-28);
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("ddd", 28, 24*60, 28*24*60);
	}
}

void MainWindow::on_toolButton_months3_toggled(bool checked)
{
	if(checked)
	{
		timespan = QDateTime::currentDateTime().addDays(-90);
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("MMMM", 3, 30*24*60, 90*24*60);
	}
}

void MainWindow::on_toolButton_months6_toggled(bool checked)
{
	if(checked)
	{
		timespan = QDateTime::currentDateTime().addDays(-180);
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("MMMM", 6, 30*24*60, 180*24*60);
	}
}

void MainWindow::on_toolButton_months9_toggled(bool checked)
{
	if(checked)
	{
		timespan = QDateTime::currentDateTime().addDays(-270);
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("MMMM", 9, 30*24*60, 270*24*60);
	}
}

void MainWindow::on_toolButton_months12_toggled(bool checked)
{
	if(checked)
	{
		timespan = QDateTime::currentDateTime().addDays(-365);
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("MMMM", 12, 30*24*60, 365*24*60);
	}
}

void MainWindow::on_toolButton_all_toggled(bool checked)
{
	if(checked)
	{
		timespan = database[view.user].count() ? QDateTime::fromMSecsSinceEpoch(database[view.user].first().dts) : QDateTime::currentDateTime();
		timespan.setTime(QTime(0, 0, 0, 0));

		changeTimeAxis("dd.MM.yyyy", 7, 0, 0);
	}
}

void MainWindow::toolbarDateTimeChanged(QDateTime datetime)
{
	int min = datetime.time().minute();
	int mod = min % 15;

	if(min != 0 && min != 15 && min != 30 && min != 45)
	{
		int round = min - mod + (mod / 15.0 >= 0.5 ? 15 : 0);

		if(round == 60)
		{
			toolbarDateTime->setTime(QTime(datetime.time().hour() + 1, 0));

		}
		else
		{
			toolbarDateTime->setTime(QTime(datetime.time().hour(), round));
		}

		return;
	}

	timespan = datetime;

	if(min == 15 || min == 45)
	{
		toolButton_15m->isChecked() ? on_toolButton_15m_toggled(true) : toolButton_15m->setChecked(true);
	}
	else if(min == 30)
	{
		toolButton_30m->isChecked() ? on_toolButton_30m_toggled(true) : toolButton_30m->setChecked(true);
	}
	else if(datetime.time().hour())
	{
		toolButton_hourly->isChecked() ? on_toolButton_hourly_toggled(true) : toolButton_hourly->setChecked(true);
	}
	else
	{
		toolButton_daily->isChecked() ? on_toolButton_daily_toggled(true) : toolButton_daily->setChecked(true);
	}
}

void MainWindow::tableItemChanged(QTableWidgetItem *item)
{
	int row = tableWidget->row(item);
	qint64 dts = QDateTime::fromString(tableWidget->item(row, 0)->text() + tableWidget->item(row, 1)->text(), "yyyy-MM-ddhh:mm:ss.zzz").toMSecsSinceEpoch();

	for(int i = 0; i < database[view.user].count(); i++)
	{
		if(database[view.user].at(i).dts == dts)
		{
			switch(item->column())
			{
				case 2: database[view.user][i].sys = item->text().toInt();
					break;

				case 3: database[view.user][i].dia = item->text().toInt();
					break;

				case 5: database[view.user][i].bpm = item->text().toInt();
					break;

				case 6: database[view.user][i].ihb = item->text().toInt() & 1;
					break;

				case 7: database[view.user][i].mov = item->text().toInt() & 1;
					break;

				case 8: database[view.user][i].inv = item->text().toInt() & 1;
					break;

				case 9: database[view.user][i].msg = item->text();
					break;
			}

			calcView();

			break;
		}
	}
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
	if(index == 0)
	{
		showChart();
	}
	else if(index == 1)
	{
		showTable();
	}
	else if(index == 2)
	{
		showStats();
	}
}

void MainWindow::wheelEvent(QWheelEvent *event)
{
	if(actionTimeMode->isChecked())
	{
		QObject *parent = childAt(event->position().toPoint())->parent()->parent();

		if(parent)
		{
			if(parent->objectName() == "tab_chart" || parent->objectName() == "tab_table" || parent->objectName() == "tab_statistic")
			{
				if(event->angleDelta().y() > 0)
				{
					on_toolButton_next_clicked();
				}
				else
				{
					on_toolButton_prev_clicked();
				}
			}
		}
	}
}

void MainWindow::customContextMenuRequestedChart(QPoint pos)
{
	QMenu *menu = new QMenu(this);

	QAction *dynamic = menu->addAction(tr("Dynamic Scaling"));
	QAction *colored = menu->addAction(tr("Colored Stripes"));
	menu->addSeparator();
	QAction *symbols = menu->addAction(tr("Show Symbols"));
	QAction *lines = menu->addAction(tr("Show Lines"));
	menu->addSeparator();
	QAction *heartrate = menu->addAction(tr("Show Heartrate"));

	dynamic->setCheckable(true);
	colored->setCheckable(true);
	symbols->setCheckable(true);
	lines->setCheckable(true);
	heartrate->setCheckable(true);

	dynamic->setChecked(settings.chart.dynamic);
	colored->setChecked(settings.chart.colored);
	symbols->setChecked(settings.chart.symbols);
	lines->setChecked(settings.chart.lines);
	heartrate->setChecked(settings.chart.heartrate);

	QAction *action = menu->exec(dynamic_cast <QChartView*>(sender())->viewport()->mapToGlobal(pos));

	if(action)
	{
		if(action == dynamic)
		{
			settings.chart.dynamic = action->isChecked();
		}
		else if(action == colored)
		{
			settings.chart.colored = action->isChecked();
		}
		else if(action == symbols)
		{
			settings.chart.symbols = action->isChecked();
		}
		else if(action == lines)
		{
			settings.chart.lines = action->isChecked();
		}
		else if(action == heartrate)
		{
			settings.chart.heartrate = action->isChecked();
		}

		calcView();
	}
}

void MainWindow::customContextMenuRequestedTable(QPoint pos)
{
	QTableWidgetItem *twi = tableWidget->itemAt(pos);

	if(twi)
	{
		int row = tableWidget->row(twi);

		QMenu *menu = new QMenu(this);

		QAction *del = menu->addAction(QIcon(":/svg/delete.svg"), tr("Delete record"));
		menu->addSeparator();
		QAction *show = menu->addAction(QIcon(":/svg/visible.svg"), tr("Show record"));
		QAction *hide = menu->addAction(QIcon(":/svg/invisible.svg"), tr("Hide record"));
		tableWidget->item(row, 7)->data(Qt::DisplayRole).toBool() ? hide->setEnabled(false) : show->setEnabled(false);

		QAction *action = menu->exec(tableWidget->viewport()->mapToGlobal(pos));

		if(action)
		{
			qint64 dts = QDateTime::fromString(tableWidget->item(row, 0)->text() + tableWidget->item(row, 1)->text(), "yyyy-MM-ddhh:mm:ss.zzz").toMSecsSinceEpoch();

			if(action == del)
			{
				if(QMessageBox::question(this, APPNAME, tr("Really delete selected record?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
				{
					recordDel(view.user, dts);
				}
			}
			else if(action == hide)
			{
				recordHide(view.user, dts, true);
			}
			else if(action == show)
			{
				recordHide(view.user, dts, false);
			}
		}
	}
}

void MainWindow::customContextMenuRequestedStats(QPoint pos)
{
	QMenu *menu = new QMenu(this);

	QAction *median = menu->addAction(tr("Show Median"));
	QAction *legend = menu->addAction(tr("Show Values"));

	median->setCheckable(true);
	legend->setCheckable(true);

	median->setChecked(settings.stats.median);
	legend->setChecked(settings.stats.legend);

	QAction *action = menu->exec(reinterpret_cast<QChartView*>(sender())->viewport()->mapToGlobal(pos));

	if(action)
	{
		if(action == median)
		{
			settings.stats.median = action->isChecked();
		}
		else if(action == legend)
		{
			settings.stats.legend = action->isChecked();
		}

		calcView();
	}
}

void MainWindow::keyPressEvent(QKeyEvent *ke)
{
	if(ke->key() == Qt::Key_F1)
	{
		DialogHelp(this, QString("01-01-%1").arg(tabWidget->currentIndex() + 1, 2, 10, QChar('0'))).exec();
	}
	else if(ke->key() == Qt::Key_Escape)
	{
		close();
	}

	QMainWindow::keyPressEvent(ke);
}

void MainWindow::closeEvent(QCloseEvent *ce)
{
	if(forced || QMessageBox::question(this, APPNAME, tr("Really quit program?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
	{
		if(!isMaximized())
		{
			settings.geometry = saveGeometry();
		}

		settings.mode = actionTimeMode->isChecked();
		settings.range = buttonGroup1->checkedId();
		settings.slide = buttonGroup2->checkedId();

		saveSettings();

		if(sqlcipher)
		{
			QFile::remove(settings.database.location);
		}

		if(database[0].count() || database[1].count())
		{
			exportToSQL(settings.database.location);
		}

		ce->accept();
	}
	else
	{
		ce->ignore();
	}
}
