#!/bin/sh

##########################
# create new index files #
##########################

clear

if [ -z $1 ]; then
	echo "Missing language parameter!"
	exit 1
fi

if [ -e help/$1.qhp ]; then
	echo "Index for $1 already exist!"
else
	echo "Generating new index for $1..."

	# copy en to xx

	cp help/en.qhcp help/$1.qhcp
	cp help/en.qhp help/$1.qhp

	# extract en strings

	EN[0]=$(grep \"00.html\" help/en.qhp | cut -d'"' -f2)
	EN[1]=$(grep \"01.html\" help/en.qhp | cut -d'"' -f2)
	EN[2]=$(grep \"01-01.html\" help/en.qhp | cut -d'"' -f2)
	EN[3]=$(grep \"01-01-01.html\" help/en.qhp | cut -d'"' -f2)
	EN[4]=$(grep \"01-01-02.html\" help/en.qhp | cut -d'"' -f2)
	EN[5]=$(grep \"01-01-03.html\" help/en.qhp | cut -d'"' -f2)
	EN[6]=$(grep \"01-02.html\" help/en.qhp | cut -d'"' -f2)
	EN[7]=$(grep \"01-02-01.html\" help/en.qhp | cut -d'"' -f2)
	EN[8]=$(grep \"01-02-02.html\" help/en.qhp | cut -d'"' -f2)
	EN[9]=$(grep \"01-02-03.html\" help/en.qhp | cut -d'"' -f2)
	EN[10]=$(grep \"01-02-04.html\" help/en.qhp | cut -d'"' -f2)
	EN[11]=$(grep \"01-02-05.html\" help/en.qhp | cut -d'"' -f2)
	EN[12]=$(grep \"01-02-06.html\" help/en.qhp | cut -d'"' -f2)
	EN[13]=$(grep \"01-02-07.html\" help/en.qhp | cut -d'"' -f2)
	EN[14]=$(grep \"01-02-08.html\" help/en.qhp | cut -d'"' -f2)
	EN[15]=$(grep \"01-03.html\" help/en.qhp | cut -d'"' -f2)
	EN[16]=$(grep \"01-04.html\" help/en.qhp | cut -d'"' -f2)
	EN[17]=$(grep \"01-05.html\" help/en.qhp | cut -d'"' -f2)
	EN[18]=$(grep \"01-06.html\" help/en.qhp | cut -d'"' -f2)
	EN[19]=$(grep \"02.html\" help/en.qhp | cut -d'"' -f2)
	EN[20]=$(grep \"02-01.html\" help/en.qhp | cut -d'"' -f2)
	EN[21]=$(grep \"02-01-01.html\" help/en.qhp | cut -d'"' -f2)
	EN[22]=$(grep \"02-01-02.html\" help/en.qhp | cut -d'"' -f2)
	EN[23]=$(grep \"02-01-03.html\" help/en.qhp | cut -d'"' -f2)
	EN[24]=$(grep \"02-01-04.html\" help/en.qhp | cut -d'"' -f2)
	EN[25]=$(grep \"02-02.html\" help/en.qhp | cut -d'"' -f2)
	EN[26]=$(grep \"02-02-01.html\" help/en.qhp | cut -d'"' -f2)
	EN[27]=$(grep \"02-02-02.html\" help/en.qhp | cut -d'"' -f2)
	EN[28]=$(grep \"02-02-03.html\" help/en.qhp | cut -d'"' -f2)
	EN[29]=$(grep \"02-02-04.html\" help/en.qhp | cut -d'"' -f2)
	EN[30]=$(grep \"02-03.html\" help/en.qhp | cut -d'"' -f2)
	EN[31]=$(grep \"02-03-01.html\" help/en.qhp | cut -d'"' -f2)
	EN[32]=$(grep \"02-03-02.html\" help/en.qhp | cut -d'"' -f2)
	EN[33]=$(grep \"03.html\" help/en.qhp | cut -d'"' -f2)

	# extract xx strings

	XX[0]=$(jq -r '.["00"] | .["001"]' html/lng/$1.json)
	XX[1]=$(jq -r '.["01"] | .["001"]' html/lng/$1.json)
	XX[2]=$(jq -r '.["01-01"] | .["001"]' html/lng/$1.json)
	XX[3]=$(jq -r '.["01-01-01"] | .["001"]' html/lng/$1.json)
	XX[4]=$(jq -r '.["01-01-02"] | .["001"]' html/lng/$1.json)
	XX[5]=$(jq -r '.["01-01-03"] | .["001"]' html/lng/$1.json)
	XX[6]=$(jq -r '.["01-02"] | .["001"]' html/lng/$1.json)
	XX[7]=$(jq -r '.["01-02-01"] | .["001"]' html/lng/$1.json)
	XX[8]=$(jq -r '.["01-02-02"] | .["001"]' html/lng/$1.json)
	XX[9]=$(jq -r '.["01-02-03"] | .["001"]' html/lng/$1.json)
	XX[10]=$(jq -r '.["01-02-04"] | .["001"]' html/lng/$1.json)
	XX[11]=$(jq -r '.["01-02-05"] | .["001"]' html/lng/$1.json)
	XX[12]=$(jq -r '.["01-02-06"] | .["001"]' html/lng/$1.json)
	XX[13]=$(jq -r '.["01-02-07"] | .["001"]' html/lng/$1.json)
	XX[14]=$(jq -r '.["01-02-08"] | .["001"]' html/lng/$1.json)
	XX[15]=$(jq -r '.["01-03"] | .["001"]' html/lng/$1.json)
	XX[16]=$(jq -r '.["01-04"] | .["001"]' html/lng/$1.json)
	XX[17]=$(jq -r '.["01-05"] | .["001"]' html/lng/$1.json)
	XX[18]=$(jq -r '.["01-06"] | .["001"]' html/lng/$1.json)
	XX[19]=$(jq -r '.["02"] | .["001"]' html/lng/$1.json)
	XX[20]=$(jq -r '.["02-01"] | .["001"]' html/lng/$1.json)
	XX[21]=$(jq -r '.["02-01-01"] | .["001"]' html/lng/$1.json)
	XX[22]=$(jq -r '.["02-01-02"] | .["001"]' html/lng/$1.json)
	XX[23]=$(jq -r '.["02-01-03"] | .["001"]' html/lng/$1.json)
	XX[24]=$(jq -r '.["02-01-04"] | .["001"]' html/lng/$1.json)
	XX[25]=$(jq -r '.["02-02"] | .["001"]' html/lng/$1.json)
	XX[26]=$(jq -r '.["02-02-01"] | .["001"]' html/lng/$1.json)
	XX[27]=$(jq -r '.["02-02-02"] | .["001"]' html/lng/$1.json)
	XX[28]=$(jq -r '.["02-02-03"] | .["001"]' html/lng/$1.json)
	XX[29]=$(jq -r '.["02-02-04"] | .["001"]' html/lng/$1.json)
	XX[30]=$(jq -r '.["02-03"] | .["001"]' html/lng/$1.json)
	XX[31]=$(jq -r '.["02-03-01"] | .["001"]' html/lng/$1.json)
	XX[32]=$(jq -r '.["02-03-02"] | .["001"]' html/lng/$1.json)
	XX[33]=$(jq -r '.["03"] | .["001"]' html/lng/$1.json)

	# replace en strings with xx strings

	sed -i "s/en\./$1\./g" help/$1.qhcp
	sed -i "s/>en</>$1</g" help/$1.qhp

	for i in {0..32}; do
		echo $i	[${EN[$i]}] = [${XX[$i]}]
		sed -i "s/\"${EN[$i]}\"/\"${XX[$i]}\"/g" help/$1.qhp
	done
fi
