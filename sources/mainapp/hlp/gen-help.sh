#!/bin/sh

#########################
# create new help files #
#########################

clear

TMP=/tmp/ubpm-help

[ -d $TMP ] && rm -r $TMP && mkdir $TMP

for LNG in en de nl; do

	echo -e "\nBuilding $LNG guide...\n"

	# translate html

	static-i18n -l $LNG -i $LNG --localesPath=html/lng --output-dir=$TMP/$LNG html

	# prepare images

	cp -r html/css html/img $TMP/$LNG

	if [ -d $TMP/$LNG/img/$LNG ]; then
		cp $TMP/$LNG/img/$LNG/*.png $TMP/$LNG/img
	else
		echo -e "\033[0;31mMissing screenshots!\033[0m"
		cp $TMP/$LNG/img/en/*.png $TMP/$LNG/img
	fi

	cp help/$LNG.qh* $TMP/$LNG

	# generate qthelp

	qhelpgenerator $TMP/$LNG/$LNG.qhp  -o $TMP/$LNG.qch
	qhelpgenerator $TMP/$LNG/$LNG.qhcp -o $TMP/$LNG.qhc

done

cp $TMP/*.q* .
