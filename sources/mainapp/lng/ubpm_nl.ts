<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Data analyse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Vraag</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="203"/>
        <source>Results</source>
        <translation>Resultaat</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>Pulsdruk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation>Hartslag</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>Onregelmatig</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Movement</source>
        <translation>Beweging</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Invisible</source>
        <translation>Onzichtbaar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="169"/>
        <source>Comment</source>
        <translation>Kommentaar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="199"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="48"/>
        <source>Could not create memory database!

%1</source>
        <translation>Kon geen geheugendatabase maken 

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="205"/>
        <source>No results for this query found!</source>
        <translation>Geen resultaat gevonden voor deze aanvraag!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 voor %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Resultaat</numerusform>
            <numerusform>Resultaten</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>User %1</source>
        <translation>Gebruiker %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Treffer</numerusform>
            <numerusform>Treffers</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Record</numerusform>
            <numerusform>Records</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Handboek</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="19"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>%1 Handboek niet gevonden, toon EN in plaats daarvan.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="56"/>
        <source>%1 guide not found!</source>
        <translation>%1 Handboek niet gevonden!</translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Handmatige opname</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Gegevensrecord</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="220"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Gegevensrecord voor %1 toevoegen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Kies datum en tijd</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>SYS ingeven</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="151"/>
        <source>Enter DIA</source>
        <translation>DIA ingeven</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="173"/>
        <source>Enter BPM</source>
        <translation>Hartslag ingeven</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Onregelmatige hartslag</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="195"/>
        <source>Movement</source>
        <translation>Beweging</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="248"/>
        <source>Enter Optional Comment</source>
        <translation>Optioneel Kommentaarveld</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="278"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Toon bericht bij succesvol creeren / verwijderen / veranderen van gegevensrecord</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="296"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="313"/>
        <source>Create</source>
        <translation>Creeren</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="333"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Gebruiker 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Gebruiker 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="30"/>
        <location filename="../DialogRecord.cpp" line="99"/>
        <source>Modify</source>
        <translation>Veranderen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="46"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>De gegevensrecord kon niet verwijderd worden!

Geen record gevonden voor deze datum en tijd.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="52"/>
        <source>Data record successfully deleted.</source>
        <translation>De gegevensrecord is succesvol verwijderd.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="62"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Een geldige waarde voor &quot;SYS&quot; ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="72"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Eerst een geldige waarde voor &quot;DIA&quot; ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="81"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Eerst een geldige waarde voor &quot;Hartslag&quot; ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="103"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>De gegevensrecord kon niet veranderd worden!

Geen record voor deze datum en tijd gevonden.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="109"/>
        <source>Data Record successfully modified.</source>
        <translation>Gegevensrecord succesvol veranderd.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="118"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>De gegevensrecord kon niet aangemaakt worden!

Er bestaat reeds een record voor deze datum/tijd combinatie.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="124"/>
        <source>Data Record successfully created.</source>
        <translation>Gegevensrecord succesvol aangemaakt.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="138"/>
        <source>Choose Database Location</source>
        <translation>Kies database locatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="174"/>
        <source>Could not display manual!

%1</source>
        <translation>Het handboek kan niet getoond worden!

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="301"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>SQL-codering kan niet worden ingeschakeld zonder wachtwoord en wordt uitgeschakeld!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="308"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Geldige waarden voor additionele informatie voor gebruiker 1 ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="315"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Geldige waarden voor additionele informatie voor gebruiker 2 ingeven aub!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="322"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>De ingevoerde leeftijd komt niet overeen met de geselecteerde leeftijdsgroep voor gebruiker 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="329"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>De ingevoerde leeftijd komt niet overeen met de geselecteerde leeftijdsgroep voor gebruiker 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="336"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Gelieve een geldig e-mailadres in te geven!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="343"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Gelieve een geldig e-mail onderwerp in te geven!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="350"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>E-Mail bericht moet $CHART, $TABLE en/of $STATS bevatten!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="442"/>
        <source>User 1</source>
        <translation>Gebruiker 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="456"/>
        <source>User 2</source>
        <translation>Gebruiker 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="568"/>
        <source>Blood Pressure Report</source>
        <translation>Bloeddrukbericht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="569"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Geachte Dr. House,

bijgevoegd vindt u mijn bloeddrukgegevens van deze maand.

Met vriendelijke groet, 
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="601"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Installatie afbreken en alle wijzigingen negeren?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="50"/>
        <source>Location</source>
        <translation>Locatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="56"/>
        <source>Current Location</source>
        <translation>Huidige locatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="72"/>
        <source>Change Location</source>
        <translation>Locatie veranderen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="98"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Versleutel met SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="101"/>
        <source>Encryption</source>
        <translation>Versleutelen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="113"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="126"/>
        <source>Show Password</source>
        <translation>Wachtwoord tonen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="154"/>
        <source>User</source>
        <translation>Gebruiker</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="189"/>
        <location filename="../DialogSettings.ui" line="440"/>
        <source>Male</source>
        <translation>Man</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="218"/>
        <location filename="../DialogSettings.ui" line="469"/>
        <source>Female</source>
        <translation>Vrouw</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="308"/>
        <location filename="../DialogSettings.ui" line="568"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="183"/>
        <location filename="../DialogSettings.ui" line="434"/>
        <source>Mandatory Information</source>
        <translation>Verplicht veld</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="244"/>
        <location filename="../DialogSettings.ui" line="504"/>
        <source>Age Group</source>
        <translation>Leeftijdsgroep</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="324"/>
        <location filename="../DialogSettings.ui" line="584"/>
        <source>Additional Information</source>
        <translation>Extra informatie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="378"/>
        <location filename="../DialogSettings.ui" line="638"/>
        <source>Height</source>
        <translation>Lengte</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="407"/>
        <location filename="../DialogSettings.ui" line="667"/>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="692"/>
        <source>Device</source>
        <translation>Apparaat</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="698"/>
        <source>Import Plugins</source>
        <translation>Plug-ins importeren</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="722"/>
        <source>Show Device Image</source>
        <translation>Apparaatafbeelding weergeven</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="745"/>
        <source>Show Device Manual</source>
        <translation>Apparaathandleiding weergeven</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Open Website</source>
        <translation>Website openen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="803"/>
        <source>Maintainer</source>
        <translation>Onderhouder</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="831"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="838"/>
        <source>Send E-Mail</source>
        <translation>E-mail versturen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="851"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="858"/>
        <source>Producer</source>
        <translation>Fabrikant</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="875"/>
        <source>Chart</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="978"/>
        <source>X-Axis Range</source>
        <translation>X-as bereik</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="984"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamisch schalen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="965"/>
        <source>Healthy Ranges</source>
        <translation>Gezond bereik</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="906"/>
        <source>Symbols</source>
        <translation>Symbolen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="959"/>
        <source>Colored Areas</source>
        <translation>Gekleurde gebieden</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="352"/>
        <location filename="../DialogSettings.ui" line="612"/>
        <source>Birth Year</source>
        <translation>Geboortejaar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="707"/>
        <source>Please choose Device Plugin…</source>
        <translation>Kies a.u.b. Device Plugin …</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1106"/>
        <location filename="../DialogSettings.ui" line="1381"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1194"/>
        <location filename="../DialogSettings.ui" line="1469"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1282"/>
        <location filename="../DialogSettings.ui" line="1557"/>
        <source>Heartrate</source>
        <translation>Hartslag</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1654"/>
        <source>Table</source>
        <translation>Tabel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1739"/>
        <location filename="../DialogSettings.ui" line="1922"/>
        <source>Systolic Warnlevel</source>
        <translation>Systolisch waarschuwingsniveau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1685"/>
        <location filename="../DialogSettings.ui" line="1982"/>
        <source>Diastolic Warnlevel</source>
        <translation>Diastolisch waarschuwingsniveau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="915"/>
        <location filename="../DialogSettings.ui" line="943"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="997"/>
        <source>Lines</source>
        <translation>Lijnen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1006"/>
        <location filename="../DialogSettings.ui" line="1034"/>
        <source>Width</source>
        <translation>breedte</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1063"/>
        <source>Show Heartrate</source>
        <translation>Hartslag tonen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1069"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Hartslag weergeven in kaartweergave</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1079"/>
        <source>Print Heartrate</source>
        <translation>Hartslag afdrukken</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1085"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Druk de hartslag af op een aparte bladzijde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1799"/>
        <location filename="../DialogSettings.ui" line="2040"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Pulsdruk waarschuwingsniveau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1853"/>
        <location filename="../DialogSettings.ui" line="2094"/>
        <source>Heartrate Warnlevel</source>
        <translation>Hartslag waarschuwingsniveau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2159"/>
        <source>Statistic</source>
        <translation>Statistiek</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2165"/>
        <source>Bar Type</source>
        <translation>Type staaf</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2171"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Toon mediaan in plaats van gemiddelde balken</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2181"/>
        <source>Legend Type</source>
        <translation>Type legende</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2187"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Toon waarden als legenda in plaats van beschrijvingen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2202"/>
        <source>E-Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2210"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2229"/>
        <source>Subject</source>
        <translation>Onderwerp</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2247"/>
        <source>Message</source>
        <translation>Bericht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2268"/>
        <source>Update</source>
        <translation>Aanpassen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2274"/>
        <source>Autostart</source>
        <translation>Automatische start</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2280"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Controleer op online updates bij het opstarten van het programma</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2293"/>
        <source>Notification</source>
        <translation>Kennisgeving</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2299"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Resultaat altijd weergeven na controle van online updates</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2315"/>
        <source>Save</source>
        <translation>Opslaan</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2332"/>
        <source>Reset</source>
        <translation>Terugzetten</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2349"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Online update</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Beschikbare Versie</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Update bestandsgrootte</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Geinstalleerde versie</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="196"/>
        <source>Download</source>
        <translation>Downloaden</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="216"/>
        <source>Ignore</source>
        <translation>negeren</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="162"/>
        <source>No new version found.</source>
        <translation>Geen nieuwe versie gevonden.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! SSL WAARSCHUWING - ZORGVULDIG LEZEN !!!

Netwerk verbindingsprobleem:

%1
Wil je toch doorgaan?</numerusform>
            <numerusform>!!! SSL WAARSCHUWING - ZORGVULDIG LEZEN !!!

Netwerk verbindingsproblemen:

%1
Wil je toch doorgaan?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Downloading update failed!

%1</source>
        <translation>Het downloaden van de update is mislukt!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Checking update failed!

%1</source>
        <translation>Het controleren van de update is mislukt!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="135"/>
        <source>Unexpected response from update server!</source>
        <translation>Onverwachte reactie van updateserver!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="182"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>Update heeft niet de verwachte grootte!

%L1:%L2

Opnieuw proberen…</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="186"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Update opgeslagen in %1

Nieuwe versie starten?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="199"/>
        <source>Could not start new version!</source>
        <translation>Kan nieuwe versie niet starten!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="242"/>
        <source>Really abort download?</source>
        <translation>Download echt afbreken?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="206"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Kan update niet opslagen in %1

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="20"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universele Bloeddruk Manager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Grafiekweergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Tabel weergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="3440"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="78"/>
        <location filename="../MainWindow.cpp" line="3441"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pulsdruk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Onregelmatig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Beweging</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Onzichtbaar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Kommentaar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Statistiek weergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>per kwartier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>per half uur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>per uur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>¼ Dag</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>½ Dag</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Per dag</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation>per week</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation>per maand</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation>per kwartaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation>halfjaarlijks</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation>Jaarlijks</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation>Laatste 7 dagen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation>Laatste 14 dagen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation>Laatste 21 dagen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation>Laatste 28 dagen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation>Laatste 3 maanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation>Laatste 6 maanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation>Laatste 9 maanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation>Laatste 12 maanden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation>Alle records</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation>Afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Configuration</source>
        <translation>Konfiguratie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1161"/>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1170"/>
        <source>Language</source>
        <translation>Taal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1179"/>
        <source>Style</source>
        <translation>Stijl</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1196"/>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1200"/>
        <source>Import</source>
        <translation>Importeren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1212"/>
        <source>Export</source>
        <translation>Exporteren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Clear</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1307"/>
        <source>Quit</source>
        <translation>Stoppen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1310"/>
        <location filename="../MainWindow.ui" line="1313"/>
        <source>Quit Program</source>
        <translation>Programma stoppen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1325"/>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1328"/>
        <location filename="../MainWindow.ui" line="1331"/>
        <source>About Program</source>
        <translation>Over dit programma</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1340"/>
        <source>Guide</source>
        <translation>Handboek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1343"/>
        <location filename="../MainWindow.ui" line="1346"/>
        <source>Show Guide</source>
        <translation>Toon handboek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1355"/>
        <source>Update</source>
        <translation>Aktualiseren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1358"/>
        <location filename="../MainWindow.ui" line="1361"/>
        <source>Check Update</source>
        <translation>Controleer Update</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1752"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1755"/>
        <location filename="../MainWindow.ui" line="1758"/>
        <source>Donate via PayPal</source>
        <translation>Schenken via PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1770"/>
        <location filename="../MainWindow.ui" line="1773"/>
        <source>Donate via Liberapay</source>
        <translation>Schenken via Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1785"/>
        <location filename="../MainWindow.ui" line="1788"/>
        <source>Donate via Amazon</source>
        <translation>Schenken via Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1800"/>
        <location filename="../MainWindow.ui" line="1803"/>
        <source>Donate via SEPA</source>
        <translation>Schenken via SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1812"/>
        <source>Translation</source>
        <translation>Vertaling</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1815"/>
        <location filename="../MainWindow.ui" line="1818"/>
        <source>Contribute Translation</source>
        <translation>Meehelpen aan de vertaling</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1827"/>
        <source>E-Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1830"/>
        <location filename="../MainWindow.ui" line="1833"/>
        <source>Send E-Mail</source>
        <translation>E-mail versturen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1132"/>
        <source>Donation</source>
        <translation>Schenken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="79"/>
        <location filename="../MainWindow.cpp" line="3442"/>
        <source>Heartrate</source>
        <translation>Hartslag</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1129"/>
        <source>Make Donation</source>
        <translation>Schenken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1373"/>
        <source>Bugreport</source>
        <translation>Foutrapport</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1376"/>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Send Bugreport</source>
        <translation>Foutrapport sturen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1388"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1391"/>
        <location filename="../MainWindow.ui" line="1394"/>
        <source>Change Settings</source>
        <translation>Instellingen veranderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1409"/>
        <source>From Device</source>
        <translation>Van Apparaat</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <location filename="../MainWindow.ui" line="1415"/>
        <source>Import From Device</source>
        <translation>Importeren van Apparaat</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>From File</source>
        <translation>Van File</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Import From File</source>
        <translation>Importeren van File</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1442"/>
        <source>From Input</source>
        <translation>Van Input</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <location filename="../MainWindow.ui" line="1448"/>
        <source>Import From Input</source>
        <translation>Importeren van Input</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1457"/>
        <source>To CSV</source>
        <translation>Naar CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <location filename="../MainWindow.ui" line="1463"/>
        <source>Export To CSV</source>
        <translation>Exporteren naar CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1475"/>
        <source>To XML</source>
        <translation>Naar XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1478"/>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>Export To XML</source>
        <translation>Exporteren naar XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1490"/>
        <source>To JSON</source>
        <translation>Naar JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1493"/>
        <location filename="../MainWindow.ui" line="1496"/>
        <source>Export To JSON</source>
        <translation>Exporteren naar JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>To SQL</source>
        <translation>Naar SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1508"/>
        <location filename="../MainWindow.ui" line="1511"/>
        <source>Export To SQL</source>
        <translation>Exporteren naar SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Print Chart</source>
        <translation>Grafiek afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1523"/>
        <location filename="../MainWindow.ui" line="1526"/>
        <source>Print Chart View</source>
        <translation>Grafiekweergave afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Print Table</source>
        <translation>Tabel afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1538"/>
        <location filename="../MainWindow.ui" line="1541"/>
        <source>Print Table View</source>
        <translation>Tabelweergave afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Print Statistic</source>
        <translation>Statistiek afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1553"/>
        <location filename="../MainWindow.ui" line="1556"/>
        <source>Print Statistic View</source>
        <translation>Statistiekweergave afdrukken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Preview Chart</source>
        <translation>Voorbeeld grafiek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1568"/>
        <location filename="../MainWindow.ui" line="1571"/>
        <source>Preview Chart View</source>
        <translation>Voorbeeld van grafiekweergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <source>Preview Table</source>
        <translation>Voorbeeld tabel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1583"/>
        <location filename="../MainWindow.ui" line="1586"/>
        <source>Preview Table View</source>
        <translation>Voorbeeld van tabelweergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <source>Preview Statistic</source>
        <translation>Voorbeeld statistiek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1598"/>
        <location filename="../MainWindow.ui" line="1601"/>
        <source>Preview Statistic View</source>
        <translation>Voorbeeld van statistiekweergave</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <location filename="../MainWindow.ui" line="1616"/>
        <source>Clear All</source>
        <translation>Alles verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <location filename="../MainWindow.ui" line="1634"/>
        <source>Clear User 1</source>
        <translation>Gebruiker 1 verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <location filename="../MainWindow.ui" line="1649"/>
        <source>Clear User 2</source>
        <translation>Gebruiker 2 verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1709"/>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1712"/>
        <location filename="../MainWindow.ui" line="1715"/>
        <source>Analyze Records</source>
        <translation>Records analyseren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1734"/>
        <location filename="../MainWindow.ui" line="1737"/>
        <location filename="../MainWindow.ui" line="1740"/>
        <source>Time Mode</source>
        <translation>Tijd-mode</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="49"/>
        <location filename="../MainWindow.cpp" line="50"/>
        <location filename="../MainWindow.cpp" line="3410"/>
        <location filename="../MainWindow.cpp" line="3411"/>
        <source>Records For Selected User</source>
        <translation>Records voor geselecteerde gebruiker</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="57"/>
        <location filename="../MainWindow.cpp" line="58"/>
        <location filename="../MainWindow.cpp" line="3413"/>
        <location filename="../MainWindow.cpp" line="3414"/>
        <source>Select Date &amp; Time</source>
        <translation>Datum en tijd kiezen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="87"/>
        <location filename="../MainWindow.cpp" line="3444"/>
        <source>Systolic - Value Range</source>
        <translation>Systolisch - Waardebereik</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="88"/>
        <location filename="../MainWindow.cpp" line="3445"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolisch - Waardebereik</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="91"/>
        <location filename="../MainWindow.cpp" line="3447"/>
        <source>Systolic - Target Area</source>
        <translation>Systolisch - Doelgebied</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="92"/>
        <location filename="../MainWindow.cpp" line="3448"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolisch - Doelgebied</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="709"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Meetwaarden : %1  |  Onregelmatig : %2  |  Beweging : %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="714"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Meetwaarden : 0  |  Onregelmatig : 0  |  Beweging : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1168"/>
        <source>Athlete</source>
        <translation>Athletisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1168"/>
        <source>To Low</source>
        <translation>Te laag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1169"/>
        <source>Excellent</source>
        <translation>Uitstekend</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1169"/>
        <source>Optimal</source>
        <translation>Optimaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1170"/>
        <source>Great</source>
        <translation>Zeer goed</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1171"/>
        <source>Good</source>
        <translation>Goed</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1170"/>
        <source>Normal</source>
        <translation>Normaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1694"/>
        <location filename="../MainWindow.ui" line="1697"/>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="45"/>
        <location filename="../MainWindow.cpp" line="46"/>
        <location filename="../MainWindow.cpp" line="3252"/>
        <location filename="../MainWindow.cpp" line="3253"/>
        <location filename="../MainWindow.cpp" line="3254"/>
        <location filename="../MainWindow.cpp" line="3255"/>
        <location filename="../MainWindow.cpp" line="3474"/>
        <location filename="../MainWindow.cpp" line="3475"/>
        <location filename="../MainWindow.cpp" line="3476"/>
        <location filename="../MainWindow.cpp" line="3477"/>
        <source>Switch To %1</source>
        <translation>Naar %1 omschakelen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="251"/>
        <location filename="../MainWindow.cpp" line="3252"/>
        <location filename="../MainWindow.cpp" line="3253"/>
        <location filename="../MainWindow.cpp" line="3474"/>
        <location filename="../MainWindow.cpp" line="3475"/>
        <source>User 1</source>
        <translation>Gebruiker 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="45"/>
        <location filename="../MainWindow.cpp" line="46"/>
        <location filename="../MainWindow.cpp" line="261"/>
        <location filename="../MainWindow.cpp" line="3254"/>
        <location filename="../MainWindow.cpp" line="3255"/>
        <location filename="../MainWindow.cpp" line="3476"/>
        <location filename="../MainWindow.cpp" line="3477"/>
        <source>User 2</source>
        <translation>Gebruiker 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="89"/>
        <location filename="../MainWindow.cpp" line="3446"/>
        <source>Heartrate - Value Range</source>
        <translation>Hartslag - Waardebereik</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="93"/>
        <location filename="../MainWindow.cpp" line="3449"/>
        <source>Heartrate - Target Area</source>
        <translation>Hartslag - Doelgebied</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="313"/>
        <source>Blood Pressure Report</source>
        <translation>Bloeddrukbericht</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="710"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4 | DIA : Ø %2 / x̃ %5 | Hartslag : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="715"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  Hartslag : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1171"/>
        <source>High Normal</source>
        <translation>Hoog normaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1172"/>
        <location filename="../MainWindow.cpp" line="1642"/>
        <location filename="../MainWindow.cpp" line="3453"/>
        <location filename="../MainWindow.cpp" line="3457"/>
        <location filename="../MainWindow.cpp" line="3461"/>
        <location filename="../MainWindow.h" line="293"/>
        <location filename="../MainWindow.h" line="297"/>
        <location filename="../MainWindow.h" line="301"/>
        <source>Average</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1172"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1173"/>
        <source>Below Average</source>
        <translation>Lager dan het gemiddelde</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1173"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1174"/>
        <source>Poor</source>
        <translation>Slecht</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1174"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1323"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Het scannen van de importplug-in &quot;%1&quot; is mislukt!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1336"/>
        <location filename="../MainWindow.cpp" line="1358"/>
        <location filename="../MainWindow.cpp" line="3420"/>
        <source>Switch Language to %1</source>
        <translation>Taal wisselen naar %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1383"/>
        <location filename="../MainWindow.cpp" line="1398"/>
        <location filename="../MainWindow.cpp" line="3428"/>
        <source>Switch Theme to %1</source>
        <translation>Thema wisselen naar %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1423"/>
        <location filename="../MainWindow.cpp" line="1443"/>
        <location filename="../MainWindow.cpp" line="3436"/>
        <source>Switch Style to %1</source>
        <translation>Stijl wisselen naar %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <location filename="../MainWindow.cpp" line="1539"/>
        <source>Edit record</source>
        <translation>Record aanpassen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1642"/>
        <location filename="../MainWindow.cpp" line="3454"/>
        <location filename="../MainWindow.cpp" line="3458"/>
        <location filename="../MainWindow.cpp" line="3462"/>
        <location filename="../MainWindow.h" line="294"/>
        <location filename="../MainWindow.h" line="298"/>
        <location filename="../MainWindow.h" line="302"/>
        <source>Median</source>
        <translation>Mediaan</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1644"/>
        <source>Click to swap Average and Median</source>
        <translation>Klik om Gemiddelde en Mediaan te wisselen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1739"/>
        <source>Click to swap Legend and Label</source>
        <translation>Klik om legende en label om te wisselen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1816"/>
        <source>No records to preview for selected time range!</source>
        <translation>Geen records beschikbaar voor het geselecteerde tijdbereik!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1848"/>
        <source>No records to print for selected time range!</source>
        <translation>Geen records om af te drukken voor het geselecteerde tijdbereik!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1886"/>
        <location filename="../MainWindow.cpp" line="1936"/>
        <location filename="../MainWindow.cpp" line="2010"/>
        <location filename="../MainWindow.cpp" line="2128"/>
        <source>%1 (Age: %2, Height: %3, Weight: %4)</source>
        <translation>%1 (Leeftijd: %2, Grootte: %3, Gewicht: %4)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1886"/>
        <location filename="../MainWindow.cpp" line="1936"/>
        <location filename="../MainWindow.cpp" line="2010"/>
        <location filename="../MainWindow.cpp" line="2128"/>
        <source>User %1</source>
        <translation>Gebruiker %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>DATE</source>
        <translation>DATUM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>TIME</source>
        <translation>TIJD</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>BPM</source>
        <translation>Hartslag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>IHB</source>
        <translation>OHS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>COMMENT</source>
        <translation>KOMMENTAAR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>PPR</source>
        <translation>PDR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2192"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Kan geen e-mail maken omdat het genereren van Base64 voor bijlage &quot;% 1&quot; is mislukt! 

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3178"/>
        <source>Chart</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3195"/>
        <source>Table</source>
        <translation>Tabel</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3212"/>
        <source>Statistic</source>
        <translation>Statistiek</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3230"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>Kan E-mail &quot;%1&quot; niet openen!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3240"/>
        <source>Could not start e-mail client!</source>
        <translation>Kan E-mail client niet starten!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3325"/>
        <source>Thanks to all translators:</source>
        <translation>Dank aan alle vertalers:</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3325"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3345"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Koop een voucher in het gewenste bedrag via uw Amazon-account, selecteer een E-mail aan lazyt@mailbox.org als leveringsmethode en specificeer &quot;UBPM&quot; als bericht. 

Hartelijk bedankt!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3362"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Stuur me een e-mail aan lazyt@mailbox.org zodat ik je mijn bankrekeninggegevens kan meedelen.

Hartelijk bedankt!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3387"/>
        <source>Loading application translation failed!</source>
        <translation>Het laden van de applicatie-vertaling is mislukt!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1880"/>
        <location filename="../MainWindow.cpp" line="1930"/>
        <location filename="../MainWindow.cpp" line="2004"/>
        <location filename="../MainWindow.cpp" line="2122"/>
        <source>Created with UBPM for
Windows / Linux / MacOS</source>
        <translation>Gemaakt met UBPM voor
Windows / Linux / MacOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="314"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Geachte Dr. House,

bijgevoegd vindt u mijn bloeddrukgegevens van deze maand.

Met vriendelijke groet, 
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1881"/>
        <location filename="../MainWindow.cpp" line="1931"/>
        <location filename="../MainWindow.cpp" line="2005"/>
        <location filename="../MainWindow.cpp" line="2123"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Gratis en OpenSource 
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2255"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importeren van CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2255"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>CSV Bestand (*.csv);;XML Bestand (*.xml);;JSON Bestand (*.json);;SQL Bestand (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2318"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Kan &quot;%1&quot; niet openen!

Reden: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2688"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Ziet er niet uit naar een UBPM-database!

Misschien verkeerde coderingsinstellingen / wachtwoord?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2737"/>
        <source>Export to %1</source>
        <translation>Exporteren als %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2737"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 File (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2768"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Kan &quot;%1&quot; niet creeren!

Reden: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2774"/>
        <source>The database is empty, no records to export!</source>
        <translation>De database is leeg, er zijn geen records om te exporteren!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2932"/>
        <source>Morning</source>
        <translation>Voormiddag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2932"/>
        <source>Afternoon</source>
        <translation>Namiddag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2940"/>
        <source>Week</source>
        <translation>Week</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2948"/>
        <source>Quarter</source>
        <translation>Kwartaal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2952"/>
        <source>Half Year</source>
        <translation>Half jaar</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2956"/>
        <source>Year</source>
        <translation>Jaar</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3004"/>
        <source>Really delete all records for user %1?</source>
        <translation>Ben je zeker om alle records voor gebruiker %1 te verwijderen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3019"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Alle records voor gebruiker %1 zijn verwijderd en de bestaande database is opgeslagen in &quot;ubpm.sql.bak&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3124"/>
        <source>Really delete all records?</source>
        <translation>Ben je zeker alle records te willen verwijderen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3135"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Alle records zijn verwijderd en de bestaande database is verplaatst naar &quot;ubpm.sql.bak&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3325"/>
        <source>This program may be installed and used free of charge for non-commercial use on as many computers as you like without limitations. A liability for any damages resulting from the use is excluded. Use at your own risk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3451"/>
        <location filename="../MainWindow.cpp" line="3455"/>
        <location filename="../MainWindow.cpp" line="3459"/>
        <location filename="../MainWindow.h" line="291"/>
        <location filename="../MainWindow.h" line="295"/>
        <location filename="../MainWindow.h" line="299"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3452"/>
        <location filename="../MainWindow.cpp" line="3456"/>
        <location filename="../MainWindow.cpp" line="3460"/>
        <location filename="../MainWindow.h" line="292"/>
        <location filename="../MainWindow.h" line="296"/>
        <location filename="../MainWindow.h" line="300"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3510"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Kan de thema file &quot;%1&quot; niet openen!

Reden: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1511"/>
        <location filename="../MainWindow.cpp" line="1528"/>
        <location filename="../MainWindow.cpp" line="4117"/>
        <source>Delete record</source>
        <translation>Record verwijderen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4064"/>
        <source>Show Heartrate</source>
        <translation>Hartslag tonen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4119"/>
        <source>Show record</source>
        <translation>Toon gegevensrecord</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1512"/>
        <location filename="../MainWindow.cpp" line="1535"/>
        <location filename="../MainWindow.cpp" line="4120"/>
        <source>Hide record</source>
        <translation>Verberg gegevensrecord</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1530"/>
        <location filename="../MainWindow.cpp" line="4131"/>
        <source>Really delete selected record?</source>
        <translation>Wil je echt de gelecteerde records verwijderen?</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2293"/>
        <location filename="../MainWindow.cpp" line="3070"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Succesvol %n record van %1 geimporteerd.

     Gebruiker 1 : %2
     Gebruiker 2 : %3</numerusform>
            <numerusform>Succesvol %n records van %1 geimporteerd.

     Gebruiker 1 : %2
     Gebruiker 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2297"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n ongeldige record overgeslagen!</numerusform>
            <numerusform>%n ongeldige records overgeslagen!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2302"/>
        <location filename="../MainWindow.cpp" line="3074"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n meervoudige record overgeslagen!</numerusform>
            <numerusform>%n meervoudige records overgeslagen!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3396"/>
        <source>Loading Qt base translation for &quot;%1&quot; failed!

Internal base translations (like &quot;Yes/No&quot;) are not available.

Don&apos;t show this warning again?</source>
        <translation>Het laden van de Qt-basisvertaling voor &quot;%1&quot; is mislukt!

Interne basisvertalingen (zoals &quot;Ja / Nee&quot;) zijn niet beschikbaar.

Deze waarschuwing niet meer weergeven?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4058"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamisch schalen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4059"/>
        <source>Colored Stripes</source>
        <translation>Gekleurde strepen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4061"/>
        <source>Show Symbols</source>
        <translation>Toon symbolen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4062"/>
        <source>Show Lines</source>
        <translation>Lijnen tonen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4152"/>
        <source>Show Median</source>
        <translation>Toon mediaan</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4153"/>
        <source>Show Values</source>
        <translation>Waarden weergeven</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4194"/>
        <source>Really quit program?</source>
        <translation>Echt stoppen met programma?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universele Bloeddruk Manager</translation>
    </message>
</context>
</TS>
