<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Dataanalyse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Spørring</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="203"/>
        <source>Results</source>
        <translation>Resultater</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Systolisk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Diastolisk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>Pulstrykk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>Uregelmessig</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Movement</source>
        <translation>Bevegelse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Invisible</source>
        <translation>Usynlig</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="169"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="199"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="48"/>
        <source>Could not create memory database!

%1</source>
        <translation>Kunne ikke opprette minnedatabase!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="205"/>
        <source>No results for this query found!</source>
        <translation>Resultatløst!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Resultat</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Match(es)</source>
        <translation type="unfinished">
            <numerusform>Treff</numerusform>
            <numerusform>Treff</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Oppføring</numerusform>
            <numerusform>Oppføringer</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>User %1</source>
        <translation>Bruker %1</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Veiledning</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="19"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>%1-veiledning ble ikke funnet. Viser EN-versjon istedenfor.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="56"/>
        <source>%1 guide not found!</source>
        <translation>Fant ikke %1-veiledning!</translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Manuell oppføring</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Dataoppføring</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="220"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Legg til oppføring for %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Velg dato og tid</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Skriv inn SYS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="151"/>
        <source>Enter DIA</source>
        <translation>Skriv inn DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="173"/>
        <source>Enter BPM</source>
        <translation>Skriv inn SPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Uregelmessig Hjertefrekvens</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="195"/>
        <source>Movement</source>
        <translation>Bevegelse</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="248"/>
        <source>Enter Optional Comment</source>
        <translation>Tast inn Valgfri Kommentar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="278"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Vis melding ved opprettelse, sletting, og endring av oppføring</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="296"/>
        <source>Delete</source>
        <translation>Slett</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="313"/>
        <source>Create</source>
        <translation>Opprett</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="333"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Bruker 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Bruker 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="30"/>
        <location filename="../DialogRecord.cpp" line="99"/>
        <source>Modify</source>
        <translation>Endre</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="46"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Dataoppføringen kunne ikke slettes.

Det finnes ingen oppføring for denne datoen og tiden.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="52"/>
        <source>Data record successfully deleted.</source>
        <translation>Dataoppføring slettet.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="62"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Skriv inn gyldig verdi for «SYS» først!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="72"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Skriv inn en gyldig verdi for «DIA» først!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="81"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Skriv inn en gyldig verdi for «SPM» først!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="103"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Dataoppføringen kunne ikke endres.

Det finnes ingen oppføring for denne datoen og tiden.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="109"/>
        <source>Data Record successfully modified.</source>
        <translation>Dataoppføring endret.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="118"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Dataoppføringen kunne ikke opprettes.

Det finnes ingen oppføring for denne datoen og tiden.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="124"/>
        <source>Data Record successfully created.</source>
        <translation>Dataoppføring opprettet.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="138"/>
        <source>Choose Database Location</source>
        <translation>Velg databaseplassering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="174"/>
        <source>Could not display manual!

%1</source>
        <translation>Kunne ikke vise manual.

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="301"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>SQL-kryptering kan ikke skrus på uten passord, og vil bli skrudd av!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="308"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Skriv inn gyldig ytterligere info for bruker 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="315"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Skriv inn gyldig ytterligere info for bruker 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="322"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>Innskrevet alder samsvarer ikke med valgt aldersgruppe for bruker 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="329"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>Innskrevet alder samsvarer ikke med valgt aldersgruppe for bruker 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="336"/>
        <source>Please enter a valid e-mail address!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="343"/>
        <source>Please enter a e-mail subject!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="350"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="442"/>
        <source>User 1</source>
        <translation>Bruker 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="456"/>
        <source>User 2</source>
        <translation>Bruker 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="568"/>
        <source>Blood Pressure Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="569"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="601"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Avbryt oppsett og forkast alle endringer?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="50"/>
        <source>Location</source>
        <translation>Plassering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="56"/>
        <source>Current Location</source>
        <translation>Nåværende plassering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="72"/>
        <source>Change Location</source>
        <translation>Endre plassering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="98"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Krypter med SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="101"/>
        <source>Encryption</source>
        <translation>Kryptering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="113"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="126"/>
        <source>Show Password</source>
        <translation>Vis passord</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="154"/>
        <source>User</source>
        <translation>Bruker</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="189"/>
        <location filename="../DialogSettings.ui" line="440"/>
        <source>Male</source>
        <translation>Mann</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="218"/>
        <location filename="../DialogSettings.ui" line="469"/>
        <source>Female</source>
        <translation>Kvinne</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="308"/>
        <location filename="../DialogSettings.ui" line="568"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="183"/>
        <location filename="../DialogSettings.ui" line="434"/>
        <source>Mandatory Information</source>
        <translation>Obligatorisk info</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="244"/>
        <location filename="../DialogSettings.ui" line="504"/>
        <source>Age Group</source>
        <translation>Aldersgruppe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="324"/>
        <location filename="../DialogSettings.ui" line="584"/>
        <source>Additional Information</source>
        <translation>Ytterligere info</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="378"/>
        <location filename="../DialogSettings.ui" line="638"/>
        <source>Height</source>
        <translation>Høyde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="407"/>
        <location filename="../DialogSettings.ui" line="667"/>
        <source>Weight</source>
        <translation>Vekt</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="692"/>
        <source>Device</source>
        <translation>Enhet</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="698"/>
        <source>Import Plugins</source>
        <translation>Importer programtillegg</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="722"/>
        <source>Show Device Image</source>
        <translation>Vis enhetsbilde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="745"/>
        <source>Show Device Manual</source>
        <translation>Vis enhetsmanual</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Open Website</source>
        <translation>Åpne nettside</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="803"/>
        <source>Maintainer</source>
        <translation>Vedlikeholder</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="831"/>
        <source>Version</source>
        <translation>Versjon</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="838"/>
        <source>Send E-Mail</source>
        <translation>Send e-post</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="851"/>
        <source>Model</source>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="858"/>
        <source>Producer</source>
        <translation>Produsent</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="875"/>
        <source>Chart</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="978"/>
        <source>X-Axis Range</source>
        <translation>X-akseverdier</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="984"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamisk skalering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="965"/>
        <source>Healthy Ranges</source>
        <translation>Sunne verdispenn</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="906"/>
        <source>Symbols</source>
        <translation>Symboler</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="959"/>
        <source>Colored Areas</source>
        <translation>Fargelagte områder</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="352"/>
        <location filename="../DialogSettings.ui" line="612"/>
        <source>Birth Year</source>
        <translation>Fødselsår</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="707"/>
        <source>Please choose Device Plugin…</source>
        <translation>Velg enhetsprogramtillegg …</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1106"/>
        <location filename="../DialogSettings.ui" line="1381"/>
        <source>Systolic</source>
        <translation>Systolisk</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1194"/>
        <location filename="../DialogSettings.ui" line="1469"/>
        <source>Diastolic</source>
        <translation>Diastolisk</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1282"/>
        <location filename="../DialogSettings.ui" line="1557"/>
        <source>Heartrate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1654"/>
        <source>Table</source>
        <translation>Tabell</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1739"/>
        <location filename="../DialogSettings.ui" line="1922"/>
        <source>Systolic Warnlevel</source>
        <translation>Systolisk advarselsnivå</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1685"/>
        <location filename="../DialogSettings.ui" line="1982"/>
        <source>Diastolic Warnlevel</source>
        <translation>Diastolisk advarselsnivå</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="915"/>
        <location filename="../DialogSettings.ui" line="943"/>
        <source>Size</source>
        <translation>Størrelse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="997"/>
        <source>Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1006"/>
        <location filename="../DialogSettings.ui" line="1034"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1063"/>
        <source>Show Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1069"/>
        <source>Show Heartrate in Chart View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1079"/>
        <source>Print Heartrate</source>
        <translation>Skriv ut puls</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1085"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1799"/>
        <location filename="../DialogSettings.ui" line="2040"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Pulstrykk-advarselsnivå</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1853"/>
        <location filename="../DialogSettings.ui" line="2094"/>
        <source>Heartrate Warnlevel</source>
        <translation>Puls-advarselsnivå</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2159"/>
        <source>Statistic</source>
        <translation>Statistikk</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2165"/>
        <source>Bar Type</source>
        <translation>Stolpetype</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2171"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Vis median istedenfor gjennonsnittsstopler</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2181"/>
        <source>Legend Type</source>
        <translation>Verditype</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2187"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Vis verdier og verdityper istedenfor beskrivelser</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2202"/>
        <source>E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2210"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2229"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2247"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2268"/>
        <source>Update</source>
        <translation>Oppgradering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2274"/>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2280"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Se etter nye versjoner på nett ved programoppstart</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2293"/>
        <source>Notification</source>
        <translation>Merknad</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2299"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Alltid vis resultat for sjekk av nye versjoner</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2315"/>
        <source>Save</source>
        <translation>Lagre</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2332"/>
        <source>Reset</source>
        <translation>Tilbakestill</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2349"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Nettbasert oppgradering</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Tilgjengelig versjon</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Oppgraderings-filstørrelse</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Installert versjon</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="196"/>
        <source>Download</source>
        <translation>Last ned</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="216"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="162"/>
        <source>No new version found.</source>
        <translation>Fant ingen ny versjon.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Downloading update failed!

%1</source>
        <translation>Kunne ikke laste ned oppgradering.

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Checking update failed!

%1</source>
        <translation>Kunne ikke se etter nye versjoner!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="135"/>
        <source>Unexpected response from update server!</source>
        <translation>Uventet svar fra oppgraderingstjener!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="182"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>Den nye versjonen har ikke forventet størrelse.

%L1 : %L2

Prøver å laste ned igjen …</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="186"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Oppgradering lagret til %1.

Start ny versjon nå?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="199"/>
        <source>Could not start new version!</source>
        <translation>Kunne ikke starte ny versjon!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="242"/>
        <source>Really abort download?</source>
        <translation>Avbryt nedlastingen?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="206"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Kunne ikke lagre oppdatering til %1!

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="20"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universell blodtrykkshåndterer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Diagramvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Tabellvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="3440"/>
        <source>Systolic</source>
        <translation>Systolisk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="78"/>
        <location filename="../MainWindow.cpp" line="3441"/>
        <source>Diastolic</source>
        <translation>Diastolisk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pulstrykk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Uregelmessig (hjertearytmi)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Bevegelse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Usynlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Statisk visning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>Hvert kvarter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>Hver halvtime</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Hver time</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>¼-daglig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>Halvdaglig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Daglig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation>Ukentlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation>Månedlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation>Kvartalsvis</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation>Halvårlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation>Årlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation>Siste 7 dager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation>Siste 14 dager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation>Siste 21 dager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation>Siste 28 dager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation>Siste 3 måneder</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation>Siste 6 måneder</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation>Siste 9 måneder</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation>Siste 12 måneder</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation>Alle oppføringer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation>Skriv ut</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation>Hjelp</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Configuration</source>
        <translation>Oppsett</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1161"/>
        <source>Theme</source>
        <translation>Drakt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1170"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1179"/>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1196"/>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1200"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1212"/>
        <source>Export</source>
        <translation>Eksporter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Clear</source>
        <translation>Tøm</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1307"/>
        <source>Quit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1310"/>
        <location filename="../MainWindow.ui" line="1313"/>
        <source>Quit Program</source>
        <translation>Avslutt programmet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1325"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1328"/>
        <location filename="../MainWindow.ui" line="1331"/>
        <source>About Program</source>
        <translation>Om programmet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1340"/>
        <source>Guide</source>
        <translation>Veiledning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1343"/>
        <location filename="../MainWindow.ui" line="1346"/>
        <source>Show Guide</source>
        <translation>Vis veiledning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1355"/>
        <source>Update</source>
        <translation>Oppgradering</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1358"/>
        <location filename="../MainWindow.ui" line="1361"/>
        <source>Check Update</source>
        <translation>Se etter oppgradring</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1752"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1755"/>
        <location filename="../MainWindow.ui" line="1758"/>
        <source>Donate via PayPal</source>
        <translation>Doner via PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1770"/>
        <location filename="../MainWindow.ui" line="1773"/>
        <source>Donate via Liberapay</source>
        <translation>Doner via Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1785"/>
        <location filename="../MainWindow.ui" line="1788"/>
        <source>Donate via Amazon</source>
        <translation>Doner via Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1800"/>
        <location filename="../MainWindow.ui" line="1803"/>
        <source>Donate via SEPA</source>
        <translation>Doner via SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1812"/>
        <source>Translation</source>
        <translation>Oversettelse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1815"/>
        <location filename="../MainWindow.ui" line="1818"/>
        <source>Contribute Translation</source>
        <translation>Bistå oversettelsen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1827"/>
        <source>E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1830"/>
        <location filename="../MainWindow.ui" line="1833"/>
        <source>Send E-Mail</source>
        <translation>Send e-post</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1132"/>
        <source>Donation</source>
        <translation>Donasjon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="79"/>
        <location filename="../MainWindow.cpp" line="3442"/>
        <source>Heartrate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1129"/>
        <source>Make Donation</source>
        <translation>Send donasjon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1373"/>
        <source>Bugreport</source>
        <translation>Feilrapport</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1376"/>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Send Bugreport</source>
        <translation>Send feilrapport</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1388"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1391"/>
        <location filename="../MainWindow.ui" line="1394"/>
        <source>Change Settings</source>
        <translation>Endre innstillinger</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1409"/>
        <source>From Device</source>
        <translation>Fra enhet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <location filename="../MainWindow.ui" line="1415"/>
        <source>Import From Device</source>
        <translation>Importer fra enhet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>From File</source>
        <translation>Fra fil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Import From File</source>
        <translation>Importer fra fil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1442"/>
        <source>From Input</source>
        <translation>Fra inndata</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <location filename="../MainWindow.ui" line="1448"/>
        <source>Import From Input</source>
        <translation>Importer fra inndata</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1457"/>
        <source>To CSV</source>
        <translation>Til CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <location filename="../MainWindow.ui" line="1463"/>
        <source>Export To CSV</source>
        <translation>Eksporter til CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1475"/>
        <source>To XML</source>
        <translation>Til XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1478"/>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>Export To XML</source>
        <translation>Eksporter til XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1490"/>
        <source>To JSON</source>
        <translation>Til JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1493"/>
        <location filename="../MainWindow.ui" line="1496"/>
        <source>Export To JSON</source>
        <translation>Eksporter til JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>To SQL</source>
        <translation>Til SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1508"/>
        <location filename="../MainWindow.ui" line="1511"/>
        <source>Export To SQL</source>
        <translation>Eksporter til SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Print Chart</source>
        <translation>Skriv ut diagram</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1523"/>
        <location filename="../MainWindow.ui" line="1526"/>
        <source>Print Chart View</source>
        <translation>Skriv ut diagramvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Print Table</source>
        <translation>Skriv ut tabell</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1538"/>
        <location filename="../MainWindow.ui" line="1541"/>
        <source>Print Table View</source>
        <translation>Skriv ut tabellvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Print Statistic</source>
        <translation>Skriv ut statistikk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1553"/>
        <location filename="../MainWindow.ui" line="1556"/>
        <source>Print Statistic View</source>
        <translation>Skriv ut statistikkvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Preview Chart</source>
        <translation>Forhåndsvis diagram</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1568"/>
        <location filename="../MainWindow.ui" line="1571"/>
        <source>Preview Chart View</source>
        <translation>Forhåndsvis diagramvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <source>Preview Table</source>
        <translation>Forhåndsvis tabell</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1583"/>
        <location filename="../MainWindow.ui" line="1586"/>
        <source>Preview Table View</source>
        <translation>Forhåndsvis tabellvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <source>Preview Statistic</source>
        <translation>Forhåndsvis statistikk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1598"/>
        <location filename="../MainWindow.ui" line="1601"/>
        <source>Preview Statistic View</source>
        <translation>Forhåndsvis statistikkvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <location filename="../MainWindow.ui" line="1616"/>
        <source>Clear All</source>
        <translation>Tøm alt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <location filename="../MainWindow.ui" line="1634"/>
        <source>Clear User 1</source>
        <translation>Tøm bruker 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <location filename="../MainWindow.ui" line="1649"/>
        <source>Clear User 2</source>
        <translation>Tøm bruker 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1709"/>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1712"/>
        <location filename="../MainWindow.ui" line="1715"/>
        <source>Analyze Records</source>
        <translation>Analyser oppføringer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1734"/>
        <location filename="../MainWindow.ui" line="1737"/>
        <location filename="../MainWindow.ui" line="1740"/>
        <source>Time Mode</source>
        <translation>Tidsmodus</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="49"/>
        <location filename="../MainWindow.cpp" line="50"/>
        <location filename="../MainWindow.cpp" line="3410"/>
        <location filename="../MainWindow.cpp" line="3411"/>
        <source>Records For Selected User</source>
        <translation>Oppføringer for valgt bruker</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="57"/>
        <location filename="../MainWindow.cpp" line="58"/>
        <location filename="../MainWindow.cpp" line="3413"/>
        <location filename="../MainWindow.cpp" line="3414"/>
        <source>Select Date &amp; Time</source>
        <translation>Velg dato og tid</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="87"/>
        <location filename="../MainWindow.cpp" line="3444"/>
        <source>Systolic - Value Range</source>
        <translation>Systolisk – verdispenn</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="88"/>
        <location filename="../MainWindow.cpp" line="3445"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolisk – verdispenn</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="91"/>
        <location filename="../MainWindow.cpp" line="3447"/>
        <source>Systolic - Target Area</source>
        <translation>Systolisk – målområde</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="92"/>
        <location filename="../MainWindow.cpp" line="3448"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolisk – målområd</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1168"/>
        <source>Athlete</source>
        <translation>Atlet</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1168"/>
        <source>To Low</source>
        <translation>For lavt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1169"/>
        <source>Excellent</source>
        <translation>Utmerket</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1169"/>
        <source>Optimal</source>
        <translation>Optimalt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1170"/>
        <source>Great</source>
        <translation>Veldig bra</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1171"/>
        <source>Good</source>
        <translation>Bra</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1170"/>
        <source>Normal</source>
        <translation>Normalt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1694"/>
        <location filename="../MainWindow.ui" line="1697"/>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="45"/>
        <location filename="../MainWindow.cpp" line="46"/>
        <location filename="../MainWindow.cpp" line="3252"/>
        <location filename="../MainWindow.cpp" line="3253"/>
        <location filename="../MainWindow.cpp" line="3254"/>
        <location filename="../MainWindow.cpp" line="3255"/>
        <location filename="../MainWindow.cpp" line="3474"/>
        <location filename="../MainWindow.cpp" line="3475"/>
        <location filename="../MainWindow.cpp" line="3476"/>
        <location filename="../MainWindow.cpp" line="3477"/>
        <source>Switch To %1</source>
        <translation>Bytt til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="251"/>
        <location filename="../MainWindow.cpp" line="3252"/>
        <location filename="../MainWindow.cpp" line="3253"/>
        <location filename="../MainWindow.cpp" line="3474"/>
        <location filename="../MainWindow.cpp" line="3475"/>
        <source>User 1</source>
        <translation>Bruker 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="45"/>
        <location filename="../MainWindow.cpp" line="46"/>
        <location filename="../MainWindow.cpp" line="261"/>
        <location filename="../MainWindow.cpp" line="3254"/>
        <location filename="../MainWindow.cpp" line="3255"/>
        <location filename="../MainWindow.cpp" line="3476"/>
        <location filename="../MainWindow.cpp" line="3477"/>
        <source>User 2</source>
        <translation>Bruker 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="89"/>
        <location filename="../MainWindow.cpp" line="3446"/>
        <source>Heartrate - Value Range</source>
        <translation>Pulse – verdispenn</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="93"/>
        <location filename="../MainWindow.cpp" line="3449"/>
        <source>Heartrate - Target Area</source>
        <translation>Puls – målområde</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="313"/>
        <source>Blood Pressure Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="314"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="710"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  SPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="715"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  SPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1171"/>
        <source>High Normal</source>
        <translation>Høy normal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1172"/>
        <location filename="../MainWindow.cpp" line="1642"/>
        <location filename="../MainWindow.cpp" line="3453"/>
        <location filename="../MainWindow.cpp" line="3457"/>
        <location filename="../MainWindow.cpp" line="3461"/>
        <location filename="../MainWindow.h" line="293"/>
        <location filename="../MainWindow.h" line="297"/>
        <location filename="../MainWindow.h" line="301"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1172"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1173"/>
        <source>Below Average</source>
        <translation>Under gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1173"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1174"/>
        <source>Poor</source>
        <translation>Dårlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1174"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1323"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Kunne ikke skanne importerings-programtillegg &quot;%1&quot;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1336"/>
        <location filename="../MainWindow.cpp" line="1358"/>
        <location filename="../MainWindow.cpp" line="3420"/>
        <source>Switch Language to %1</source>
        <translation>Bytt språk til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1383"/>
        <location filename="../MainWindow.cpp" line="1398"/>
        <location filename="../MainWindow.cpp" line="3428"/>
        <source>Switch Theme to %1</source>
        <translation>Bytt drakt til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1423"/>
        <location filename="../MainWindow.cpp" line="1443"/>
        <location filename="../MainWindow.cpp" line="3436"/>
        <source>Switch Style to %1</source>
        <translation>Bytt stil til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1514"/>
        <location filename="../MainWindow.cpp" line="1539"/>
        <source>Edit record</source>
        <translation>Rediger oppføring</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1642"/>
        <location filename="../MainWindow.cpp" line="3454"/>
        <location filename="../MainWindow.cpp" line="3458"/>
        <location filename="../MainWindow.cpp" line="3462"/>
        <location filename="../MainWindow.h" line="294"/>
        <location filename="../MainWindow.h" line="298"/>
        <location filename="../MainWindow.h" line="302"/>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1644"/>
        <source>Click to swap Average and Median</source>
        <translation>Klikk for å bytte gjennomsnitt og median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1739"/>
        <source>Click to swap Legend and Label</source>
        <translation>Klikk for å bytte verditype og etikett</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1816"/>
        <source>No records to preview for selected time range!</source>
        <translation>Ingen oppføringer å vise for valgt tidsområde!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1848"/>
        <source>No records to print for selected time range!</source>
        <translation>Ingen oppføringer å skrive ut for valgt tidsområde!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1886"/>
        <location filename="../MainWindow.cpp" line="1936"/>
        <location filename="../MainWindow.cpp" line="2010"/>
        <location filename="../MainWindow.cpp" line="2128"/>
        <source>%1 (Age: %2, Height: %3, Weight: %4)</source>
        <translation>%1 (Alder: %2, Høyde: %3, Vekt: %4)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1886"/>
        <location filename="../MainWindow.cpp" line="1936"/>
        <location filename="../MainWindow.cpp" line="2010"/>
        <location filename="../MainWindow.cpp" line="2128"/>
        <source>User %1</source>
        <translation>Bruker %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>DATE</source>
        <translation>DATO</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>TIME</source>
        <translation>TID</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>BPM</source>
        <translation>SPM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>IHB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>COMMENT</source>
        <translation>KOMMENTAR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2044"/>
        <source>PPR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2192"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3178"/>
        <source>Chart</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3195"/>
        <source>Table</source>
        <translation>Tabell</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3212"/>
        <source>Statistic</source>
        <translation>Statistikk</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3230"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3240"/>
        <source>Could not start e-mail client!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3325"/>
        <source>Thanks to all translators:</source>
        <translation>Takk til alle oversettere:</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3325"/>
        <source>Version</source>
        <translation>Versjon</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3345"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Kjøp en kupong for ønsket beløp fra din Amazon-konto og velg e-post til lazyt@mailbox.org som leveringsmetode og angi «UBPM» som melding.

Takk skal du ha!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3362"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Skriv en e-post til lazyt@mailbox.org og spør om bankkontoinfo, så får du det.

Takk skal du ha!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3387"/>
        <source>Loading application translation failed!</source>
        <translation>Kunne ikke laste inn oversettelse!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1880"/>
        <location filename="../MainWindow.cpp" line="1930"/>
        <location filename="../MainWindow.cpp" line="2004"/>
        <location filename="../MainWindow.cpp" line="2122"/>
        <source>Created with UBPM for
Windows / Linux / MacOS</source>
        <translation>Opprettet med UBPM for
Linux/Windows/macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1881"/>
        <location filename="../MainWindow.cpp" line="1931"/>
        <location filename="../MainWindow.cpp" line="2005"/>
        <location filename="../MainWindow.cpp" line="2123"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Fri programvare
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2255"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importer fra CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2255"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>CSV-fil (*.csv);;XML File (*.xml);;JSON-fil (*.json);;SQL-fil (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2318"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Kunne ikke åpne «%1».

Grunn: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="709"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Målinger: %1  |  Uregelmessig: %2  |  Bevegelse: %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="714"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Målinger: 0  |  Uregelmessig: 0  |  Bevegelse: 0</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2293"/>
        <location filename="../MainWindow.cpp" line="3070"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2297"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2302"/>
        <location filename="../MainWindow.cpp" line="3074"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2688"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Ser ikke ut til å være en UBPM-database.

Kanskje du krypteringsinnstillingene eller passordet er galt?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2737"/>
        <source>Export to %1</source>
        <translation>Eksporter til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2737"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 fil (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2768"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Kunne ikke opprette «%1».

Grunn: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2774"/>
        <source>The database is empty, no records to export!</source>
        <translation>Databasen er tom. Ingen oppføringer å eksportere!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2932"/>
        <source>Morning</source>
        <translation>Morgen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2932"/>
        <source>Afternoon</source>
        <translation>Ettermiddag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2940"/>
        <source>Week</source>
        <translation>Uke</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2948"/>
        <source>Quarter</source>
        <translation>Kvartal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2952"/>
        <source>Half Year</source>
        <translation>Halvår</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2956"/>
        <source>Year</source>
        <translation>År</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3004"/>
        <source>Really delete all records for user %1?</source>
        <translation>Slett alle oppføringer for bruker %1?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3019"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Alle oppføringer for bruker %1 er slettet, og eksisterende database lagret som «ubpm.sql.bak».</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3124"/>
        <source>Really delete all records?</source>
        <translation>Slett alle oppføringer?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3135"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Alle oppføringer slettet og eksisterende database «ubpm.sql» flyttet til papirkurv.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3325"/>
        <source>This program may be installed and used free of charge for non-commercial use on as many computers as you like without limitations. A liability for any damages resulting from the use is excluded. Use at your own risk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3451"/>
        <location filename="../MainWindow.cpp" line="3455"/>
        <location filename="../MainWindow.cpp" line="3459"/>
        <location filename="../MainWindow.h" line="291"/>
        <location filename="../MainWindow.h" line="295"/>
        <location filename="../MainWindow.h" line="299"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3452"/>
        <location filename="../MainWindow.cpp" line="3456"/>
        <location filename="../MainWindow.cpp" line="3460"/>
        <location filename="../MainWindow.h" line="292"/>
        <location filename="../MainWindow.h" line="296"/>
        <location filename="../MainWindow.h" line="300"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3510"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Kunne ikke åpne draktfilen &quot;%1&quot;!

Grunn: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1511"/>
        <location filename="../MainWindow.cpp" line="1528"/>
        <location filename="../MainWindow.cpp" line="4117"/>
        <source>Delete record</source>
        <translation>Slett oppføring</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4062"/>
        <source>Show Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4064"/>
        <source>Show Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4119"/>
        <source>Show record</source>
        <translation>Vis oppføring</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1512"/>
        <location filename="../MainWindow.cpp" line="1535"/>
        <location filename="../MainWindow.cpp" line="4120"/>
        <source>Hide record</source>
        <translation>Skjul oppføring</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1530"/>
        <location filename="../MainWindow.cpp" line="4131"/>
        <source>Really delete selected record?</source>
        <translation>Slett valgt oppføring?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3396"/>
        <source>Loading Qt base translation for &quot;%1&quot; failed!

Internal base translations (like &quot;Yes/No&quot;) are not available.

Don&apos;t show this warning again?</source>
        <translation>Kunne ikke laste inn Qt Base-oversettelse for &quot;%1&quot;!

Interne Base-oversettelser (som «Ja/Nei») er ikke tilgjengelige.

Ikke vis denne meldingen igjen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4058"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamisk skalering</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4059"/>
        <source>Colored Stripes</source>
        <translation>Fargede striper</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4061"/>
        <source>Show Symbols</source>
        <translation>Vis symboler</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4152"/>
        <source>Show Median</source>
        <translation>Vis median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4153"/>
        <source>Show Values</source>
        <translation>Vis verdier</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4194"/>
        <source>Really quit program?</source>
        <translation>Avslutt programmet?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universell blodtrykkshåndterer</translation>
    </message>
</context>
</TS>
