#ifndef DEVICEINTERFACE_H
#define DEVICEINTERFACE_H

#define DeviceInterface_iid "de.lazyt.ubpm.deviceinterface"

#include <QObject>
#include <QString>

struct DEVICEINFO
{
	QString producer;
	QString model;
	QString alias;
	QString maintainer;
	QString version;
	QIcon icon;
	QByteArray image;
	QByteArray manual;
};

struct HEALTHDATA
{
	qint64 dts;
	int sys;
	int dia;
	int bpm;
	bool ihb;
	bool mov;
	bool inv;
	QString msg;
};

class DeviceInterface
{
public:

	virtual ~DeviceInterface() = default;

	virtual DEVICEINFO getDeviceInfo() = 0;
	virtual bool getDeviceData(QWidget*, QString, QString, QVector<HEALTHDATA>*, QVector<HEALTHDATA>*) = 0;
};

Q_DECLARE_INTERFACE(DeviceInterface, DeviceInterface_iid)

#endif // DEVICEINTERFACE_H
